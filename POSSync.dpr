program POSSync;

uses
  System.StartUpCopy,
  FMX.Forms,
  dmMainUnit in 'Units\dm\dmMainUnit.pas' {dmMain: TDataModule},
  FormMainUnit in 'Units\Forms\FormMainUnit.pas' {FormMain},
  dmDBConnectionUnit in 'Units\dm\dmDBConnectionUnit.pas' {dmDBConnection: TDataModule},
  dmItemsUnit in 'Units\dm\dmItemsUnit.pas' {dmItems: TDataModule},
  InterFaceUnit in 'Units\Misc\InterFaceUnit.pas',
  SharedUnit in 'Units\Misc\SharedUnit.pas',
  FuelPOSUnit in 'Units\OutData\FuelPOSUnit.pas',
  TeraFunkUnit in 'Units\Misc\TeraFunkUnit.pas',
  EnumConverterUnit in 'Units\Misc\EnumConverterUnit.pas',
  dmLogDataUnit in 'Units\dm\dmLogDataUnit.pas' {dmBon: TDataModule},
  dmCaxhStatementsUnit in 'Units\dm\dmCaxhStatementsUnit.pas' {dmCashStatements: TDataModule},
  FuelPOSImportUnit in 'Units\InData\FuelPOSImportUnit.pas',
  IniFileUnit in 'Units\Misc\IniFileUnit.pas',
  CashStatementsUnit in 'Units\InData\CashStatementsUnit.pas',
  BonTransUnit in 'Units\InData\BonTransUnit.pas',
  dmSystemTablesUnit in 'Units\dm\dmSystemTablesUnit.pas' {dmSystemTables: TDataModule},
  dmItemTransactionUnit in 'Units\dm\dmItemTransactionUnit.pas' {dmItemTransaction: TDataModule},
  dmVirtualPOSClientsUnit in 'Units\dm\dmVirtualPOSClientsUnit.pas' {dmVirtualPOSClients: TDataModule},
  IDGeneratorUnit in 'Units\Misc\IDGeneratorUnit.pas';

{$R *.res}

begin
  {$IFDEF DEBUG}
  ReportMemoryLeaksOnShutdown := True;
  {$ENDIF}
  Application.Initialize;
  Application.CreateForm(TdmDBConnection, dmDBConnection);
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TdmItems, dmItems);
  Application.Run;
end.
