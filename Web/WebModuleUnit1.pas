unit WebModuleUnit1;

interface

uses
  System.SysUtils
  , System.Classes
  , Web.HTTPApp
  , IdBaseComponent
  , IdComponent
  , IdTCPConnection
  , IdTCPClient
  , IdHTTP
  , IdWebDAV
  ;

type
  TWebModule1 = class(TWebModule)
    IdWebDAV1: TIdWebDAV;
    procedure WebModule1DefaultHandlerAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModule1WebActionTestAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WebModuleClass: TComponentClass = TWebModule1;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

procedure TWebModule1.WebModule1DefaultHandlerAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  Response.Content :=
        '<!DOCTYPE html> ' +
        '<html> ' +
        '<body> ' +
        ' ' +
        '<h2>Visible Option Values</h2> ' +
        '<p>Use the size attribute to specify the number of visible values.</p> ' +
        ' ' +
        '<form action="/Test"> ' +
        '  <select name="cars" size="3"> ' +
        '    <option value="volvo">Volvo</option> ' +
        '    <option value="saab">Saab</option> ' +
        '    <option value="fiat">Fiat</option> ' +
        '    <option value="audi">Audi</option> ' +
        '  </select> ' +
        '  <br><br> ' +
        '  <input type="submit"> ' +
        '</form> ' +
        '<form action="/Test"> ' +
        '  <select name="cars" size="3"> ' +
        '    <option value="volvo">Volvo</option> ' +
        '    <option value="saab">Saab</option> ' +
        '    <option value="fiat">Fiat</option> ' +
        '    <option value="audi">Audi</option> ' +
        '  </select> ' +
        '  <br><br> ' +
        '  <input type="submit"> ' +
        '</form> ' +
        ' ' +
        '</body> ' +
        '</html> ';



      {'<!DOCTYPE html> ' +
      '<html> ' +
      '<body> ' +
      ' ' +
      '<h2>Effo FuelPOS</h2> ' +
      ' ' +
      '<form action="/action_page.php"> ' +
      '  Send pr�suppdateringar til kassan: ' +
      '  <br><br> ' +
      '  <input type="button" onclick="alert(''Hello World!'')" value="Click Me!"> ' +
      '</form> ' +
      ' ' +
      '</body> ' +
      '</html> ';}



    {'<html>' +
    '<head><title>Web Server Application</title></head>' +
    '<body>Web Server Application</body>' +
    '</html>';}
end;

procedure TWebModule1.WebModule1WebActionTestAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
  LRequest: string;
begin
    LRequest:=Request.ReadUnicodeString(10);
    Response.Content :=
      '<!DOCTYPE html> ' +
      '<html> ' +
      '<body> ' +
      ' ' +
      '<h2>Test action</h2> ' +
      ' ' +
      '<form action="/action_page.php"> ' +
      '  Send pr�suppdateringar til kassan: <br><br>' +
      LRequest+
      '  <br><br> ' +
      '  <input type="button" onclick="alert(''Hello World!'')" value="Click Me!"> ' +
      '</form> ' +
      ' ' +
      '</body> ' +
      '</html> ';
end;

end.
