unit FuelPOSUnit;

interface

uses
  InterFaceUnit
  , FireDac.Stan.Param
  , SharedUnit
  , System.Classes
  , TeraFunkUnit
  , System.Generics.Collections
  , RTTI
  , IDGeneratorUnit
  //, dmDBConnectionUnit
  , dmItemsUnit
  , IniFileUnit
  ;

Type
  TFuelPOSExport = class(TInterFacedObject,IUpdatePOS)
  private
    SortPromotions: TList<integer>;
    SortItems: TList<integer>;
    FItems: TDictionary<integer,TItem>;
    FPromotions: TObjectDictionary<integer, TPromotion>;
    ExportList: TStringList;
    FFilePaths: TFilePaths;
    function VatCode(Rate: Currency): Char;
    procedure StartExport;
    procedure EndExport;
    procedure StartItemUpdate;
    procedure EndItemUpdate;
    procedure StartPromotion;
    procedure EndPromotion;
    procedure ExportItem(item: TItem);
    procedure ExportPromotion(LineID: integer; Promo: TPromotion);
    procedure UpdatePromotion(Promo: TPromotion);
    procedure UpdateItem(item: TItem);
    procedure ExportAll;
    procedure RemoveUpdatedItemsFromPriceQueue;
    procedure ExportPromotions;
    procedure DeleteAllCampaigns;
  Public
    procedure BeforeDestruction; override;
    constructor create(AFilePaths: TFilePaths); overload;
    destructor Destroy; override;
  end;

implementation

uses
  FMX.Dialogs
  , System.SysUtils
  , EnumConverterUnit;

{ TFuelPOSExport }

constructor TFuelPOSExport.create(AFilePaths: TFilePaths);
begin
    inherited Create;
    FFilePaths:=AFilePaths;
    FItems:=TDictionary<integer,TItem>.Create;
    FPromotions:= TObjectDictionary<integer, TPromotion>.Create([doOwnsValues]);
    ExportList:=TStringList.Create;
    SortPromotions:=TList<integer>.Create;
    SortItems:=TList<integer>.Create;
end;

procedure TFuelPOSExport.DeleteAllCampaigns;
var
   i: integer;
begin
     StartExport;
     ExportList.Add('');
     ExportList.Add('[PROMOTION_DELETE]');

     for i := 1 to 999 do
     begin
        ExportList.Add('PRO'+intToStr(i));
     end;

     ExportList.Add('[END_PROMOTION_DELETE]');
     EndExport;
end;

destructor TFuelPOSExport.Destroy;
begin
    SortItems.Free;
    SortPromotions.Free;
    ExportList.Free;
    FPromotions.Free;
    FItems.free;
    inherited;
end;

procedure TFuelPOSExport.ExportPromotions;
var
  PromotionID: Integer;
  Promotion: TPromotion;
  i: integer;
begin
    i:=0;
    StartPromotion;
    SortPromotions.Sort;

    for PromotionID in SortPromotions do
    begin
      if FPromotions.TryGetValue(PromotionID, Promotion) then
      begin
          inc(i);
          ExportPromotion(i,Promotion);
      end;
    end;

    EndPromotion;
end;

procedure TFuelPOSExport.RemoveUpdatedItemsFromPriceQueue;
var
  item: TItem;
  ItemID: integer;
begin
    for ItemID in FItems.Keys do
    begin
      if FItems.TryGetValue(ItemID, item) then
      begin
          dmItems.DeleteItemFromQueue(Item.ItemID);
      end;
    end;
end;

procedure TFuelPOSExport.EndExport;
var
  Counter: IIDGen;
  Seq: string;
begin
    Counter:=TIDGen.Create(FFilePaths.FIniFilePath, FFilePaths.FCompanyID);
    Seq:=Counter.GetIdAsStr(3);
    ExportList.add('[END_FILE]');
    ExportList.SaveToFile(IncludeTrailingPathDelimiter(FFilePaths.FExportFilePath)+'ART_MUT.'+Seq);
end;

procedure TFuelPOSExport.EndItemUpdate;
begin
    ExportList.Add('[END_SHOP_UPDATE]');
end;

procedure TFuelPOSExport.EndPromotion;
begin
    ExportList.Add('[END_PROMOTION_UPDATE]');
end;

procedure TFuelPOSExport.StartExport;
begin
    ExportList.add('[START_FILE]');
    ExportList.add('');
end;


procedure TFuelPOSExport.StartItemUpdate;
begin
   ExportList.Add('[SHOP_UPDATE]');
end;

procedure TFuelPOSExport.StartPromotion;
begin
    ExportList.Add('');
    ExportList.Add('[PROMOTION_UPDATE]');
    ExportList.Add('MULTIPROMO=YES');
end;

procedure TFuelPOSExport.UpdateItem(item: TItem);
var
  tempItemText: string;
begin
    //Inc(ItemCounter);
    tempItemText:=Copy(item.Text,1,20);
    tempItemText:=FjernaReturnLinjuSkift(tempItemText);

    FItems.Add(Item.ItemID,item);
    SortItems.Add(Item.ItemID);
end;

procedure TFuelPOSExport.UpdatePromotion(Promo: TPromotion);
var
  Item: TItem;
  PromoLines: TDictionary<integer,TPromotionLine>;
  PromoLine: TPromotionLine;
begin
    FPromotions.Add(Promo.PromotionID,Promo);
    SortPromotions.Add(Promo.PromotionID);  // Used for sorting FPromotions Dictionary
    PromoLines:=Promo.GetLines;

    for PromoLine in PromoLines.Values do
    begin
        if FItems.TryGetValue(PromoLine.ItemID,Item) = true then
        begin
            Item.AddPromotion(Promo.PromotionID, PromoLine.Level);
            FItems.AddOrSetValue(PromoLine.ItemID,Item);
        end;
    end;
end;


procedure TFuelPOSExport.ExportAll;
var
  item: TItem;
  ItemID: integer;
  //ExportCounter: integer;
  //i: Integer;
begin
    StartExport;

    // Export Items
    StartItemUpdate;
    SortItems.Sort;

    for ItemID in SortItems do
    begin
      if FItems.TryGetValue(ItemID, item) then
          ExportItem(item);
    end;

    EndItemUpdate;

    // Export Promotions
    if (SortPromotions.Count > 0) then
    begin
        ExportPromotions;
    end;

    EndExport;
end;

procedure TFuelPOSExport.ExportItem(item: TItem);
var
    List: Tlist<string>;
    BarCode: string;
    BarCodeCounter: integer;
    tempItemText: string;
    PromoID: integer;
    PromoLevel: ShortInt;
    Promotion: TPromotion;
    fs: TFormatSettings;
    PromoCounter: integer;
    PromoLines: TDictionary<Integer,TPromotionLine>;
    PromoLine: TPromotionLine;
begin
    fs:=TFormatSettings.Create('DK-dk');
    fs.DecimalSeparator:='.';
    System.SysUtils.FormatSettings := fs;

    PromoCounter:=0;
    PromoLevel:=0;
    tempItemText:=Copy(item.Text,1,20);
    tempItemText:=FjernaReturnLinjuSkift(tempItemText);

    ExportList.Add('NAM'+intToStr(Item.ItemID)+'='+tempItemText);
    ExportList.Add('CRD'+intToStr(Item.ItemID)+'='+item.ItemGrp);
    ExportList.Add('REP'+intToStr(Item.ItemID)+'='+item.DSKGrp);
    ExportList.Add('VAT'+intToStr(Item.ItemID)+'='+VatCode(item.VatRate));
    ExportList.Add('PRI'+intToStr(Item.ItemID)+'='+FloatToStrF(item.PriceIncVat,ffFixed,10,2));
    ExportList.Add('DVAL'+intToStr(Item.ItemID)+'=0.00');
    ExportList.Add('MOD'+intToStr(Item.ItemID)+'='+Item.AllowPriceChangeYesNo);
    ExportList.Add('LNK'+intToStr(Item.ItemID)+'=0');
    ExportList.Add('EVT'+intToStr(Item.ItemID)+'=');

    if (item.PromotionCount > 0) then
    begin
        for PromoID in item.Promotions.Keys do
        begin
            inc(PromoCounter);

            if FPromotions.TryGetValue(PromoID,Promotion) then
            begin
                ExportList.Add('PRO'+intToStr(Item.ItemID)+','+intToStr(PromoCounter)+'='+intToStr(PromoID));

                if (Promotion.PromoType = ptQtyBased) or (Promotion.PromoType = ptMixArticle) then
                begin
                   ExportList.Add('PPRI'+intToStr(Item.ItemID)+','+intToStr(PromoCounter)+'='+FloatToStrF(0,ffFixed,8,2));
                end
                else if (Promotion.PromoType = ptGroup) then
                begin
                   Item.Promotions.TryGetValue(PromoID,PromoLevel);

                   PromoLines:=Promotion.GetLines;

                   if PromoLines.TryGetValue(item.ItemID,PromoLine) then
                      ExportList.Add('PPRI'+intToStr(Item.ItemID)+','+intToStr(PromoCounter)+'='+FloatToStrF(PromoLine.PromPrice,ffFixed,8,2));


                   ExportList.Add('PGRP'+intToStr(Item.ItemID)+','+intToStr(PromoCounter)+'='+IntToStr(PromoLevel));
                end
                else
                   ExportList.Add('PPRI'+intToStr(Item.ItemID)+','+intToStr(PromoCounter)+'='+FloatToStrF(Promotion.TotalPromoPrice,ffFixed,8,2))
            end;
        end;
    end;

    List:=item.GetBarcodeList;
    BarCodeCounter:=1;

    // Add all barcodes from the barcodetable
    if List <> nil then
    begin
        if List.Count > 0 then
        begin
            for BarCode in List do
            begin
                ExportList.Add('BAR'+intToStr(Item.ItemID)+','+intToStr(BarCodeCounter)+'='+BarCode);
                inc(BarCodeCounter);
            end;
        end;
    end;
end;

procedure TFuelPOSExport.ExportPromotion(LineID: integer; Promo: TPromotion);
var
    tempText: string;
    PromoGrp: TDictionary<ShortInt,ShortInt>;
    GroupQty: ShortInt;
    i: integer;
begin
    PromoGrp:=nil;
    try
        i:=0;
        tempText:=Copy(Promo.Name,1,20);
        tempText:=FjernaReturnLinjuSkift(tempText);

        ExportList.Add('PRO'+intToStr(LineID)+'='+intToStr(Promo.PromotionID));
        ExportList.Add('NAM'+intToStr(LineID)+'='+tempText);
        tempText:=intToStr(TEnumConverter.EnumToInt(Promo.PromoType));
        ExportList.Add('TYPE'+intToStr(LineID)+'='+tempText);
        ExportList.Add('FROM_DT'+intToStr(LineID)+'='+Promo.FromDateAsStr);
        ExportList.Add('TO_DT'+intToStr(LineID)+'='+Promo.ToDateAsStr);

        if (Promo.PromoType = ptQtyBased) then
        begin
            ExportList.Add('QUA'+intToStr(LineID)+'='+FloatToStr(Promo.TriggerQty));
            ExportList.Add('AMT'+intToStr(LineID)+'='+FloatToStrF(Promo.TotalPromoPrice, ffFixed,6,2 ));
        end
        else if (Promo.PromoType = ptMixArticle) then
        begin
            ExportList.Add('QUA'+intToStr(LineID)+'='+FloatToStr(Promo.TriggerQty));
            ExportList.Add('AMT'+intToStr(LineID)+'='+FloatToStrF(Promo.TotalPromoPrice, ffFixed,6,2 ));
        end
        else if Promo.PromoType = ptGroup then
        begin
            PromoGrp:=Promo.GetPromoGrp;

            for GroupQty in PromoGrp.Values do
            begin
                inc(i);
                ExportList.Add('GRPQUA'+intToStr(LineID)+','+IntToStr(i)+'='+IntToStr(GroupQty));
            end;
        end;
    finally
        if Assigned(PromoGrp) then
          PromoGrp.Free;
    end;
end;

procedure TFuelPOSExport.BeforeDestruction;
var
   Key: Integer;
   Item: TItem;
begin
    for Key in FItems.Keys do
    begin
        FItems.TryGetValue(Key,Item);

        if (Item <> nil) then
           Item.Free;
    end;
end;

function TFuelPOSExport.VatCode(Rate: Currency): Char;
begin
    if (Rate = 0) then
        result:='0'
    else
        result:='1';
end;

end.
