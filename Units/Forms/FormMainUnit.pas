unit FormMainUnit;

interface

uses
  System.SysUtils
  , System.Types
  , System.UITypes
  , System.Classes
  , System.Variants
  , FMX.Types
  , FMX.Controls
  , FMX.Forms
  , FMX.Graphics
  , FMX.Dialogs
  , FMX.Controls.Presentation
  , FMX.StdCtrls
  , FMX.TabControl
  , Fmx.Bind.Editors
  , FMX.Layouts
  , Fmx.Bind.Navigator
  , FMX.Grid.Style
  , FMX.ScrollBox
  , FMX.Grid
  , Fmx.Bind.Grid
  , Fmx.Bind.DBEngExt
  , dmMainUnit
  , IniFileUnit
  , System.Rtti
  , dmSystemTablesUnit
  , Data.Bind.EngExt
  , System.Bindings.Outputs
  , Data.Bind.Components
  , Data.Bind.Grid
  , Data.Bind.DBScope
  , Data.Bind.Controls
  , Data.DB
  ;

type
  TFormMain = class(TForm)
    TabControl1: TTabControl;
    tabExport: TTabItem;
    TabItem2: TTabItem;
    GroupBox1: TGroupBox;
    btnUpdateAllItems: TButton;
    btnPriceUpdates: TButton;
    btnSendPromotions: TButton;
    tabImport: TTabItem;
    btnGetCashStatements: TButton;
    btnGetBonData: TButton;
    TabItem1: TTabItem;
    StringGrid1: TStringGrid;
    ToolBar1: TToolBar;
    btnInsert: TButton;
    btnSave: TButton;
    btnOpenPaymMethods: TButton;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    NavigatorBindSourceDB1: TBindNavigator;
    chkBoxEnableBatchJob: TCheckBox;
    btnExportPayments: TButton;
    btnImportPayments: TButton;
    btnVirtualStatements: TButton;
    Button1: TButton;
    procedure btnUpdateAllItemsClick(Sender: TObject);
    procedure btnPriceUpdatesClick(Sender: TObject);
    procedure btnSendPromotionsClick(Sender: TObject);
    procedure btnGetCashStatementsClick(Sender: TObject);
    procedure btnGetBonDataClick(Sender: TObject);
    procedure btnOpenPaymMethodsClick(Sender: TObject);
    procedure btnInsertClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure chkBoxEnableBatchJobChange(Sender: TObject);
    procedure btnExportPaymentsClick(Sender: TObject);
    procedure btnImportPaymentsClick(Sender: TObject);
    procedure btnVirtualStatementsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    dmSystemTables: TdmSystemTables;

  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

{$R *.fmx}

uses
  dmItemTransactionUnit
  , FMX.DialogService
  ;

procedure TFormMain.chkBoxEnableBatchJobChange(Sender: TObject);
begin
    dmMain.EnableBatchJob:=chkBoxEnableBatchJob.IsChecked;
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
    TabControl1.ActiveTab:=tabExport;
end;

procedure TFormMain.btnOpenPaymMethodsClick(Sender: TObject);
begin
    if not assigned(dmSystemTables) then
      dmSystemTables:=TdmSystemTables.Create(self);

    dmSystemTables.OpenPaymTable;
    btnInsert.Enabled:=True;
    btnSave.Enabled:=True;
end;

procedure TFormMain.btnSaveClick(Sender: TObject);
begin
    if (dmSystemTables.FDMemFuelPosPaymMethods.State in [dsEdit,dsInsert]) then
        dmSystemTables.FDMemFuelPosPaymMethods.Post;

    dmSystemTables.SavePaymTableToFile;
end;

procedure TFormMain.btnSendPromotionsClick(Sender: TObject);
begin
    dmMain.UpdateCampaigns;
end;

procedure TFormMain.btnExportPaymentsClick(Sender: TObject);
begin
    dmSystemTables.ExportPaymentsToCsv;
end;

procedure TFormMain.btnGetBonDataClick(Sender: TObject);
begin
    dmMain.ProcessBonData;
end;

procedure TFormMain.btnGetCashStatementsClick(Sender: TObject);
begin
    dmMain.ProcessCashStatements;
end;

procedure TFormMain.btnImportPaymentsClick(Sender: TObject);
begin
    dmSystemTables.ImportPaymentFromCsv;
    btnSave.Enabled:=True;
end;

procedure TFormMain.btnInsertClick(Sender: TObject);
begin
    if assigned(dmSystemTables) then
       dmSystemTables.FDMemFuelPosPaymMethods.Insert;
end;

procedure TFormMain.btnPriceUpdatesClick(Sender: TObject);
begin
    dmMain.UpdatePriceChanges;
end;

procedure TFormMain.btnUpdateAllItemsClick(Sender: TObject);
begin
    dmMain.UpdateAll;
end;

procedure TFormMain.btnVirtualStatementsClick(Sender: TObject);
begin
    dmMain.CreateVirtualStatements;
end;

procedure TFormMain.Button1Click(Sender: TObject);
var
  Password: string;
begin
    TDialogService.InputQuery('Loynior�.', ['Loynior�:'], [Password],
        procedure(const AResult: TModalResult; const AValues: array of string)
        begin
            if AValues[0] = 'SYSOP' then
              dmMain.DeleteAllCampaigns;
        end);
end;

end.
