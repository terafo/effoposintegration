unit IniFileUnit;

interface

uses
  inifiles
  , System.SysUtils
  , System.Classes
  , System.Generics.Collections
  , SharedUnit
  ;

type
  TDBParams = record
    IP: string;
    Path: string;
  end;

  TBatchJob = record
    GetBonInterval: SmallInt;    // Minuttir
  end;



  TFilePaths = record
    FCompanyID: SmallInt;
    FPriceGroup: SmallInt;
    FCashStatements: string;
    FCashStatementsArchive: string;
    FCashStatementsError: string;
    FRevenueFile: string;
    FRevenueFileArchive: string;
    FRevenueFileError: string;
    FExportFilePath: string;
    FIniFilePath: string;
  end;

  TProgramParams = class
    private
      FFilePaths: TDictionary<SmallInt, TFilePaths>;
      FVirtualClients: TList<TVirtualClient>;
      FDBParams: TDBParams;
      //FDataAreaID: SmallInt;
      FExtItemGrpHeader: Smallint;
      FPath: string;
      FPaymentDef: string;
      FBatchJob: TBatchJob;
      function GetBatchTimerInterval: integer;
      function LoadIniFile:boolean;
    public
      procedure GetVirtalPOSClients(var Clients: TList<TVirtualClient>);
      function GetFilePaths(CompanyID: SmallInt): TFilePaths;
      constructor Create; overload;
      destructor Destroy; override;
      property DBParams: TDBParams read FDBParams;
      //property DataAreaID: SmallInt read FDataAreaID;
      property ExtItemGroupHeaderID: SmallInt read FExtItemGrpHeader;
      Property IniFilePath: string read FPath;
      property PaymentTable: string read FPaymentDef;
      property BatchTimerInterval: integer read GetBatchTimerInterval;
  end;

implementation

{ TFilePaths }

constructor TProgramParams.Create;
begin
    FFilePaths:=TDictionary<SmallInt, TFilePaths>.Create;
    FVirtualClients:=TList<TVirtualClient>.Create;
    FPath:=IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)))+'POSSync.ini';
    LoadIniFile;
end;

destructor TProgramParams.Destroy;
begin
  FFilePaths.Free;
  FVirtualClients.Free;
  inherited;
end;

function TProgramParams.GetBatchTimerInterval: integer;
begin
    result:=FBatchJob.GetBonInterval*60000;  //Minuttir til millisekung
end;

function TProgramParams.GetFilePaths(CompanyID: SmallInt): TFilePaths;
begin
    FFilePaths.TryGetValue(CompanyID, result);
end;


procedure TProgramParams.GetVirtalPOSClients(var Clients: TList<TVirtualClient>);
begin
    Clients:=FVirtualClients;
end;

function TProgramParams.LoadIniFile:boolean;
var
    IniFile: TMemIniFile;
    LFilePaths: TFilePaths;
    LVirtualClient: TVirtualClient;
begin
    IniFile:=TMemIniFile.Create(FPath);
    try
        try
            FDBParams.IP:=IniFile.ReadString('DB','Server','127.0.0.1');
            FDBParams.Path:=IniFile.ReadString('DB','Path','');
            FExtItemGrpHeader:=IniFile.ReadInteger('Sync Properties','ExtItemGroupHeaderID',0);
            FPaymentDef:=IniFile.ReadString('Tables','PaymentDef','');
            FBatchJob.GetBonInterval:=IniFile.ReadInteger('BathcJob','GetBonDataInterval',60);

            // FilePaths DataArea 1
            LFilePaths.FCompanyID:=1;
            LFilePaths.FCashStatements:=IniFile.ReadString('Import Folders 1','CashStatements','');
            LFilePaths.FCashStatementsError:=IniFile.ReadString('Import Folders 1','CashStatementsError','');
            LFilePaths.FCashStatementsArchive:=IniFile.ReadString('Import Folders 1','CashStatementsArchive','');
            LFilePaths.FRevenueFile:=IniFile.ReadString('Import Folders 1','Logfile','');
            LFilePaths.FRevenueFileError:=IniFile.ReadString('Import Folders 1','LogfileError','');
            LFilePaths.FRevenueFileArchive:=IniFile.ReadString('Import Folders 1','LogfileArchive','');
            LFilePaths.FExportFilePath:=IniFile.ReadString('Export Folder 1','ExportFilePath','');
            LFilePaths.FIniFilePath:=FPath;
            FFilePaths.AddOrSetValue(LFilePaths.FCompanyID,LFilePaths);

            // FilePaths DataArea 2
            LFilePaths.FCompanyID:=2;
            LFilePaths.FCashStatements:=IniFile.ReadString('Import Folders 2','CashStatements','');
            LFilePaths.FCashStatementsError:=IniFile.ReadString('Import Folders 2','CashStatementsError','');
            LFilePaths.FCashStatementsArchive:=IniFile.ReadString('Import Folders 2','CashStatementsArchive','');
            LFilePaths.FRevenueFile:=IniFile.ReadString('Import Folders 2','Logfile','');
            LFilePaths.FRevenueFileError:=IniFile.ReadString('Import Folders 2','LogfileError','');
            LFilePaths.FRevenueFileArchive:=IniFile.ReadString('Import Folders 2','LogfileArchive','');
            LFilePaths.FExportFilePath:=IniFile.ReadString('Export Folder 2','ExportFilePath','');
            LFilePaths.FIniFilePath:=FPath;
            FFilePaths.AddOrSetValue(LFilePaths.FCompanyID,LFilePaths);


            var VirtClientValues := TStringList.Create;
            try
                // Pumpur DataAreaID 1
                IniFile.ReadSection('VirtualPOSClientsID 1',VirtClientValues);

                for var i := 0 to VirtClientValues.Count -1 do
                begin
                    LVirtualClient.ClientID := StrToInt(VirtClientValues[i]);
                    LVirtualClient.DataAreaID:=1;
                    FVirtualClients.Add(LVirtualClient);
                end;

                // Pumpur DataAreaID 2
                VirtClientValues.Clear;
                IniFile.ReadSection('VirtualPOSClientsID 2',VirtClientValues);

                for var i := 0 to VirtClientValues.Count -1 do
                begin
                    LVirtualClient.ClientID := StrToInt(VirtClientValues[i]);
                    LVirtualClient.DataAreaID:=2;
                    FVirtualClients.Add(LVirtualClient);
                end;
            finally
                VirtClientValues.Free;
            end;

            result:=true;
        except
            result:=false;
        end;
    finally
        IniFile.Free;
    end;
end;

end.
