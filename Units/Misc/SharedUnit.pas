unit SharedUnit;

interface

uses
  System.Generics.Collections
  , TeraFunkUnit;

type
  TPromoType = (ptNotSet, ptTimeBased, ptQtyBased, ptMixArticle, ptGroup);

  TVirtualClient = record
    ClientID: SmallInt;
    DataAreaID: SmallInt;
  end;

  TBonHeaderFields = record
    BonID: integer;
    BonDateTime: TDateTime;
    PosClientID: SmallInt;
    BonStatus: SmallInt;
    PosCashStatementID: integer;
    EmplID: string;
    BonAmount: Currency;
    Ref: int64;
    LRef: int64;
    TransType: SmallInt;
  end;

  TBonLineFields = record
    FLineNo: SmallInt;
    FDateTime: TDateTime;
    FItemName: string;
    FBonID: integer;
    FPOSClientID: integer;
    FItemID: integer;
    FEan: string;
    FUnit: string;
    FVatRate: Currency;
    FLineStatus: ShortInt;
    FQty: Extended;
    FSalesPricePrUnit: Currency;
    FLineDiscount: Currency;
    FInvoiceDiscount: Currency;
    FLineAmount: Currency;
    FLineAmountIncVat: Currency;
    FLinePriceIncDepositAndVat: Currency;
  end;


  // Used for payment from the REVENUE file
  TBonPayment = record
    FBonID: integer;
    FPosClientID: integer;
    FLineNo: SmallInt;
    FDateTime: TDateTime;
    FPaydAmount: Currency;
    FPaymMethod: SmallInt;
    FMOP: SmallInt;
    MOP_ADDX: SmallInt;
    MOP_ADDY: SmallInt;
    ROP: SmallInt;
    procedure Clear;
  end;

  TItem = class(TObject)
    private
      FItemID: integer;
      BarcodeList: TList<string>;
      FPromotions: TDictionary<integer,ShortInt>;  // PromotionID, Level
      FPromotionCount: integer;
      FAllowPriceChange: boolean;
      FNegativQty: boolean;
      procedure AddBarCodes(inBarcode: string);
      function CalcPricIncVat: currency;
      function IsPriceChangeAllowed: string;
    public
      Text: string;
      ItemGrp: string;
      DSKGrp: string;
      VatRate: Currency;
      PriceExVat: Currency;
      procedure AddPromotion(PromoID: integer; Level:ShortInt);
      function GetBarcodeList: TList<string>;
      procedure SetNegativQty(isNegativ: ShortInt);
      constructor Create(ItemID: integer); overload;
      destructor Destroy; override;
      procedure ClearBarcodes;
      property ItemID: integer read FItemID;
      property Barcode: string write AddBarCodes;
      property PriceIncVat: Currency read CalcPricIncVat;
      property PromotionCount: integer read FPromotionCount;
      property Promotions: TDictionary<integer,ShortInt> read FPromotions;
      property AllowPriceChange: boolean read FAllowPriceChange write FAllowPriceChange;
      property AllowPriceChangeYesNo: string read IsPriceChangeAllowed;
      property NegativQty: boolean read FNegativQty;
  end;

  TPromotionLine = record
    ItemID: integer;
    Qty: Extended;
    PromPrice: Currency;
    NormalPrice: Currency;
    Level: ShortInt;
  end;

  TPromotion = class
    private
      FLines: TDictionary<Integer,TPromotionLine>;
      FGroups: TDictionary<ShortInt,ShortInt>;
      FLastLineLevel: ShortInt;
      FPromotionID: integer;
      FFromDate: TDate;     //YYYYMMDDhhmm
      FToDate: TDate;      //YYYYMMDDhhmm
      FPromoType: TPromoType;
      FQuantity: Extended;  //QUA bara br�kt vi� Type 2
      FTotalNormalPrice: Extended;
      function GetFromDate: string;
      function GetToDate: string;
    public
      Name: string;  // max 20 chars
      TotalPromoPrice: Extended; //AMT
      procedure AddLine(Line: TPromotionLine);
      function GetLines: TDictionary<Integer,TPromotionLine>;
      function GetPromoGrp: TDictionary<ShortInt,ShortInt>;
      procedure CalcPromoLinePrice;
      Constructor Create(PromotionID: integer); overload;
      Destructor Destroy; override;
      property PromotionID: integer read FPromotionID;
      property FromDate: TDate write FFromDate;
      property ToDate: TDate write FToDate;
      property FromDateAsStr: string read GetFromDate;
      property ToDateAsStr: string read GetToDate;
      property PromoType: TPromoType read FPromoType;
      property TriggerQty: Extended read FQuantity;
  end;

const
    BaseDataAreaID = 1;

var

  LastART_MUTID: integer;

implementation

uses
  FMX.Dialogs
  , System.SysUtils
  , DateUtils, Math
  ;

{ TItem }

procedure TItem.AddBarCodes(inBarcode: string);
begin
    if BarcodeList = nil then
        BarcodeList:=TList<string>.Create;

    BarcodeList.Add(inBarcode);
end;


procedure TItem.AddPromotion(PromoID: integer; Level:ShortInt);
begin
    inc(FPromotionCount);

    if not Assigned(FPromotions) then
       FPromotions:=TDictionary<integer,ShortInt>.Create;

    FPromotions.Add(PromoID,Level);
end;

function TItem.CalcPricIncVat: currency;
begin
    result:=PriceExVat*((100+VatRate)/100);

    // Um v�ran er sett upp til negativqty, so ger pr�sin negativan. FuelPOS kl�rar ikki at hondtera negativ qty.
    if NegativQty = True then
        result:=result * -1;
end;

procedure TItem.ClearBarcodes;
begin
    if BarcodeList <> nil then
       BarcodeList.Clear;
end;

constructor TItem.Create(ItemID: integer);
begin
    inherited Create;
    FItemID:=ItemID;
    FPromotionCount:=0;
    FPromotions:= TDictionary<integer,ShortInt>.Create;
end;


destructor TItem.Destroy;
begin
  if (FPromotions <> nil) then
      FPromotions.Free;
  if (BarcodeList <> nil) then
      BarcodeList.Free;

  inherited;
end;

function TItem.GetBarcodeList: TList<string>;
begin
    result:=BarcodeList;
end;

function TItem.IsPriceChangeAllowed: string;
begin
    if FAllowPriceChange = True then
      result:= 'YES'
    else
      result:= 'NO';
end;

procedure TItem.SetNegativQty(isNegativ: ShortInt);
begin
    if isNegativ = 1 then
      FNegativQty:=True
    else
      FNegativQty:=False;
end;

{ TPromotion }

procedure TPromotion.AddLine(Line: TPromotionLine);
begin
    if (Line.Qty > 1) then     //and (FPromoType = ptTimeBased) then
    begin
      if (FLines.Count > 0) then
        FPromoType:=ptMixArticle
      else
        FPromoType:=ptQtyBased;        // Type 2 = price with quantity > 1

      FQuantity:=Line.Qty;
    end;

    FGroups.AddOrSetValue(Line.Level,trunc(Line.Qty));

    if (Line.Level > FLastLineLevel) and (FLastLineLevel <> 0) then
    begin
       FPromoType:=ptGroup;        // ptGroup = MixMatch
    end;

    FLines.Add(Line.ItemID,Line);
    FTotalNormalPrice:=FTotalNormalPrice+(Line.Qty*Line.NormalPrice);
    FLastLineLevel:=Line.Level;
end;


procedure TPromotion.CalcPromoLinePrice;
var
  //Discount: Currency;
  DiscPercent: Extended;
  PromoItemID: Integer;
  PromoLine: TPromotionLine;
  CalcPromoPrice: Currency;
  TotalCalcPromPrice: Currency;
begin
    TotalCalcPromPrice:=0;
    //Discount:=FTotalNormalPrice-TotalPromoPrice;
    DiscPercent:=TotalPromoPrice/(FtotalNormalPrice/100);
    PromoItemID:=0;

    for PromoItemID in FLines.Keys do
    begin
        Flines.TryGetValue(PromoItemID,PromoLine);
        CalcPromoPrice:= PromoLine.NormalPrice*(DiscPercent/100);
        PromoLine.PromPrice:=CalcPromoPrice;
        FLines.AddOrSetValue(PromoItemID,PromoLine);
        TotalCalcPromPrice:=TotalCalcPromPrice+CalcPromoPrice*PromoLine.Qty;
    end;

    // Um kalkulati�nin pr linju ikki stemmar vi� samla�a tilbo�i (avrundingarfeilur)
    if SameValue(TotalCalcPromPrice, TotalPromoPrice, 0.01) = false then
    begin
       PromoLine.PromPrice:=PromoLine.PromPrice+(TotalPromoPrice-TotalCalcPromPrice);
       FLines.AddOrSetValue(PromoItemID,PromoLine);
    end;

end;

constructor TPromotion.Create(PromotionID: integer);
begin
    inherited Create;
    FPromoType:=ptTimeBased;  // Type 1 = price pr unit
    FLines:= TDictionary<Integer,TPromotionLine>.Create;
    FGroups:=TDictionary<ShortInt,ShortInt>.Create;
    FLastLineLevel:=0;
    FPromotionID:=PromotionID;
end;



destructor TPromotion.Destroy;
begin
    FLines.Free;
    FGroups.Free;
    inherited;
end;

function TPromotion.GetFromDate: string;
begin
    //result:=DateToStr(FFromDate);
    result:= IntToStr(YearOf(FFromDate));
    result:=result+MonthAsString(MonthOf(FFromDate));
    result:=result+DayAsString(DayOfTheMonth(FFromDate));
    result:=result+'0000';
end;

function TPromotion.GetLines: TDictionary<Integer,TPromotionLine>;
begin
    result:=FLines;
end;

function TPromotion.GetPromoGrp: TDictionary<ShortInt, ShortInt>;
begin
    result:=FGroups;
end;

function TPromotion.GetToDate: string;
begin
    //result:=DateToStr(FToDate);
    result:= IntToStr(YearOf(FToDate));
    result:=result+MonthAsString(MonthOf(FToDate));
    result:=result+DayAsString(DayOfTheMonth(FToDate));
    result:=result+'2359';
end;

{ TBonPayment }

procedure TBonPayment.Clear;
begin
    FBonID:= -1;
    FPosClientID:= -1;
    FLineNo:= -1;
    FDateTime:= 0;
    FPaydAmount:= 0;
    FPaymMethod:= -1;
    FMOP:=0;
    MOP_ADDX:=0;
    MOP_ADDY:=0;
    ROP:=0;
end;

end.
