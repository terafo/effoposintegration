unit TeraFunkUnit;

interface
uses
  Windows
  , SysUtils
  , Classes
  , FMX.Dialogs
  , FMX.Forms
  , Winsock
  , psAPI
  , StrUtils
  , IBX.IBDatabase
  , IBX.IBQuery
  , System.Generics.Collections
  , System.DateUtils
  ;

type
   TEntity = record
   strict Private
     FValue: Word;
     procedure SetValue(Value: string);
   public
     Position: ShortInt;
     Length: ShortInt;
     property Value: Word read FValue;
     property ValueAsStr: string write SetValue;
   end;



  TTimar = record
  KL00_06: integer;
  KL06_07: integer;
  KL07_08: integer;
  KL08_09: integer;
  KL09_10: integer;
  KL10_11: integer;
  KL11_12: integer;
  KL12_13: integer;
  KL13_14: integer;
  KL14_15: integer;
  KL15_16: integer;
  KL16_17: integer;
  KL17_18: integer;
  KL18_19: integer;
  KL19_20: integer;
  KL20_21: integer;
  KL21_22: integer;
  KL22_23: integer;
  KL23_00: integer;
end;

var
    Timar: TTimar;
    Debug: boolean;

  procedure TelAvgrPrTima(Klokkan: TTime);

  // DateTime Utils
  function ConvertStrToDateTime(sDateTime, sDateFormat: string):TDateTime;
  function FormateraDato(sDato, DatoFormat: string): TDate;
  function FormatDateToStr(inDate: TDate; Format: string):string;
  function DayAsString(Day: word):string;
  function MonthAsString(Month: Word): string;
  Function DayOfWeekFo(Date: TDate):integer;
  // End DateTimeUtils
  Function CTRLDown: Boolean;
  function ShiftDown : boolean;
  Function SetStringLongd(Strongur: string; longd: integer; TeknFramm: Boolean; Tekn: Char):string;
  Function SetMaxStringLongd(Strongur: string; longd: integer):string;
  function getStringFromString(s, sDeliminator: string; iDelNumberFrom,iDelNumberTo: integer): string;
  function GetQuotedStringFromString(s, sDeliminator, QuoteChar: string; iDelNumberFrom,iDelNumberTo: integer):string;
  procedure ParseLine(sLine, Delimiter, QuoteChar: string; var Result: TQueue<string>);
  function HeintaTeknSepareraFelt(Strongur, Separator, Qualifier: string; FeltNr: integer): string;
  function KannaFilPath(Path: string): Boolean;
  function UmdoypUmFilurFinnist(FilNavn:string): string;
  function SlettaFilExtension(FilNavn: String): string;
  function EAN13SetCheckCiffur(inntal: string):string;
  function ToEan(InString: string):string;
  function StringToBool(InString:string):boolean;
  //function BaraNummarTekn(inString:string):string;
  function FjernaReturnLinjuSkift(instring:string): string;
  procedure GoymTextFil(Tekstur,FilNavn: string);
  function KannaUmVektVara(EAN, Eind: string): string;
  procedure LogFile(Sender :TObject; Bod: string; UseDateInFileName: boolean = false);
  //Function GetIPAddress():String;

  Function RemoveFOandDKChars(InString: String):string;
  Function ReplaceChar(InChar: char): char;
  function RemoveAllNonAscii(Instring: string) :string;
  Function RemoveChar(RemoveChar: char; inString: String): string;
  //function TColorToHex(Color : TColor) : string;
  //function HexToTColor(sColor : string) : TColor;
  function SetVariableInFileName(OrgFileName: string):string;
  function SetIDInFileName(OrgFileName, ID: string):string;
  function RemoveIDInFileName(OrgFileName: string):string;
  function GetIDFromFileName(OrgFileName: string):string;
  function GetProgramVersion(sFileName:string): string;
  //function GetColumnIndex(DBGrid: TDBGrid; FieldName: string):Integer;
  function IntToStrDef(i: integer; default: string):string;
  function GetCustomerIDFromFileName(FileName: string):string;
  function CreateQuery(db: TIBDataBase; Trans: TIBTransaction; SqlCmd: string): TIBQuery;
  function RemoveCharFromString(Separator: char;input: string):string;
  function RoundEx(const AInput, APlaces: Extended): Currency;

  //Function WeekOfYear(ADate : TDateTime): word;
  //var
  //Gult: TColor = $7EEFFC;
  //Gront: TColor = $AEFFAB;
  //DeBugStrongur: TstringList;

implementation

uses
    Math;


function DayAsString(Day: word):string;
begin
    if (Day < 10) then
      result:='0'+IntToStr(Day)
    else
      result:=IntToStr(Day);
end;

function MonthAsString(Month: Word):string;
begin
    if (Month < 10) then
      result:='0'+IntToStr(Month)
    else
      result:=IntToStr(Month);
end;

function RoundEx(const AInput, APlaces: Extended): Currency;
var
  k: extended;
  temp: Currency;
begin
  k := power(10, APlaces);
  result:= round(AInput*k)/k;
  temp:=AInput-result;

  if (temp) = 0.005 then
    result:=result+0.01;
end;

{function WeekOfYear(ADate : TDateTime) : word;
var
  day : word;
  month : word;
  year : word;
  FirstOfYear : TDateTime;
begin
  DecodeDate(ADate, year, month, day);
  FirstOfYear := EncodeDate(year, 1, 1);
  Result := Trunc(ADate - FirstOfYear) div 7 + 1;
end;
}


function CreateQuery(db: TIBDataBase; Trans: TIBTransaction; SqlCmd: string): TIBQuery;
var
    Query: TIBQuery;
begin
    Query:=TIBQuery.Create(nil);
    try
        Query.Database:=db;
        Query.Transaction:=Trans;
        Query.SQL.Add(SqlCmd);
        result:=Query;
    except
        Query.Free;
        result:=nil;
    end;
end;

function RemoveCharFromString(Separator: char; input: string):string;
begin
    while (pos(Separator,input) > 0) do
    begin
        delete(input,pos(Separator,input),1);
    end;

    result:=input;
end;




function IntToStrDef(i: integer; default: string):string;
begin
   try
      result:=intToStr(i)
   except
      result:=default;
   end;
end;

{function GetColumnIndex(DBGrid: TDBGrid; FieldName: string):Integer;
var
  i: Integer;
begin
  result:=-1;

  for i := 0 to DBGrid.Columns.Count - 1 do
  begin
    if DBGrid.Columns[i].FieldName = FieldName then
      result := i;
  end;
end; }


function GetProgramVersion(sFileName:string): string;
var
  VerInfoSize: DWORD;
  VerInfo: Pointer;
  VerValueSize: DWORD;
  VerValue: PVSFixedFileInfo;
  Dummy: DWORD;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(sFileName), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(sFileName), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
  begin
    Result := IntToStr(dwFileVersionMS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionMS and $FFFF);
    Result := Result + '.' + IntToStr(dwFileVersionLS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionLS and $FFFF);
  end;
  FreeMem(VerInfo, VerInfoSize);
end;


function SetVariableInFileName(OrgFileName: string):string;
var
  TempFileName: string;
begin
    // Funkti�n er ikki li�ug
    TempFileName:= ExtractFileName(OrgFileName);

end;

function GetIDFromFileName(OrgFileName: string):string;
var
  TempFileID: string;
begin
    TempFileID:= ExtractFileName(OrgFileName);
    result:=copy(TempFileID,pos('#',TempFileID),length(TempFileID)-1);
end;

function GetCustomerIDFromFileName(FileName: string):string;
begin
    result:=getStringFromString(FileName,'_',1,2);
end;


function SetIDInFileName(OrgFileName, ID: string):string;
var
  TempFileName, FileExt, TempFilePath: string;
begin
    TempFileName:= ExtractFileName(OrgFileName);
    TempFilePath:=ExtractFilePath(OrgFileName);
    FileExt:= ExtractFileExt(TempFileName);
    TempFileName:=copy(TempFileName,0,pos(FileExt,TempFileName)-1);

    TempFileName:=TempFileName+'#'+RemoveAllNonAscii(ID)+FileExt;
    result:=IncludeTrailingPathDelimiter(TempFilePath)+TempFileName;
end;

function RemoveIDInFileName(OrgFileName: string):string;
var
  TempFileName, FileExt, TempFilePath: string;
begin
    if pos('#',OrgFileName) > 0 then
    begin
        TempFileName:= ExtractFileName(OrgFileName);
        TempFilePath:=ExtractFilePath(OrgFileName);
        FileExt:= ExtractFileExt(TempFileName);
        TempFileName:=copy(TempFileName,0,pos('#',TempFileName)-1);
        result:=IncludeTrailingPathDelimiter(TempFilePath)+TempFileName+FileExt;
    end
    else
        result:=OrgFileName;
end;

{function TColorToHex(Color : TColor) : string;
begin
   Result :=
     IntToHex(GetRValue(Color), 2) +
     IntToHex(GetGValue(Color), 2) +
     IntToHex(GetBValue(Color), 2) ;
end; }

{function HexToTColor(sColor : string) : TColor;
begin
   Result :=
     RGB(
       StrToInt('$'+Copy(sColor, 1, 2)),
       StrToInt('$'+Copy(sColor, 3, 2)),
       StrToInt('$'+Copy(sColor, 5, 2))
     ) ;
end; }



Function RemoveChar(RemoveChar: char; inString: String): string;
begin
    if Pos(RemoveChar,inString) > 0 then
       delete(inString,Pos(RemoveChar,inString),1);

    result:=inString;
end;

Function RemoveFOandDKChars(InString: String):string;
var
    i: integer;
begin
    result:='';
    for i:= 1 to length(inString) do
    begin
        result:=result+ReplaceChar(InString[i]);
    end;
end;

function RemoveAllNonAscii(Instring: string) :string;
begin
    if (pos('/',Instring) > 0) then
    begin
         while (pos('/',Instring) > 0) do
         begin
             InString:=RemoveChar('/',InString);
         end;
    end;
    if (pos('\',Instring) > 0) then
    begin
         while (pos('\',Instring) > 0) do
         begin
             InString:=RemoveChar('\',InString);
         end;
    end;
    if (pos(':',Instring) > 0) then
    begin
         while (pos(':',Instring) > 0) do
         begin
             InString:=RemoveChar(':',InString);
         end;
    end;
    if (pos('.',Instring) > 0) then
    begin
         while (pos('.',Instring) > 0) do
         begin
             InString:=RemoveChar('.',InString);
         end;
    end;

    InString:=RemoveFOandDKChars(Instring);

    result:=InString;
end;

function ReplaceChar(InChar: char): char;
begin
    if inChar = '�' then result:='i'
    else if inChar = '�' then result:='a'
    else if inChar = '�' then result:='A'
    else if inChar = '�' then result:='o'
    else if inChar = '�' then result:='�'
    else if inChar = '�' then result:='d'
    else if inChar = '�' then result:='D'
    else if inChar = '�' then result:='o'
    else if inChar = '�' then result:='O'
    else if inChar = '�' then result:='u'
    else if inChar = '�' then result:='U'
    else if inChar = '�' then result:='y'
    else if inChar = '�' then result:='Y'
    else if inChar = '�' then result:='a'
    else if inChar = '�' then result:='A'
    else result:=Inchar;
end;



Function DayOfWeekFo(Date: TDate):integer;
begin
    result:=DayOfWeek(Date)-1;

    if result = 0 then
       result:=7;
end;

function KannaUmVektVara(EAN, Eind: string): string;
var
  temp: string;
begin
    try
        // Ger tey seinastu siffrini til 000000 �sta�in fyri pr�s � vektv�rum
        if (length(EAN) = 13) and (EAN[1] = '2') and (Eind = 'Stk') then
        begin
            temp:=copy(EAN,0,7);
            temp:=temp+'000000'; //+EAN[13];
            result:=temp;
        end
        else
          result:=EAN;
    except
        //showmessage('Feilur � strikukotuni');
        //MessageFormur.Bod('Feilur � strikukotuni',1);
    end;
end;


{function BaraNummarTekn(inString:string):string;
var
   i: integer;
   tempString: string;
begin
     for i:=0 to length(inString) do
     begin
         //if CharInSet(inString, ['0'..'9']) then

         if (inString[i] in ['0'..'9']) then
            tempString:=tempString+inString[i]
         else if (inString[i] = ',') or (inString[i] = '.') then
            tempString:=tempString+inString[i];
     end;

     result:=tempString;
end;   }


procedure TelAvgrPrTima(Klokkan: TTime);
begin
    if (Klokkan < StrToTime('06:00:00')) then
       Timar.KL00_06:=Timar.KL00_06+1
    else if (Klokkan >= StrToTime('06:00:00')) and (Klokkan < StrToTime('07:00')) then
       Timar.KL06_07:=Timar.KL06_07+1
    else if (Klokkan >= StrToTime('07:00')) and (Klokkan < StrToTime('08:00')) then
       Timar.KL07_08:=Timar.KL07_08+1
    else if (Klokkan >= StrToTime('08:00')) and (Klokkan < StrToTime('09:00')) then
       Timar.KL08_09:=Timar.KL08_09+1
    else if (Klokkan >= StrToTime('09:00')) and (Klokkan < StrToTime('10:00')) then
       Timar.KL09_10:=Timar.KL09_10+1
    else if (Klokkan >= StrToTime('10:00')) and (Klokkan < StrToTime('11:00')) then
       Timar.KL10_11:=Timar.KL10_11+1
    else if (Klokkan >= StrToTime('11:00')) and (Klokkan < StrToTime('12:00')) then
       Timar.KL11_12:=Timar.KL11_12+1
    else if (Klokkan >= StrToTime('12:00')) and (Klokkan < StrToTime('13:00')) then
       Timar.KL12_13:=Timar.KL12_13+1
    else if (Klokkan >= StrToTime('13:00')) and (Klokkan < StrToTime('14:00')) then
       Timar.KL13_14:=Timar.KL13_14+1
    else if (Klokkan >= StrToTime('14:00')) and (Klokkan < StrToTime('15:00')) then
       Timar.KL14_15:=Timar.KL14_15+1
    else if (Klokkan >= StrToTime('15:00')) and (Klokkan < StrToTime('16:00')) then
       Timar.KL15_16:=Timar.KL15_16+1
    else if (Klokkan >= StrToTime('16:00')) and (Klokkan < StrToTime('17:00')) then
       Timar.KL16_17:=Timar.KL16_17+1
    else if (Klokkan >= StrToTime('17:00')) and (Klokkan < StrToTime('18:00')) then
       Timar.KL17_18:=Timar.KL17_18+1
    else if (Klokkan >= StrToTime('18:00')) and (Klokkan < StrToTime('19:00')) then
       Timar.KL18_19:=Timar.KL18_19+1
    else if (Klokkan >= StrToTime('19:00')) and (Klokkan < StrToTime('20:00')) then
       Timar.KL19_20:=Timar.KL19_20+1
    else if (Klokkan >= StrToTime('20:00')) and (Klokkan < StrToTime('21:00')) then
       Timar.KL20_21:=Timar.KL20_21+1
    else if (Klokkan >= StrToTime('21:00')) and (Klokkan < StrToTime('22:00')) then
       Timar.KL21_22:=Timar.KL21_22+1
    else if (Klokkan >= StrToTime('22:00')) and (Klokkan < StrToTime('23:00')) then
       Timar.KL22_23:=Timar.KL22_23+1
    else if (Klokkan >= StrToTime('23:00')) and (Klokkan < StrToTime('23:59:59')) then
       Timar.KL23_00:=Timar.KL23_00+1
end;



function getStringFromString(s, sDeliminator: string; iDelNumberFrom,iDelNumberTo: integer): string;
var sNew : string;
  i : integer;
begin
  for i := 0 to iDelNumberFrom -1 do
  begin
    s := copy(s,pos(sDeliminator,s)+1,length(s));
  end;

  if iDelNumberTo > 0 then
  begin
    sNew := '';
    for i := iDelNumberFrom to iDelNumberTo-1 do
    begin
      sNew := sNew + copy(s,1,pos(sDeliminator,s));
      s := copy(s,pos(sDeliminator,s)+1,length(s));
    end;
    result := copy(sNew,1,length(sNew)-1);
  end
  else
    result := s;
end;


procedure ParseLine(sLine, Delimiter, QuoteChar: string; var Result: TQueue<string>);
var
  PosDelimiter, PosQuoteChar: integer;
  FieldValue, tempString: string;
  ParsingOk: Boolean;
begin
    tempString:=sLine;

    while length(tempString) > 0 do
    begin
        //ParsingOK:=False;
        PosDelimiter:=pos(Delimiter, tempString );
        PosQuoteChar:=pos(QuoteChar, tempString);

        {if (PosQuoteChar = 0) then
        begin


        end
        else} if (PosQuoteChar < PosDelimiter) and (tempString[1] = QuoteChar[1]) then
        begin
           Delete(tempString,1,length(QuoteChar));
           //Delete(tempString,1,length(QuoteChar));
           PosQuoteChar:=pos(QuoteChar, tempString);
           FieldValue:=Copy(tempString,1,PosQuoteChar -1);
           Delete(tempString,1,PosQuoteChar+length(QuoteChar));
           ParsingOK:=True;
        end
        else
        begin
            if (PosDelimiter = 0) then
            begin
                FieldValue:=Copy(tempString,1,length(tempString));
                Delete(tempString,1,length(tempString));
                ParsingOK:=True;
            end
            else
            begin
                FieldValue:=Copy(tempString,1,PosDelimiter -1);
                Delete(tempString,1,PosDelimiter);
                ParsingOK:=True;
            end;
        end;

        result.Enqueue(FieldValue);

        // Used to enshure that we don't end in an endless loop.
        if ParsingOK = False then
           Break
    end;
end;

function GetQuotedStringFromString(s, sDeliminator, QuoteChar: string; iDelNumberFrom,iDelNumberTo: integer): string;
var
  sNew : string;
  i : integer;
  QuotedField: boolean;
begin
  QuotedField:= False;

  for i := 0 to iDelNumberFrom -1 do
  begin
    s := copy(s,pos(sDeliminator,s)+1,length(s));
  end;

  if iDelNumberTo > 0 then
  begin
    sNew := '';
    for i := iDelNumberFrom to iDelNumberTo-1 do
    begin
      //if s then

      while (s[1] = QuoteChar) do
      begin
          Delete(s,1,1);
          QuotedField:=True;
      end;

      if QuotedField = True then
        sNew := sNew + copy(s,1,pos(QuoteChar,s))
      else
        sNew := sNew + copy(s,1,pos(sDeliminator,s));
      //s := copy(s,pos(sDeliminator,s)+1,length(s));
    end;
    result := copy(sNew,1,length(sNew)-1);
  end
  else
    result := s;
end;


function HeintaTeknSepareraFelt(Strongur, Separator, Qualifier: string; FeltNr: integer): string;
var
  i: integer;
  tempFelt: string;
begin
     for i:= 1 to FeltNr do
     begin
     //if FeltNr = 1 then
     //begin
        if Strongur[1] = Separator then
        begin
           Delete(Strongur,1,1);
        end;

        if Strongur[1] = Qualifier then
        begin
           if pos(separator,strongur) > 0 then
              tempFelt:=copy(Strongur,2,pos(Qualifier+Separator,Strongur)-2)
           else
              tempFelt:=copy(Strongur,2,posEx(Qualifier,Strongur,2)-2);
              
           Delete(Strongur,1,pos(Qualifier+Separator,Strongur));
        end  
        else
        begin
           tempFelt:=copy(Strongur,0,pos(separator,Strongur)-1);
           Delete(Strongur,1,pos(separator,Strongur)-1);
        end;   
     end;

     result:=tempFelt;
end;



function FormateraDato(sDato, DatoFormat: string): TDate;
var
  Ar, Mdr, Dagur, testTekn: string;
  YearLongd: integer;
  tempShortDateFormat: string;
  DateFormat: TFormatSettings;
begin
   try
       tempShortDateFormat:= DateFormat.ShortDateFormat;

       try
          TestTekn:=Copy(DatoFormat, pos('y',DatoFormat),4);

          if TestTekn = 'yyyy' then
          begin
             YearLongd:=4;
             DateFormat.ShortDateFormat:='yyyy-mm-dd';
          end
          else
          begin
             YearLongd:=2;
             DateFormat.ShortDateFormat:='yy-mm-dd';
          end;

          Ar:=Copy(sDato, pos('y',DatoFormat), YearLongd);
          Mdr:=Copy(sDato, pos('m',DatoFormat), 2);
          Dagur:=Copy(sDato, pos('d',DatoFormat), 2);


          //ShortDateFormat:='yy-mm-dd';

          if (DatoFormat = 'ddmmyy') or (DatoFormat = 'ddmmyyyy') then
             result:=StrToDate(Dagur+'-'+Mdr+'-'+Ar)
          else
             result:=StrToDate(Ar+'-'+Mdr+'-'+Dagur);
       except
          result:=0;
       end;
   finally
       DateFormat.ShortDateFormat:=tempShortDateFormat;
   end;
end;

function OccurrencesOfChar(const S: string; const C: char): integer;
var
  i: Integer;
begin
  result := 0;
  for i := 1 to Length(S) do
    if S[i] = C then
      inc(result);
end;


function ConvertStrToDateTime(sDateTime, sDateFormat: string):TDateTime;
var
  EntityList: TDictionary<Char,TEntity>;
  Entity: TEntity;
  Key: Char;
  Year: Word;
  Month: Word;
  Day: Word;
  Hour: Word;
  Minute: Word;
  Second: Word;
begin
   EntityList:=TDictionary<Char,TEntity>.Create;
   try
      EntityList.Add('Y', Entity);
      EntityList.Add('M', Entity);
      EntityList.Add('D', Entity);
      EntityList.Add('h', Entity);
      EntityList.Add('m', Entity);
      EntityList.Add('s', Entity);

      for Key in EntityList.Keys do
      begin
          Entity.Position:=Pos(Key,sDateFormat);
          Entity.Length:=OccurrencesOfChar(sDateFormat,Key);
          Entity.ValueAsStr:=Copy(sDateTime,Entity.Position,Entity.Length);
          EntityList.AddOrSetValue(Key,Entity);
      end;

      Key:='Y';
      EntityList.TryGetValue(Key,Entity);
      Year:= Entity.Value;

      Key:='M';
      EntityList.TryGetValue(Key,Entity);
      Month:= Entity.Value;

      Key:='D';
      EntityList.TryGetValue(Key,Entity);
      Day:= Entity.Value;

      Key:='h';
      EntityList.TryGetValue(Key,Entity);
      Hour:= Entity.Value;

      Key:='m';
      EntityList.TryGetValue(Key,Entity);
      Minute:= Entity.Value;

      Key:='s';
      EntityList.TryGetValue(Key,Entity);
      Second:= Entity.Value;

      result:=EncodeDateTime(Year,Month,Day,Hour,Minute,Second,0);
   finally
      EntityList.Free;
   end;
end;


function FormatDateToStr(inDate: TDate; Format: string):string;
var
  LYear: word;
  LMonth: word;
  LDay: word;
begin
    // Fyribils kann funkti�ni bara returnara eitt format.
    LYear:=YearOf(inDate);
    LMonth:=MonthOf(inDate);
    LDay:=DayOf(inDate);

    if Format = 'YYYYMMDD' then
    begin
       result:=intToStr(Lyear);
       result:=result+MonthAsString(LMonth);
       result:=result+DayAsString(LDay);
    end
    else
    begin
       result:=DayAsString(LDay);
       result:=result+MonthAsString(LMonth);
       result:=result+intToStr(Lyear);
    end;
end;


Function CTRLDown: Boolean;       // Kanna um CTRL tasturin er ni�ri
var
  State: TKeyboardState;
begin
  GetkeyBoardState(State);
  Result:=((State[VK_CONTROL] And 128) <> 0);
end;

function ShiftDown : Boolean;
var
   State : TKeyboardState;
begin
   GetKeyboardState(State) ;
   Result := ((State[vk_Shift] and 128) <> 0) ;
end;


Function SetStringLongd(Strongur: string; longd: integer; TeknFramm: Boolean; Tekn: Char):string;
var
  i: integer;
  TempStrongur: string;
begin
     if length(Strongur) >= longd then
        SetLength(Strongur,longd)
     else
     begin
         for i:= length(Strongur) to longd-1 do
         begin
              TempStrongur:=TempStrongur+Tekn;
         end;

         if TeknFramm then
            strongur:=TempStrongur+Strongur
         else
            strongur:=Strongur+TempStrongur;
     end;

     result:= strongur;
end;

Function SetMaxStringLongd(Strongur: string; longd: integer):string;
begin
    // klippur strongin av um hann er ov langur, annars letur hann vera sum hann er.
     if length(Strongur) >= longd then
        SetLength(Strongur,longd);

     result:= strongur;
end;


function KannaFilPAth(Path: string): Boolean;
var
  i, tempLength: integer;
  tempPath, tempString, inPath: string;
begin
    // Kannar um mappan finnist, um ikki so ger eina mappu
     try
        //result:=false;
        tempPath:='';
        tempString:='';
        //tempLength:=0;
        inPath:=Path;

        if DirectoryExists(Path) then
        begin
           result:=True;
           exit;
        end
        else
        begin
           for i:= 0 to 20 do
           begin
              if (pos('\',inPath) > 0) then
              begin
                  tempString:=copy(inPath,0,pos('\',inPath));

                  if i = 0 then
                  begin
                      // Kannar um drevi t.d "c:\" finnist
                      if not (DirectoryExists(tempString)) then
                      begin
                          result:=false;
                          Exit;
                      end;

                      tempPath:=TempPath+tempstring;
                  end
                  else
                  begin
                      tempPath:=TempPath+tempstring;

                      if not DirectoryExists(TempPath) then
                         CreateDir(TempPath);
                  end;

                  tempLength:=length(tempString);

                  Delete(inPath,1,templength);
              end
              else
              begin
                  if length(inpath) > 0 then
                     tempPath:=tempPath+inpath;

                  if not DirectoryExists(TempPath) then
                     CreateDir(TempPath);
                  break;
              end;
           end;

           result:=true;

           //   SetCurrentDir('C:\PDFFakturar');

        end;
     except
        result:=false;
     end;
end;


// Um f�lurin finnist ver�ur hann umdoyptur til filnavn(tal)
function UmdoypUmFilurFinnist(FilNavn:string): string;
var
    Path, tempFilNavn, tempFilExt: String;
    i: integer;
begin
    {if FileExists(Filnavn) then
    begin
       i:=i+1;
       Path:=ExtractFilePath(Filnavn);
       tempFilNavn:=ExtractFileName(Filnavn);
       tempFilExt:=ExtractFileExt(Filnavn);
       UmdoypFilurFinnist(IncludeTrailingPathDelimiter(Path)+tempFilNavn+'('+intToStr(i)+')'+tempFilExt,'',i);
    end
    else
        result:=filnavn;
    }
    i:=0;

    while FileExists(Filnavn) do
    begin
       i:=i+1;
       Path:=ExtractFilePath(Filnavn);
       tempFilNavn:=SlettaFilExtension(ExtractFileName(Filnavn));

       if pos('(',tempFilNavn) = Length(tempFilNavn)-2 then
          Delete(tempFilNavn,pos('(', tempFilNavn),3)
       else if pos('(',tempFilNavn) = Length(tempFilNavn)-3 then
          Delete(tempFilNavn,pos('(', tempFilNavn),4);


       tempFilExt:=ExtractFileExt(Filnavn);
       Filnavn:=IncludeTrailingPathDelimiter(Path)+tempFilNavn+'('+intToStr(i)+')'+tempFilExt;
    end;

    result:=filnavn;
end;

function SlettaFilExtension(FilNavn: String): string;
//var
//  StringLongd, ExtLongd: integer;
begin
    result:=copy(FilNavn,0,LastDelimiter('.',FilNavn)-1);
    {StringLongd:=Length(FilNavn);
    ExtLongd:=StringLongd-(pos('.',FilNavn))+1;
    Delete(FilNavn,(pos('.',FilNavn)),ExtLongd);
    result:=FilNavn;
    }
end;

function EAN13SetCheckCiffur(inntal: string):string;
var
   ciffur13: string;
   tal1, tal2, tal3, tal4, tal5, tal6, tal7, tal8, tal9, tal10, tal11, tal12: integer;
   SumTal : integer;

   HeilTal, TruncTempTal, TempTal, kontrolciffur: double;
begin
     //inntal:= '571234500009';
     tal1:= strtoint(inntal[1])*1;
     tal2:= strtoint(inntal[2])*3;
     tal3:= strtoint(inntal[3])*1;
     tal4:= strtoint(inntal[4])*3;
     tal5:= strtoint(inntal[5])*1;
     tal6:= strtoint(inntal[6])*3;
     tal7:= strtoint(inntal[7])*1;
     tal8:= strtoint(inntal[8])*3;
     tal9:= strtoint(inntal[9])*1;
     tal10:= strtoint(inntal[10])*3;
     tal11:= strtoint(inntal[11])*1;
     tal12:= strtoint(inntal[12])*3;
     SumTal:= tal1+tal2+tal3+tal4+tal5+tal6+tal7+tal8+tal9+tal10+tal11+tal12;
     TempTal:=SumTal/10;
     TruncTempTal:=Trunc(TempTal);
     if not (TruncTempTal = TempTal) then
     begin
        HeilTal:= (TruncTempTal)*10+10;
        kontrolciffur:= HeilTal-SumTal;
     end
     else
         Kontrolciffur:=0;

     ciffur13:=floattostr(kontrolciffur);
     inntal[13]:=ciffur13[1];

     result:= inntal;
end;

function ToEan(InString: string):string;
var
   TempString, BarCodeString: string;
   OddInt, EvenInt, Teljari: Integer;
begin
      BarCodeString:='';
//      Showmessage(InString+' '+IntToStr(Length(InString)));
//      Assert(Length(InString)=13, 'Feilur: ToEan fekk ikki 13 tekn');

      if length(InString) <> 13 then
         exit;

      TempString:=InString;

      BarCodeString:=BarCodeString+(Chr(Ord(TempString[1])-15));    // Flag 1 = char - 15 chars ni�ur
      BarCodeString:=BarCodeString+(Chr(Ord(TempString[2])+48));    // Flag 2 = char + 48 chars upp

      // Fyrsti datablokkur � 5 tekn.
      case TempString[1] of
           '0':
           begin
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[3])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[4])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[5])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[6])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[7])));
           end;
           '1':
           begin
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[3])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[4])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[5])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[6])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[7])+16));
           end;
           '2':
           begin
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[3])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[4])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[5])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[6])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[7])+16));
           end;
           '3':
           begin
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[3])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[4])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[5])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[6])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[7])));
           end;
           '4':
           begin
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[3])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[4])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[5])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[6])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[7])+16));
           end;
           '5':
           begin
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[3])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[4])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[5])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[6])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[7])+16));
           end;
           '6':
           begin
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[3])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[4])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[5])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[6])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[7])));
           end;
           '7':
           begin
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[3])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[4])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[5])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[6])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[7])+16));
           end;
           '8':
           begin
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[3])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[4])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[5])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[6])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[7])));
           end;
           '9':
           begin
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[3])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[4])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[5])));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[6])+16));
                BarCodeString:=BarCodeString+(Chr(Ord(TempString[7])));
           end;
      end;
      // Centre flag
      BarcodeString:=BarCodeString+Chr(124);

      // Seinri data blokkur � 5 chars, flutt 32 chars upp
      BarCodeString:=BarCodeString+(Chr(Ord(TempString[8])+32));
      BarCodeString:=BarCodeString+(Chr(Ord(TempString[9])+32));
      BarCodeString:=BarCodeString+(Chr(Ord(TempString[10])+32));
      BarCodeString:=BarCodeString+(Chr(Ord(TempString[11])+32));
      BarCodeString:=BarCodeString+(Chr(Ord(TempString[12])+32));

      // Check character... sum av odd position * 3 + sum av even position +
      // ta� sum skal til so ta� gongur upp� 10 (odd og even eru b�tt um, vegna char 0 � string er nummar 1, odd, ikki even.)
      begin
          OddInt:=0;
          EvenInt:=0;
          for Teljari:=1 to 12 do
          begin
                if  Odd(Teljari) then EvenInt:=EvenInt+StrToInt(TempString[Teljari])
                else
                    OddInt:=OddInt+StrToInt(TempString[Teljari]);
          end;
          OddInt:=OddInt*3;
          Teljari:=OddInt+EvenInt;
          Teljari:=10-Teljari mod 10;
          if Teljari=10 then Teljari:=0;
          TempString:=IntToStr(Teljari);
//          ShowMessage(TempString);
//          ShowMessage(BarCodeString);
          BarCodeString:=BarCodeString+Chr(Ord(TempString[1])+64);
//          ShowMessage(BarCodeString);
      end;
      Result:=BarCodeString;
end;

function StringToBool(InString:string):boolean;
begin
     if UpperCase(InString) = UpperCase('TRUE') then
        result:=True
     else
        result:=False;
end;

function FjernaReturnLinjuSkift(instring:string): string;
var
  i: integer;
  tempStr: string;
begin
    tempStr:='';
    
    //Kannar um naka� tekn er minni enn char 13 og fjernar tey
    if (Pos(Chr(13),instring) > 0) or (pos(Chr(10),instring) > 0) then
    begin
        for i:=0 to length(instring) do
        begin
            if (instring[i] = chr(13)) then
                tempStr:=tempStr+' '
            else if (instring[i] > chr(13)) then
               tempStr:=tempStr+instring[i];
        end;
    end
    else
       tempStr:=instring;

    result:=tempstr;
end;


procedure GoymTextFil(Tekstur,FilNavn: string);
var
  tf: TextFile;
begin
    AssignFile(tf, FilNavn);

    if FileExists(FilNavn) then
        Append(tf)
    else
        Rewrite(tf);

   Write(tf, Tekstur+#13#10) ;
   CloseFile(tf) ; 
end;


procedure LogFile(Sender :TObject; Bod: string; UseDateInFileName: boolean = false);
var
  FilePath: string;
  FileName: string;
  FilePathAndName: string;
  LogFileName: string;
  LogFilur: TextFile;
begin
    // Prepare logfile
    if UseDateInFileName = True then
    begin
       FilePath:=ExtractFilePath(ParamStr(0));
       FileName:=ExtractFileName(ParamStr(0));   //
       FileName:=ChangeFileExt(FileName,'')+'_'+FormatDateToStr(Date,'YYYYMMDD');
       FilePathAndName:=IncludeTrailingPathDelimiter(FilePath)+FileName;
       LogFileName:= ChangeFileExt(FilePathAndName, '.log');
    end
    else
       LogFileName:= ChangeFileExt(ParamStr(0), '.log');

    AssignFile(LogFilur, LogFileName);

    if FileExists(LogFileName) then
       Append(LogFilur)
    else
       Rewrite(LogFilur);

    if Sender is TComponent then
       Writeln(LogFilur, DateTimeToStr(now) + ': '+(Sender as TComponent).Name+': '+Bod)
    else
       Writeln(LogFilur, DateTimeToStr(now) + ': '+Bod);

    if  Debug = True then
        Showmessage(Bod);

    CloseFile(LogFilur);    
end;

{18-11-2011 Function GetIPAddress():String;
type
  pu_long = ^u_long;
var
  varTWSAData : TWSAData;
  varPHostEnt : PHostEnt;
  varTInAddr : TInAddr;
  namebuf : Array[0..255] of char;
begin
  If WSAStartup($101,varTWSAData) <> 0 Then
  Result := 'No. IP Address'
  Else Begin
    gethostname(namebuf,sizeof(namebuf));
    varPHostEnt := gethostbyname(namebuf);
    varTInAddr.S_addr := u_long(pu_long(varPHostEnt^.h_addr_list^)^);
    //Result := 'IP Address: '+inet_ntoa(varTInAddr);
    Result := inet_ntoa(varTInAddr);
  End;
  WSACleanup;
end;
}

{Function GetIPAddress():String;
type
  pu_long = ^u_long;
var
  varTWSAData : TWSAData;
  varPHostEnt : PHostEnt;
  varTInAddr : TInAddr;
  namebuf : PAnsiChar;
begin
  If WSAStartup($101,varTWSAData) <> 0 Then
  Result := 'No. IP Address'
  Else Begin
    gethostname(namebuf,sizeof(namebuf));
    varPHostEnt := gethostbyname(namebuf);
    varTInAddr.S_addr := u_long(pu_long(varPHostEnt^.h_addr_list^)^);
    //Result := 'IP Address: '+inet_ntoa(varTInAddr);
    Result := inet_ntoa(varTInAddr);
  End;
  WSACleanup;
end;
}



{ TEntity }

procedure TEntity.SetValue(Value: string);
begin
    FValue:=StrToInt(Value);
end;

end.
