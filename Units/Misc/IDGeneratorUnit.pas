unit IDGeneratorUnit;

interface

uses
  SysUtils
  ,inifiles
  ;

type

 IIDGen = interface['{C8940DC4-3B7D-46A8-BDE1-EDBC733F9FD9}']
    function GetID: integer;
    function GetIdAsStr(Digits: ShortInt): string;
 end;

 TIDGen = class(TInterfacedObject,IIDGen)
 private
    FID: integer;
    FMaxID: integer;
    FIniFilePath: string;
    FIniFile: TIniFile;
    FDataAreaID: SmallInt;
    function GetID: integer;
    function GetIdAsStr(Digits: ShortInt): string;
    procedure LoadFromIniFile;
    procedure SaveToIniFile;
 public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    constructor Create(IniFile: string; DataAreaID: SmallInt; MaxValue: integer = 990); overload;
    constructor Create(InitialID: integer = 1); overload;
 end;

implementation

{ TIDGen }

procedure TIDGen.AfterConstruction;
begin
  inherited;
    LoadFromIniFile;
end;

procedure TIDGen.BeforeDestruction;
begin
  inherited;
   SaveToIniFile;
end;

constructor TIDGen.Create(InitialID: integer);
begin
    inherited Create;
    FMaxID:=-1;
    FID:=InitialID;
end;

constructor TIDGen.Create(IniFile: string; DataAreaID: SmallInt; MaxValue: integer = 990);
begin
   FIniFilePath:=IniFile;
   FDataAreaID:=DataAreaID;
   FMaxID:=MaxValue;
end;

function TIDGen.GetID: integer;
begin
    result:=FID;

    if (FMaxID > 0) and (FID >= FMaxID) then
    begin
       FID:=0;
    end
    else
        inc(FID);
end;

function TIDGen.GetIdAsStr(Digits: ShortInt): string;
begin
    result:= intToStr(GetID);

    if (length(result) < Digits) then
    begin
        while length(result) < Digits do
        begin
            result:='0'+result;
        end;
    end;
end;

procedure TIDGen.LoadFromIniFile;
begin
    FIniFile:=TIniFile.Create(FIniFilePath);
    try
        FID:=FIniFile.ReadInteger('Export Folder '+FDataAreaID.ToString,'Last_ART_MUTID',0);
    finally
        FIniFile.Free;
    end;
end;

procedure TIDGen.SaveToIniFile;
begin
    FIniFile:=TIniFile.Create(FIniFilePath);
    try
        FIniFile.WriteInteger('Export Folder '+FDataAreaID.ToString,'Last_ART_MUTID',FID);
    finally
        FIniFile.Free;
    end;
end;

end.
