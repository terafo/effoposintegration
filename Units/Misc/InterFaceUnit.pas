unit InterFaceUnit;

interface

uses SharedUnit;

type
  IUpdatePOS = interface(IInterface)
  ['{1A16FD92-A61A-45F3-AB67-9F590A7AC34F}']
    procedure UpdateItem(Item: TItem);
    procedure UpdatePromotion(Promo: TPromotion);
    procedure RemoveUpdatedItemsFromPriceQueue;
    procedure ExportAll;
    procedure DeleteAllCampaigns;
  end;

  IImportFromPOS = interface(IInterface)
  ['{4DB42E5E-1DA3-4D4D-B2F3-F61056F78CA6}']
    procedure ProcessCashStatements;
    procedure ProcessBonData;
  end;

implementation

end.
