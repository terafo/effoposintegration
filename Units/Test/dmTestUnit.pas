unit dmTestUnit;

interface

uses
  System.SysUtils
  , System.Classes
  ,FMX.Dialogs
  ;

type
  TdmTest = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FID: integer;
    function SaveStatementHeader(StatementID, POSClientID: integer; DateFrom, DateTo: TDateTime; EmplID: string): integer;
    function DoPaymentTypeExists(PaymTypeID: integer): boolean;
    function GetPaymentTypeByNodeName(NodeName: string): integer;
    procedure SavePayments(PaymentTypeID: integer; Amount: Currency);
  public
    { Public declarations }
  end;


implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

{ TDataModule1 }

procedure TdmTest.DataModuleCreate(Sender: TObject);
begin
    FID:=1;
end;

procedure TdmTest.DataModuleDestroy(Sender: TObject);
begin
    FID:=0;
    showmessage('destroy');
end;

function TdmTest.DoPaymentTypeExists(PaymTypeID: integer): boolean;
begin
   //
end;

function TdmTest.GetPaymentTypeByNodeName(NodeName: string): integer;
begin
    //
end;

procedure TdmTest.SavePayments(PaymentTypeID: integer; Amount: Currency);
begin
    //
end;

function TdmTest.SaveStatementHeader(StatementID, POSClientID: integer;
  DateFrom, DateTo: TDateTime; EmplID: string): integer;
begin
    //
end;

end.
