unit CashStatementsUnit;

interface

uses
  System.SysUtils
  , TeraFunkUnit
  , dmLogDataUnit
  , dmCaxhStatementsUnit
  , System.Generics.Collections
  , InterFaceUnit
  ;

type
  TShiftDocSection = (COUNTED_MONEY,SALES_MOP);

  TPayment = record
    PaymentMethodID: integer;
    PaymNodeName: string;
    TransAmount: Currency;
    CountedAmount: Currency;
    ShiftDocSetion: TShiftDocSection;
  end;



  TCashStateMentHeader = class
    private
      FID: integer;
      FDB: TdmCashStatements;
      FOpenDate: TDateTime;
      FCloseDate: TDateTime;
      FPayments: TDictionary<integer, TPayment>;
      FDataAreaID: SmallInt;
      //FCashStateMentCompleted: Boolean;
      function ConStrToDateTime(sDateTime: string): TDateTime;
      procedure SetOpenDate(sDateTime: string);
      procedure SetCloseDate(sDateTime: string);
      function SaveHeader: boolean;
      function SavePayments: boolean;
      procedure SetCashStatementID(ID: integer);
    public
      FEmpl: string;
      FPOSClientID: integer;
      procedure AddPayment(Payment: TPayment);
      function SaveCashStatements: boolean;
      function GetPaymIDByNodeName(NodeName: string): integer;
      constructor Create(DataAreaID: SmallInt);
      Destructor Destroy; override;
      property OpenDateAsStr: string write SetOpenDate;
      property CloseDateAsStr: string write SetCloseDate;
      property OpenDate: TDateTime read FOpenDate;
      property CloseDate: TDateTime read FCloseDate;
      property CashStateMentID: integer read FID write SetCashStatementID;
      //Property CashStateMentCompleted: boolean read FCashStateMentCompleted;
  end;


implementation

{ TCashStateMentHeader }

procedure TCashStateMentHeader.AddPayment(Payment: TPayment);
var
  tempPayment: TPayment;
begin
    
    if FPayments.TryGetValue(Payment.PaymentMethodID,tempPayment) then
    begin
        if Payment.ShiftDocSetion = COUNTED_MONEY then
        begin
            tempPayment.CountedAmount:=tempPayment.CountedAmount+Payment.CountedAmount;
        end
        else
        begin
            tempPayment.PaymNodeName:=Payment.PaymNodeName;
            tempPayment.TransAmount:=tempPayment.TransAmount+Payment.TransAmount;
        end;
        FPayments.AddOrSetValue(Payment.PaymentMethodID,tempPayment);
    end
    else
        FPayments.Add(Payment.PaymentMethodID, Payment);
end;

function TCashStateMentHeader.ConStrToDateTime(sDateTime: string): TDateTime;
var
  Year: word;
  Month: word;
  Day: word;
  Hour: word;
  Minute: word;
  Seconds: word;
  TempDate: TDate;
  TempTime: TTime;
begin
    Year:=StrToInt(Copy(sDateTime,1,4));
    Month:=StrToInt(Copy(sDateTime,5,2));
    Day:=StrToInt(Copy(sDateTime,7,2));
    Hour:=StrToInt(Copy(sDateTime,9,2));
    Minute:=StrToInt(Copy(sDateTime,11,2));
    Seconds:=StrToInt(Copy(sDateTime,13,2));

    TempDate:=EncodeDate(Year,Month,Day);
    TempTime:=EncodeTime(Hour,Minute,Seconds,0);
    result:=TempDate+TempTime;
end;

constructor TCashStateMentHeader.Create(DataAreaID: SmallInt);
begin
  FDataAreaID:=DataAreaID;
  FPayments:=TDictionary<integer, TPayment>.Create;
  FDB:=TdmCashStatements.Create(nil,FDataAreaID);
end;

destructor TCashStateMentHeader.Destroy;
begin
    FDB.free;
    FPayments.Free;
end;

function TCashStateMentHeader.GetPaymIDByNodeName(NodeName: string): integer;
begin
    result:=FDB.GetPaymentTypeByNodeName(NodeName);

    if result = -1 then
       LogFile(nil,'ERROR: PaymentType manglar til nodename "'+NodeName+'"');
end;

function TCashStateMentHeader.SaveCashStatements: boolean;
var
    ResultSaveHeader: Boolean;
    ResultSaveLines: Boolean;
begin
    ResultSaveHeader:= SaveHeader;
    ResultSaveLines:= SavePayments;
    result:=(ResultSaveHeader and ResultSaveLines = true);
end;

function TCashStateMentHeader.SaveHeader: boolean;
var
    HeaderID: integer;
begin
   try
      HeaderID:= FDB.SaveStatementHeader(FID, FPOSClientID,FOpenDate, FCloseDate, FEmpl, FDataAreaID);
      result:=HeaderID > 0;
   except
      result:=false;
      logfile(nil,'ERROR: CashStatement SaveHeader ClientID: '+intToStr(FPOSClientID)+': Date: '+DateTimeToStr(FOpenDate));
      //FCashStateMentCompleted:=False;
   end;
end;

function TCashStateMentHeader.SavePayments: boolean;
var
  LPayment: TPayment;
  MissingPayment: Boolean;
  LCountedAmount: Currency;
begin
   result:=False;
    try
        MissingPayment:=False;

        if (FPayments.Count < 1) then
        begin
            LogFile(nil, 'Warning: Payments: Uppger� fr�/til: '+DateTimeToStr(FOpenDate)+' - '+DateTimeToStr(FCloseDate), True);
            LogFile(nil, 'Warning: Payments: Ongar gjaldingar eru � hesi uppger�ini', True);
        end;

        for LPayment in FPayments.Values do
        begin
            if FDB.DoPaymentTypeExists(LPayment.PaymentMethodID) then
            begin
                if LPayment.PaymentMethodID = 1 then   // PaymentMethodID 1 = kontant, ta� er bara kontant, sum ver�ur talt upp � kassanum
                    LCountedAmount:=LPayment.CountedAmount
                else
                    LCountedAmount:=LPayment.TransAmount;

                FDB.SavePayments(LPayment.PaymentMethodID,LPayment.TransAmount, LCountedAmount);
            end
            else
            begin
              LogFile(nil,'ERROR: PaymentType: '+IntToStr(LPayment.PaymentMethodID)+' finnist ikki.',True);
              MissingPayment:=True;
            end;

            result:= not MissingPayment;
        end;
    except
        result:=False;
    end;
end;

procedure TCashStateMentHeader.SetCashStatementID(ID: integer);
begin
    FID:=ID;
end;

procedure TCashStateMentHeader.SetCloseDate(sDateTime: string);
begin
    FCloseDate:=ConStrToDateTime(sDateTime);
end;

procedure TCashStateMentHeader.SetOpenDate(sDateTime: string);
begin
    FOpenDate:=ConStrToDateTime(sDateTime);
end;


end.
