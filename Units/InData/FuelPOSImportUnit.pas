unit FuelPOSImportUnit;

interface

uses
  InterFaceUnit
  , System.IOUtils
  , Xml.xmldom
  , Xml.XmlTransform
  , Xml.XMLIntf
  , Xml.XMLDoc
  , dmCaxhStatementsUnit
  , IniFileUnit
  , CashStatementsUnit
  , TeraFunkUnit
  , BonTransUnit
  , System.Classes
  ;

type

  TFuelPOSImport = class(TInterfacedObject,IImportFromPOS)
    private
      FCashStatement: TCashStateMentHeader;
      FFileList: TStringList;
      FFilePaths: TFilePaths;
      procedure GetCashStatements;
      procedure GetBonData;
      procedure ProcessBonData;
      procedure ProcessCashStatements;
      function CreateFileList(FilePath: string): string;
      function GetFileNameFromList: string;
      procedure MoveToArchive(FileName, Archive: string);
    public
      constructor Create(AFilePaths: TFilePaths);
      destructor Destroy; Override;
  end;


implementation

uses
  FMX.Dialogs, System.SysUtils, FMX.StdCtrls, SharedUnit;

{ TFuelPOSImport }

constructor TFuelPOSImport.Create(AFilePaths: TFilePaths);
begin
    inherited Create;
    FFilePaths:=AFilePaths;
    FFileList:=TStringList.Create;
    FFileList.Sorted:=True;
end;

destructor TFuelPOSImport.Destroy;
begin
  FFileList.free;
  inherited;
end;

procedure TFuelPOSImport.GetBonData;
var
  LFileNameAndPath: string;
  tempFileName: string;
  OrgDecimalSeperator: Char;
  XMLDoc: IXMLDocument;
  Node: IXmlNode;
  BonList: TBonList;
  LBonHeader: TBonHeader;
  LBonLine: TBonLine;
  LPayment: TBonPayment;
  i: integer;
  LLAstPaymLine: SmallInt;
  tempPaymID: SmallInt;
begin
    try
        OrgDecimalSeperator:=FormatSettings.DecimalSeparator;
        LLAstPaymLine:=0;
        //FFilePaths.FCompanyID
        BonList:=TBonList.Create(FFilePaths.FCompanyID);
        try
            tempFileName:=GetFileNameFromList;

            if (tempFileName = '') then
                Exit;

            LFileNameAndPath:=IncludeTrailingPathDelimiter(FFilePaths.FRevenueFile)+tempFileName;
            Logfile(nil,'>>>>>--  Logf�lur: '+LFileNameAndPath, True);

            if (Length(LFileNameAndPath) < 12)  then
            begin
                LogFile(nil,'Ongin f�lur at lesa inn.',True);
                Exit;
            end;

            FormatSettings.DecimalSeparator:='.';
            XMLDoc:=TXMLDocument.Create(nil);
            XMLDoc.LoadFromFile(LFileNameAndPath);

            if Assigned(XMLDoc) then
            begin
                Node:=XMLDoc.DocumentElement.ChildNodes.First;

                while Node <> nil do
                begin
                    // BonHeader
                    if Node.NodeName = 'Transaction' then
                    begin
                        //LogFile(nil,'NodeName Transaction: ', True);
                        LBonHeader.Clear;
                        LLAstPaymLine:=0;

                        if (Node.ChildNodes.FindNode('SEQ') <> nil) then
                        begin
                            LBonHeader.BonIDAsStr:=(Node.ChildNodes['SEQ'].Text);
                            //LogFile(nil,'     SEQ: '+LBonHeader.BonIDAsStr, True);
                        end;

                        if (Node.ChildNodes.FindNode('REF') <> nil) then
                        begin
                            var RefValue := Node.ChildNodes['REF'].Text;
                            //LogFile(nil,'Node REF: '+RefValue, True);
                            LBonHeader.Ref:=StrToInt64(RefValue);
                            //LogFile(nil,'     REF: '+LBonHeader.Ref.ToString, True);
                        end;
                        if (Node.ChildNodes.FindNode('LREF') <> nil) then
                            LBonHeader.LRef:=StrToInt64(Node.ChildNodes['LREF'].Text);
                        if (Node.ChildNodes.FindNode('TYP') <> nil) then
                        begin
                            BonList.ErrorRegistered:=LBonHeader.FuelPosStatusToBonStatus(StrToIntDef(Node.ChildNodes['TYP'].Text,-1));
                        end;
                        if (Node.ChildNodes.FindNode('DATI') <> nil) then
                            LBonHeader.BonDateTimeAsString:=Node.ChildNodes['DATI'].Text;
                        if (Node.ChildNodes.FindNode('SHF') <> nil) then
                            LBonHeader.POSCashStatementID:=StrToIntDef(Node.ChildNodes['SHF'].Text,0);
                        if (Node.ChildNodes.FindNode('MERCH_ID') <> nil) then
                            LBonHeader.EmplID:=Node.ChildNodes['MERCH_ID'].Text;

                        BonList.AddBonHeader(LBonHeader);

                        // Gjaldsuppl�singar � samband vi� �tsettar gjaldingar liggja � summun f�rum TRANSACTION blokkinum.
                        if (Node.ChildNodes.FindNode('MOP') <> nil) then
                        begin
                           LPayment.Clear;

                           LPayment.FMOP:=StrToInt((Node.ChildNodes['MOP'].Text));

                           if (Node.ChildNodes.FindNode('MOP_ADDX') <> nil) then
                              LPayment.MOP_ADDX:=StrToInt((Node.ChildNodes['MOP_ADDX'].Text));
                           if (Node.ChildNodes.FindNode('MOP_ADDY') <> nil) then
                              LPayment.MOP_ADDY:=StrToInt((Node.ChildNodes['MOP_ADDY'].Text));
                           if (Node.ChildNodes.FindNode('ROP') <> nil) then
                              LPayment.ROP:=StrToInt((Node.ChildNodes['ROP'].Text));

                           LPayment.FLineNo:=1; //StrToInt((Node.ChildNodes['LINE_NO'].Text));

                           if (Node.ChildNodes.FindNode('DATI') <> nil) then
                              LPayment.FDateTime:=ConvertStrToDateTime((Node.ChildNodes['DATI'].Text),'YYYYMMDDhhmmss');
                           if (Node.ChildNodes.FindNode('PA') <> nil) then
                              LPayment.FPaydAmount:=StrToFloat(Node.ChildNodes['PA'].Text);

                           BonList.AddPayment(LPayment);
                        end;
                    end
                    // Bonlines
                    else if Node.NodeName = 'SalesLine' then
                    begin
                        LBonLine.Clear;

                        if (Node.ChildNodes.FindNode('PLU') <> nil) then   // PLU = itemid � projectstyringini
                        begin
                            LBonLine.ItemIDAsStr:=Node.ChildNodes['PLU'].Text;

                            if (Node.ChildNodes.FindNode('LINE_NO') <> nil) then
                                LBonLine.LineNo:=StrToInt((Node.ChildNodes['LINE_NO'].Text));
                            if (Node.ChildNodes.FindNode('DATI') <> nil) then
                                LBonLine.DateTime:=ConvertStrToDateTime(Node.ChildNodes['DATI'].Text, 'YYYYMMDDhhmmss');
                            if (Node.ChildNodes.FindNode('NAM') <> nil) then
                                LBonLine.ItemName:=Node.ChildNodes['NAM'].Text;

                            if (Node.ChildNodes.FindNode('BAR') <> nil) then
                                LBonLine.Ean:=Node.ChildNodes['BAR'].Text;
                            if (Node.ChildNodes.FindNode('QTYUNIT') <> nil) then
                                LBonLine.ItemUnit:=Node.ChildNodes['QTYUNIT'].Text;
                            if (Node.ChildNodes.FindNode('PRC') <> nil) then
                                LBonLine.VatRate:=StrToCurr(Node.ChildNodes['PRC'].Text);
                            if (Node.ChildNodes.FindNode('QTY') <> nil) then
                                LBonLine.Qty:=StrToFloat(Node.ChildNodes['QTY'].Text);
                            if (Node.ChildNodes.FindNode('EXC') <> nil) then        // Pr�sur uttan mvg
                                LBonLine.LinePriceExVat:=StrToCurr(Node.ChildNodes['EXC'].Text);
                            if (Node.ChildNodes.FindNode('INC') <> nil) then        // Pr�sur vi� mvg uttan oljuavgjald
                                LBonLine.LinePriceIncVat:=StrToCurr(Node.ChildNodes['INC'].Text);
                            if (Node.ChildNodes.FindNode('AMT') <> nil) then        // Pr�sur vi� mvg
                                LBonLine.SalesPriceIncDeposit:=StrToCurr(Node.ChildNodes['AMT'].Text);

                            BonList.AddBonLine(LBonLine);
                        end;
                    end
                    // Payments
                    else if Node.NodeName = 'PaymentLine' then
                    begin
                         // i Ver�ur br�kt fyri at fanga um tv�r gjaldingar liggja � einum paymentblokki
                         // Hetta er ein feilur � logf�linum fr� Effo
                         i:=0;

                         while (Node.ChildNodes.FindNode('MOP') <> nil) do
                         begin
                             LPayment.Clear;

                             if (Node.ChildNodes.FindNode('LINE_NO') <> nil) then
                             begin
                                tempPaymID:=StrToInt((Node.ChildNodes['LINE_NO'].Text))+i;

                                if (tempPaymID > LLAstPaymLine) then
                                begin
                                    LPayment.FLineNo:=tempPaymID;
                                    LLAstPaymLine:=LPayment.FLineNo;
                                end
                                else
                                begin
                                    LPayment.FLineNo:=LLAstPaymLine+1;
                                    LLAstPaymLine:=LPayment.FLineNo;
                                end;
                             end;
                             if (Node.ChildNodes.FindNode('DATI') <> nil) then
                             begin
                                LPayment.FDateTime:=ConvertStrToDateTime((Node.ChildNodes['DATI'].Text),'YYYYMMDDhhmmss');
                             end;
                             if (Node.ChildNodes.FindNode('PA') <> nil) then
                             begin
                                LPayment.FPaydAmount:=StrToFloat(Node.ChildNodes['PA'].Text);
                                Node.ChildNodes.Delete(Node.ChildNodes.IndexOf('PA'));    // M� sletta t�  <CB> taggi er vi� liggja uppl�singarnir
                             end;                                                         // dupult undir somu PaymentLine
                             if (Node.ChildNodes.FindNode('MOP') <> nil) then
                             begin
                                LPayment.FMOP:=StrToInt((Node.ChildNodes['MOP'].Text));
                                Node.ChildNodes.Delete(Node.ChildNodes.IndexOf('MOP'));
                             end;
                             if (Node.ChildNodes.FindNode('MOP_ADDX') <> nil) then
                             begin
                                LPayment.MOP_ADDX:=StrToInt((Node.ChildNodes['MOP_ADDX'].Text));
                                Node.ChildNodes.Delete(Node.ChildNodes.IndexOf('MOP_ADDX'));
                             end;
                             if (Node.ChildNodes.FindNode('MOP_ADDY') <> nil) then
                             begin
                                LPayment.MOP_ADDY:=StrToInt((Node.ChildNodes['MOP_ADDY'].Text));
                                Node.ChildNodes.Delete(Node.ChildNodes.IndexOf('MOP_ADDY'));
                             end;
                             if (Node.ChildNodes.FindNode('ROP') <> nil) then
                             begin
                                LPayment.ROP:=StrToInt((Node.ChildNodes['ROP'].Text));
                                Node.ChildNodes.Delete(Node.ChildNodes.IndexOf('ROP'));
                             end;

                             BonList.AddPayment(LPayment);
                             inc(i);
                         end;
                    end;

                    Node := Node.NextSibling;
                end;
            end;

            BonList.SaveListToDb;

            if BonList.ErrorRegistered = false then
               MoveToArchive(LFileNameAndPath, FFilePaths.FRevenueFileArchive)
            else
               MoveToArchive(LFileNameAndPath, FFilePaths.FRevenueFileError);

        finally
            FormatSettings.DecimalSeparator:=OrgDecimalSeperator;
            BonList.Free;
        end;
    except
        on E: exception do
            LogFile(nil,'ERROR: Parsing bondata in file: '+tempFileName+': '+E.Message,True);
    end;
end;

procedure TFuelPOSImport.GetCashStatements;
var
  XMLDoc: IXMLDocument;
  Node: IXmlNode;
  LFileNameAndPath: string;
  tempFileName: string;
  OrgDecimalSeperator: Char;
  Payment: TPayment;
begin
   OrgDecimalSeperator:=FormatSettings.DecimalSeparator;
   try
       FormatSettings.DecimalSeparator:='.';

       XMLDoc:=TXMLDocument.Create(nil);
       try
          tempFileName:=GetFileNameFromList;
          LFileNameAndPath:=IncludeTrailingPathDelimiter(FFilePaths.FCashStatements)+tempFileName;

          if (Length(LFileNameAndPath) < 12)  then
          begin
              LogFile(nil,'        Ongin f�lur at lesa inn.',True);
              Exit;
          end;

          logfile(nil,'>>>>>-- Innles kassauppger�: '+LFileNameAndPath,True);

          XMLDoc.LoadFromFile(LFileNameAndPath);

          if Assigned(XMLDoc) then
          begin
              FCashStatement:=TCashStateMentHeader.Create(FFilePaths.FCompanyID);
              Node:=XMLDoc.DocumentElement.ChildNodes.First;

              while Node <> nil do
              begin
                  if (Node.ChildNodes.Count > 0) then
                  begin
                      if Node.NodeName = 'STATUS' then
                      begin
                          if (Node.ChildNodes.FindNode('REP_NR') <> nil) then
                             FCashStatement.CashStateMentID:=StrToInt(Node.ChildNodes['REP_NR'].Text);
                             //FCashStatement.Create(StrToInt(Node.ChildNodes['REP_NR'].Text));
                          if (Node.ChildNodes.FindNode('OPEN') <> nil) then
                             FCashStatement.OpenDateAsStr:=Node.ChildNodes['OPEN'].Text;
                          if (Node.ChildNodes.FindNode('CLOS') <> nil) then
                             FCashStatement.CloseDateAsStr:=Node.ChildNodes['CLOS'].Text;
                          if (Node.ChildNodes.FindNode('OPID') <> nil) then
                             FCashStatement.FEmpl:=Node.ChildNodes['OPID'].Text;
                          if (Node.ChildNodes.FindNode('POS') <> nil) then
                             FCashStatement.FPOSClientID:=StrToInt(Node.ChildNodes['POS'].Text);
                      end
                      else if Node.NodeName = 'COUNTED_MONEY' then
                      begin
                          Payment.ShiftDocSetion:=COUNTED_MONEY;
                          Payment.PaymNodeName:='';

                          if (Node.ChildNodes.FindNode('CURL') <> nil) then   // CURL = Total Cash Amount
                          begin
                              //Payment.PaymentMethodID:=StrToInt(Node.ChildNodes['ID'].Text);
                              Payment.PaymentMethodID:=1;
                              Payment.CountedAmount:=StrToCurr(Node.ChildNodes['CURL'].Text);
                              FCashStatement.AddPayment(Payment);
                          end;
                      end
                      else if Node.NodeName = 'POS_CASH_IN' then    // Inngj�ld � kassan
                      begin
                          Payment.ShiftDocSetion:=SALES_MOP;
                          Payment.PaymNodeName:='';


                          while (Node.ChildNodes.FindNode('PACC') <> nil) or (Node.ChildNodes.FindNode('DOF') <> nil) do
                          begin
                              if (Node.ChildNodes.FindNode('PACC') <> nil) then
                              begin
                                  try                                                                    // �tsett bon sum eru goldnar � kassan
                                      Payment.PaymentMethodID:=FCashStatement.GetPaymIDByNodeName('PACC');
                                      Payment.TransAmount:=StrToCurr(Node.ChildNodes['PACC'].Text)*-1;   // PACC = Delayd Payment
                                      Node.ChildNodes.Delete(Node.ChildNodes.IndexOf('PACC'));
                                  except
                                      Payment.TransAmount:=0;
                                      Node.ChildNodes.Delete(Node.ChildNodes.IndexOf('PACC'));
                                  end;

                                  FCashStatement.AddPayment(Payment);
                              end
                              else if (Node.ChildNodes.FindNode('DOF') <> nil) then
                              begin
                                  try                                                                    // �tsett bon sum eru goldnar � kassan
                                      Payment.PaymentMethodID:=FCashStatement.GetPaymIDByNodeName('DOF');
                                      Payment.TransAmount:=StrToCurr(Node.ChildNodes['DOF'].Text)*-1;   // PACC = Delayd Payment
                                      Node.ChildNodes.Delete(Node.ChildNodes.IndexOf('DOF'));
                                  except
                                      Payment.TransAmount:=0;
                                      Node.ChildNodes.Delete(Node.ChildNodes.IndexOf('DOF'));
                                  end;

                                  FCashStatement.AddPayment(Payment);
                              end;
                          end;
                      end
                      else if Node.NodeName = 'CREDIT_PAYM_TOTALS' then
                      begin
                          Payment.ShiftDocSetion:=SALES_MOP;
                          Payment.PaymNodeName:='';

                          if (Node.ChildNodes.FindNode('DOF') <> nil) then    // DOF = r�mdur uttan at gjalda
                          begin
                              Payment.PaymentMethodID:=FCashStatement.GetPaymIDByNodeName('DOF');
                              Payment.TransAmount:=StrToCurr(Node.ChildNodes['DOF'].Text);
                              FCashStatement.AddPayment(Payment);
                          end;
                          if (Node.ChildNodes.FindNode('COG') <> nil) then    // DOF = r�mdur uttan at gjalda
                          begin
                              Payment.PaymentMethodID:=FCashStatement.GetPaymIDByNodeName('COG');
                              Payment.TransAmount:=StrToCurr(Node.ChildNodes['COG'].Text);
                              FCashStatement.AddPayment(Payment);
                          end;
                          if (Node.ChildNodes.FindNode('PACC') <> nil) then
                          begin
                              try                                                                    // �tsett gjaldingar
                                  Payment.PaymentMethodID:=FCashStatement.GetPaymIDByNodeName('PACC');
                                  Payment.TransAmount:=StrToCurr(Node.ChildNodes['PACC'].Text);   // PACC = Delayd Payment
                                  FCashStatement.AddPayment(Payment);
                              except
                                  Payment.TransAmount:=0;
                              end;
                          end;

                          if (Node.ChildNodes.FindNode('ID') <> nil) then
                          begin
                              if StrToInt(Node.ChildNodes['ID'].Text) > 1 then
                                 Payment.PaymentMethodID:=StrToInt(Node.ChildNodes['ID'].Text);
                              //else
                              //   Payment.PaymentMethodID:=0;


                              if (Node.ChildNodes.FindNode('CRD') <> nil) then
                                  Payment.TransAmount:=StrToCurr(Node.ChildNodes['CRD'].Text)
                              else if (Node.ChildNodes.FindNode('CSCCRD') <> nil) then
                                  Payment.TransAmount:=StrToCurr(Node.ChildNodes['CSCCRD'].Text)
                              else
                                  Payment.TransAmount:=0;

                              FCashStatement.AddPayment(Payment);
                          end;
                      end
                      else if Node.NodeName = 'RECEIVED_MONEY' then
                      begin
                          Payment.ShiftDocSetion:=SALES_MOP;
                          Payment.PaymNodeName:='';

                          if (Node.ChildNodes.FindNode('TOT') <> nil) then
                          begin
                              Payment.TransAmount:=StrToCurr(Node.ChildNodes['TOT'].Text);
                              Payment.PaymentMethodID:=1;
                              FCashStatement.AddPayment(Payment);
                          end;
                      end;
                  end;

                  //sXml:=sXML+': '+Node.NodeName;
                  Node := Node.NextSibling;
              end;

              if FCashStatement.SaveCashStatements = true then
              begin
                 MoveToArchive(LFileNameAndPath,FFilePaths.FCashStatementsArchive);
              end
              else
                  MoveToArchive(LFileNameAndPath, FFilePaths.FCashStatementsError);
          end;
       finally
          FCashStatement.free;
          //XMLDoc.Free;
       end;
   finally
       FormatSettings.DecimalSeparator:=OrgDecimalSeperator;
   end;
end;

function TFuelPOSImport.GetFileNameFromList: string;
begin
    if (FFileList.Count > 0) then
    begin
        result:= FFileList[0];
        FFileList.Delete(0);
    end;
end;

function TFuelPOSImport.CreateFileList(FilePath: string): string;
var
  sr: TSearchRec;
begin
    try
        FFileList.Clear;

        if SetCurrentDir(FilePath) then
        begin
            if FindFirst('*.xml', faAnyFile,sr) = 0 then
            begin
              repeat
                  FFileList.Add(sr.Name);
              until FindNext(sr) <> 0;

              FindClose(sr);
            end;
        end
        else
            LogFile(nil,'ERROR: Finnur ikki mappu til kassauppger�irnar.',True);
    except
        on E: exception do
          LogFile(nil,'ERROR: CreateFileList. '+E.message ,True);
    end;
end;


procedure TFuelPOSImport.MoveToArchive(FileName, Archive: string);
var
  ArchiveFileName: string;
begin
    if KannaFilPath(Archive) = True then
    begin
       ArchiveFileName:=UmdoypUmFilurFinnist(IncludeTrailingPathDelimiter(Archive)+ExtractFileName(FileName));
       TFile.Move(FileName,ArchiveFileName);
    end;
end;

procedure TFuelPOSImport.ProcessBonData;
var
  DataProcessed: boolean;
begin
    DataProcessed:=False;
    CreateFileList(FFilePaths.FRevenueFile);

    if (FFileList.Count > 0) then
    begin
        logfile(nil,'>>>>>--', True);
        logfile(nil,'>>>>>--  Byrja innlesing av bondata. -->>>>>--      ', True);
        DataProcessed := True;
    end;

    while (FFileList.Count > 0) do
    begin
        GetBonData;
    end;

    if DataProcessed = True then
    begin
        logfile(nil,'<<<<<--  Enda innlesing av bondata          --<<<<<.', True);
        logfile(nil,'<<<<<--', True);
    end;
end;

procedure TFuelPOSImport.ProcessCashStatements;
var
    FilesProcessed: boolean;
begin
    FilesProcessed:=False;
    CreateFileList(FFilePaths.FCashStatements);

    if (FFileList.Count > 0) then
    begin
       logfile(nil,'>>>>>-- CashStatements START IMPORT.',True);
       FilesProcessed:=True;
    end;

    while (FFileList.Count > 0) do
    begin
        GetCashStatements;
    end;

    if FilesProcessed = True then
       logfile(nil,'<<<<<-- CashStatements END IMPORT.',True);
end;

end.

