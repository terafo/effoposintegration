unit BonTransUnit;

interface

uses
  System.Generics.Collections
  , TeraFunkUnit
  , SharedUnit
  , dmLogDataUnit
  ;

type
  TBonHeader = record
    strict private
      procedure SetBonID(BonID: string);
      function GetBonDateTimeAsStr: string;
      procedure SetBonDateTimeAsStr(sDateTime: string);
    private
      FFields: TBonHeaderFields;
    public
      procedure Clear;
      function FuelPosStatusToBonStatus(Status: SmallInt):boolean;
      property BonID: integer read FFields.BonID;
      property BonIdAsStr: string write SetBonID;
      property BonDateTime: TDateTime read FFields.BonDateTime write FFields.BonDateTime;
      property BonDateTimeAsString: string read GetBonDateTimeAsStr write SetBonDateTimeAsStr;
      property POSClientID: SmallInt read FFields.PosClientID;
      property EmplID: string read FFields.EmplID write FFields.EmplID;
      property POSCashStatementID: integer read FFields.PosCashStatementID write FFields.PosCashStatementID;
      property BonStatus: SmallInt read FFields.BonStatus; //write SetBonStatus;
      property Fields: TBonHeaderFields read FFields;
      property Ref: int64 read FFields.Ref write FFields.Ref;
      property LRef: int64 read FFields.LRef write FFields.LRef;
  end;

  TBonLine = record
    strict private
      //procedure Set
    private
      FFields: TbonLineFields;
      procedure SetItemID(const Value: string);
      procedure SetItemUnit(const Value: string);
      procedure SetLinePriceExVat(const SalesPriceExVat: Currency);
    public
      procedure Clear;
      property LineNo: SmallInt write FFields.FLineNo;
      property DateTime: TDateTime write FFields.FDateTime;
      property ItemName: string write FFields.FitemName;
      property ItemIDAsStr: string write SetItemID;
      property Ean: string write FFields.Fean;
      property ItemUnit: string  write SetItemUnit;
      property VatRate: Currency write FFields.FVatRate;
      property Qty: Extended read FFields.FQty write FFields.FQty;
      property LinePriceExVat: Currency write SetLinePriceExVat;
      property LinePriceIncVat: Currency read FFields.FLineAmountIncVat write FFields.FLineAmountIncVat;
      property SalesPriceIncDeposit: Currency Read FFields.FLinePriceIncDepositAndVat Write FFields.FLinePriceIncDepositAndVat;
      //property NetPricePrUnit: Currency write SetNetPricePrUnit;
      property Fields: TBonLineFields read FFields;
  end;



  TBon = class(TObject)
    private
      FHeader: TBonHeader;
      FLines: TList<TBonLine>;
      FPayments: TList<TBonPayment>;
      FTestBon: Boolean;
      //FPaymentTypes: TList<SmallInt>
      function SaveToDB(dm: TdmBon): boolean;
    public
      procedure AddBonLine(BonLine: TBonLine);
      procedure AddPayment(Payment: TBonPayment);
      constructor Create(BonHeader: TBonHeader);
      destructor Destroy; override;
  end;


  TBonList = class(TObject)
    private
      Fdm: TdmBon;
      FBon: TBon;
      FDataAreaID: SmallInt;
      FLastBonID: string;
      FBonList: TDictionary<int64, TBon>;
      FSortBons: TList<int64>;
      FImportError: Boolean;
      procedure SetBonID(BonID: string);
      procedure RegisterError(result: boolean);
    public
      procedure AddBonHeader(BonHeader: TBonHeader);
      procedure AddBonLine(BonLine: TBonLine);
      procedure AddPayment(Payment: TBonPayment);
      procedure SaveListToDb;
      constructor Create(DataAreaID: SmallInt);
      destructor Destroy; override;
      property BonID: string write SetBonID;
      property ErrorRegistered: boolean read FImportError write RegisterError;
  end;


implementation

uses
  System.SysUtils, System.Classes, FMX.Dialogs;

{ TBonList }

procedure TBonList.AddBonHeader(BonHeader: TBonHeader);
var
  LBon: TBon;
  i: integer;
  LBonLine: TBonLine;
  LBonID: integer;
begin
    try
        if (BonHeader.BonStatus = 1) then
        begin
            if (BonHeader.BonID > 0) then
            begin
                FBon:=TBon.Create(BonHeader);
                FBonList.AddOrSetValue(Fbon.FHeader.Ref, FBon);
                FSortBons.Add(Fbon.FHeader.Ref);
            end;
        end
        else if (BonHeader.BonStatus = 2) then // annullera bon
        begin
            // Goyma bongina fyribils, skal ikki goymast � databasan.
            // LREF nummari � hesari bon v�sur til upprunaligu bon, sum skal annullerast.

            FBon:=TBon.Create(BonHeader);
            FBon.FHeader.FFields.BonStatus:= -2;
            FBonList.AddOrSetValue(Fbon.FHeader.Ref, FBon);
            FSortBons.Add(Fbon.FHeader.Ref);

            // Finn upprunaligu bon � Dictionary og markera hana sum annullera�
            if FBonList.TryGetValue(BonHeader.Fields.LRef, LBon) = true then
            begin
                //LBon:=FBonList.Items[BonHeader.Fields.LRef];
                LBon.FHeader.FFields.BonStatus:=2;

                for i:=0 to LBon.FLines.Count-1 do
                begin
                    LBonLine:=LBon.FLines[i];
                    LBonLine.FFields.FLineStatus:= 2;
                    LBon.FLines[i]:=LBonLine;
                end;

                FBonList.AddOrSetValue(LBon.FHeader.Ref, LBon);
            end
            else
            begin
                // Um bongin er fr� undanfarnari koyring, m� hon heintast fr� databasanum fyri at broyta status
                LBonID:=Fdm.GetBonIDFromRefID(BonHeader.Fields.LRef);

                if (LBonID > 0) then
                begin
                    Fdm.UpdateBonStatus(LBonID,2);
                end
                else
                begin
                    LogFile(nil,'ERROR: BonID:'+BonHeader.BonId.ToString+ ' Finnur ikki LREF Nr. '+BonHeader.Fields.LRef.ToString, True);
                    RegisterError(false);
                end;
            end;


            {else
            begin
                FBon:=TBon.Create(BonHeader);
                FBonList.AddOrSetValue(Fbon.FHeader.Ref, FBon);
                FSortBons.Add(Fbon.FHeader.Ref);
            end;}
        end;
    except
        on E: exception do
        begin
            LogFile(nil,'ERROR: BonID:'+BonHeader.BonId.ToString+ ' AddBonHeader '+E.Message, True);
            RegisterError(false);
        end;
    end;
end;

procedure TBonList.AddBonLine(BonLine: TBonLine);
begin
    try
        FBon.AddBonLine(BonLine);
    except
        on E: exception do
        begin
            LogFile(nil,'ERROR: BonID:'+FBon.FHeader.BonId.ToString+' AddBonLine '+E.Message , True);
            RegisterError(false);
        end;
    end;
end;

procedure TBonList.AddPayment(Payment: TBonPayment);
begin
    try
        if (Payment.FMOP > 0) then
        begin
            Payment.FPaymMethod:=Fdm.GetPaymentMethod(FBon.FHeader.BonID, FBon.FHeader.POSClientID, Payment.FMOP,Payment.MOP_ADDX, Payment.MOP_ADDY,Payment.ROP);

            if Payment.FPaymMethod >= 0 then
            begin
                FBon.AddPayment(Payment);
            end
            else
                RegisterError(False);
        end;
    except
        on E: exception do
        begin
            LogFile(nil,'ERROR: BonID:'+FBon.FHeader.BonId.ToString+' AddPayment '+E.Message, True);
            RegisterError(false);
        end;
    end;
end;

constructor TBonList.Create(DataAreaID: SmallInt);
begin
  inherited Create;
  FDataAreaID:=DataAreaID;
  FImportError:=False;
  FLastBonID:='';
  FBonList:=TDictionary<int64, TBon>.Create;
  FSortBons:=TList<int64>.Create;
  Fdm:=TdmBon.Create(nil,FDataAreaID);
end;

destructor TBonList.Destroy;
begin
    FSortBons.Free;
    FBonList.Free;

    if Assigned(Fdm) then
        Fdm.Free;

    inherited;
end;

procedure TBonList.RegisterError(result: boolean);
begin
    if result = false then
        FImportError:=True;
end;

procedure TBonList.SaveListToDb;
var
  LBon: TBon;
  LREfBon: TBon;
  LBonID: int64;
  LRefBonID: int64;
  LPayment: TBonPayment;
begin
    try
        for LBonID in FSortBons do
        begin
            if FBonList.TryGetValue(LBonID,LBon) then
            begin
                if (LBon.FHeader.BonStatus < 1) then
                begin
                    FreeAndNil(LBon);
                    Continue;
                end;

                //Um LREF > 0 finn upprunaligu bon fyri at kunna m�tb�ka gjaldi fr� t� bongini.
                if (LBon.FHeader.LRef > 0) and (LBon.FHeader.FFields.TransType = 2)
                    and (LBon.FHeader.BonStatus = 1) and (LBon.FLines.Count = 0) then
                begin
                    try
                        LBon.FHeader.FFields.BonStatus:=6;
                        LPayment.Clear;

                        LRefBonID:=Fdm.GetBonIDFromRefID(LBon.FHeader.LRef);

                        if LRefBonID > 0 then
                        begin
                            LPayment:=Fdm.GetBonPaymentType(LRefBonID);

                            if (LPayment.FPaymMethod < 0) then
                            begin
                                LogFile(nil,'ERROR: Bon RefID:'+LBon.FHeader.LRef.ToString+' finnist ikki', True);
                                RegisterError(False);
                                Exit;
                            end;
                        end
                        else if FBonList.TryGetValue(LBon.FHeader.LRef, LRefBon) = true then
                        begin
                           if (LRefBon.FPayments.Count > 0) then
                              LPayment:=LRefBon.FPayments[0]
                           else
                           begin
                              LogFile(nil,'ERROR: BonID :'+LBon.FHeader.BonID.ToString+' No Payments on this bon', True);
                              LogFile(nil,'ERROR: LRefID:'+LBon.FHeader.LRef.ToString+' No Payments on this bon', True);
                              RegisterError(False);
                              Exit;
                           end;
                        end
                        else
                        begin
                            LogFile(nil,'ERROR: Bon RefID:'+LBon.FHeader.LRef.ToString+' finnist ikki', True);
                            RegisterError(False);
                            Exit;
                        end;

                        LPayment.FBonID:=LBonID;
                        LPayment.FPosClientID:=Lbon.FHeader.FFields.PosClientID;
                        LPayment.FLineNo:=101; //Payment.FLineNo+1;
                        LPayment.FDateTime:=Lbon.FHeader.FFields.BonDateTime;
                        LPayment.FPaydAmount:=LPayment.FPaydAmount * -1;
                        LBon.AddPayment(LPayment);
                    except
                        on E: Exception do
                        begin
                            LogFile(nil,'ERROR: TryGetLRefHeader. BON REF: '+LBon.FHeader.LRef.ToString +E.Message , True);
                            RegisterError(false);
                        end;

                    end;
                end;

                if LBon.SaveToDB(Fdm) = false then
                    RegisterError(false);
            end;

            if assigned(LBon) then
               FreeAndNil(LBon);
        end;
    except
        on E: Exception do
        begin
            LogFile(nil,'ERROR: Savebonlist To DB. '+E.Message , True);
            RegisterError(false);
        end;
    end;
end;


procedure TBonList.SetBonID(BonID: string);
begin
    {if BonID <> FLastBonID then
    begin
      if (FLastBonID > '') then
         FBonList.Add(FBon);

      FBon:=TBon.Create(BonID);
      FLastBonID:=BonID;
    end;  }
end;

{ TBon }

procedure TBon.AddBonLine(BonLine: TBonLine);
begin
    BonLine.FFields.FBonID:=FHeader.BonID;
    BonLine.FFields.FPOSClientID:=FHeader.POSClientID;
    BonLine.FFields.FLineStatus:=FHeader.BonStatus;
    FLines.Add(BonLine);

    // Stovna linju fyri oljuavjgald. V�runr hj� Effo = 310
    if BonLine.SalesPriceIncDeposit <> BonLine.LinePriceIncVat then
    begin
       BonLine.FFields.FLineNo:=BonLine.FFields.FLineNo+1000;
       BonLine.FFields.FItemName:='Oljuavgjald';
       BonLine.FFields.FItemID:=310;
       BonLine.FFields.FUnit:='L';
       BonLine.FFields.FVatRate:=0;
       BonLine.FFields.FLineStatus:=1;
       BonLine.FFields.FSalesPricePrUnit:=0.8;
       BonLine.FFields.FLineDiscount:=0;
       BonLine.FFields.FInvoiceDiscount:=0;
       BonLine.FFields.FLineAmount:=BonLine.FFields.FQty*0.8;
       BonLine.FFields.FLineAmountIncVat:=BonLine.FFields.FLineAmount;
       BonLine.FFields.FLinePriceIncDepositAndVat:=BonLine.FFields.FLineAmount;
       FLines.Add(BonLine);
    end;
end;

procedure TBon.AddPayment(Payment: TBonPayment);
begin
    //Um PaymMethod = 0 so skal ta� ikki b�kast   Um MOP = 12, so er ta� ein pumputest
    FTestBon:=(Payment.FPaymMethod = 0);   // (Payment.FMOP = 12);

    Payment.FBonID:=FHeader.BonID;
    Payment.FPosClientID:=FHeader.POSClientID;
    FPayments.add(Payment);
end;

constructor TBon.Create(BonHeader: TBonHeader);
begin
    inherited Create;
    FTestBon:=False;
    FHeader:=BonHeader;
    FLines:=TList<TBonLine>.Create;
    FPayments:=TList<TBonPayment>.Create;
end;

destructor TBon.Destroy;
begin
    if FLines <> nil then
       FLines.free;

    if FPayments <> nil then
       FPayments.Free;
end;

function TBon.SaveToDB(dm: TdmBon):boolean;
var
  BonLine: TBonLine;
  Payment: TBonPayment;
begin
    result:=True;
    try
        if (FHeader.BonID > 0) and (FTestBon = false) then
        begin
            dm.SaveBonHeader(FHeader.Fields);

            if (FLines <> nil) then
            begin
                for BonLine in FLines do
                begin
                   if dm.SaveBonLines(BonLine.Fields) = false then
                      result:=False;
                end;
            end;

            if (FPayments <> nil) then
            begin
                for Payment in FPayments do
                  dm.SavePayment(Payment);
            end;
        end;
    except
        on E: Exception do
        begin
            result:=false;
            logfile(nil,'ERROR Saving bondata: '+E.Message,True);
        end;
    end;
end;


{ TBonHeader }

procedure TBonHeader.Clear;
begin
    FFields.BonID:= -1;
    FFields.BonDateTime:= 0;
    FFields.PosClientID:= -1;
    FFields.BonStatus:= -1;
    FFields.PosCashStatementID:= -1;
    FFields.EmplID:='';
    FFields.BonAmount:= 0;
    FFields.Ref:=-1;
    FFields.LRef:= -1;
end;

function TBonHeader.GetBonDateTimeAsStr: string;
begin
    result:=DateTimeToStr(Fields.BonDateTime);
end;


procedure TBonHeader.SetBonDateTimeAsStr(sDateTime: string);
begin
    BonDateTime:=ConvertStrToDateTime(sDateTime,'YYYYMMDDhhmmss');
end;

procedure TBonHeader.SetBonID(BonID: string);
begin
    FFields.BonID:=StrToInt(Copy(Bonid,5,9));

    if (Copy(Bonid,5,1) > '1') then      // 5 = bensinpumpur, t�r f�a eitt tveysiffra kassanr.
        FFields.PosClientID:=StrToInt(Copy(Bonid,5,2))
    else
        FFields.PosClientID:=StrToInt(Copy(Bonid,6,1));
end;

function TBonHeader.FuelPosStatusToBonStatus(Status: SmallInt): Boolean;
begin
    result:=True;
    FFields.TransType:=Status;

    if (Status = 1) then
       FFields.BonStatus:= -1   // Ignore Open shift
    else if (Status = 2) then    // Status 2 = Normal Transaction in FuelPOS
       FFields.BonStatus:=1     // 1 = Avslutta bon � Projectstyring
    else if (Status = 3) then
       FFields.BonStatus:= -1   // Ignore Close shift
    else if (Status = 5) then   // Status 5 = Canceled Transaction in FuelPOS
       FFields.BonStatus:= 2     // Vit skulu bara br�ka LREF vir�i �r hesi bon og seta bonstatus 2 = Annullera til upprunaligu bon. Ver�ur lagt til a�ra bon via LREF vir�inum LREF = REF � org bongini.
    else if (Status = 10) then   // Ignore Open day
       FFields.BonStatus:= -1
    else if (Status = 11) then   // Ignore Close day
       FFields.BonStatus:= -1
    else if (Status = 20) then   // End of Shift (ignoring)
       FFields.BonStatus:= -1
    else if (Status = 21) then   // Ignore 21 Annullera transaktion. Status 5 gevur eisini annullera�ar bon'ir
       FFields.BonStatus:= -1
    else
    begin
        result:=False;
        FFields.BonStatus:=Status;
        LogFile(nil,'ERROR: Unknown Transaction Status: <TYP>'+IntToStr(Status),True);
    end;
end;

{ TBonLine }

procedure TBonLine.Clear;
begin
    FFields.FLineNo:= -1;
    FFields.FDateTime:= 0;
    FFields.FItemName:='';
    FFields.FBonID:= -1;
    FFields.FPOSClientID:= -1;
    FFields.FItemID:= -1;
    FFields.FEan:='';
    FFields.FUnit:='';
    FFields.FVatRate:= 0;
    FFields.FLineStatus:= -1;
    FFields.FQty:= 0;
    FFields.FSalesPricePrUnit:= 0;
    FFields.FLinePriceIncDepositAndVat:=0;
    FFields.FLineDiscount:= 0;
    FFields.FInvoiceDiscount:= 0;
    FFields.FLineAmount:= 0;
    FFields.FLineAmountIncVat:= 0;
end;

procedure TBonLine.SetItemID(const Value: string);
begin
    FFields.FItemID:=StrToInt(Value);
end;

procedure TBonLine.SetItemUnit(const Value: string);
begin
   if (Value = '1') then
      FFields.FUnit := 'Stk'
   else if (Value = '2') then
      FFields.FUnit := 'L'
   else if (Value = '3') then
      FFields.FUnit := 'Kg';
end;


procedure TBonLine.SetLinePriceExVat(const SalesPriceExVat: Currency);
begin
     FFields.FSalesPricePrUnit:=SalesPriceExVat/Qty;
end;

end.
