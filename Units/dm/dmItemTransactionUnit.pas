unit dmItemTransactionUnit;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet
  , FireDAC.Comp.Client
  , FMX.Dialogs
  ;

type
  TItemTransRecord = record
    REFTABEL: SmallInt;
    REFLINJUID: Integer;
    NETTOPRISUR: Extended;
    KOSTPRISUR: Extended;
    FLUTNINGSKOSTNADUR: Extended;
    MVGSATSUR: Extended;
    GATT: Extended;
    PUNKT: Extended;
    VEKTGJALD: Extended;
    LINJUAVSLATTUR: Extended;
    INVOICEDISCOUNT: Extended;
    STARVSFOLK: String;
    DAGFEST: TDateTime;
    SOLUPRISUR: Extended;
    VARUNRID: Integer;
    TRANSSLAG: SmallInt;
    NOGD: Extended;
    DATAAREAID: SmallInt;
    REFLINESTATUS: SmallInt;
  end;

  IItemTrans = interface ['{C2A032F3-2FAC-4E1E-9775-E21A55DF9227}']
    procedure InsertItem(ItemTrans: TItemTransRecord);
    procedure UpdateItem(ItemTrans: TItemTransRecord; RefTable, RefLineID: integer);
    procedure DeleteItem(RefTableID: SmallInt; RefLineID: integer);
  end;

  TdmItemTransaction = class(TDataModule, IItemTrans)
    FDQItemTransaction: TFDQuery;
    FDUpdateSQL1: TFDUpdateSQL;
    FDComItemTransInsert: TFDCommand;
    FDComItemTransUpdate: TFDCommand;
    FDComItemTransDelete: TFDCommand;
  private
    procedure InsertItem(ItemTrans: TItemTransRecord);
    procedure UpdateItem(ItemTrans: TItemTransRecord; RefTable, RefLineID: integer);
    procedure DeleteItem(RefTableID: SmallInt; RefLineID: integer);
  public
    { Public declarations }
  end;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses dmDBConnectionUnit, TeraFunkUnit;

{$R *.dfm}

{ TdmItemTransaction }

procedure TdmItemTransaction.DeleteItem(RefTableID: SmallInt;
  RefLineID: integer);
begin
    FDComItemTransDelete.ParamByName('OLD_REFTABLE').AsSmallInt:=RefTableID;
    FDComItemTransDelete.ParamByName('OLD_REFLINJUID').AsSmallInt:=RefLineID;
    FDComItemTransDelete.Execute(1);
end;

procedure TdmItemTransaction.InsertItem(ItemTrans: TItemTransRecord);
begin
    try
        if (ItemTrans.REFLINESTATUS = 1) then
        begin
            FDComItemTransInsert.ParamByName('NEW_REFTABEL').AsSmallInt:= ItemTrans.REFTABEL;
            FDComItemTransInsert.ParamByName('NEW_REFLINJUID').AsInteger:= ItemTrans.REFLINJUID;
            FDComItemTransInsert.ParamByName('NEW_NETTOPRISUR').AsFloat:= ItemTrans.NETTOPRISUR;
            FDComItemTransInsert.ParamByName('NEW_KOSTPRISUR').AsFloat:= ItemTrans.KOSTPRISUR;
            FDComItemTransInsert.ParamByName('NEW_FLUTNINGSKOSTNADUR').AsFloat:= ItemTrans.FLUTNINGSKOSTNADUR;
            FDComItemTransInsert.ParamByName('NEW_MVGSATSUR').AsFloat:= ItemTrans.MVGSATSUR;
            FDComItemTransInsert.ParamByName('NEW_GATT').AsFloat:= ItemTrans.GATT;
            FDComItemTransInsert.ParamByName('NEW_PUNKT').AsFloat:= ItemTrans.PUNKT;
            FDComItemTransInsert.ParamByName('NEW_VEKTGJALD').AsFloat:= ItemTrans.VEKTGJALD;
            FDComItemTransInsert.ParamByName('NEW_LINJUAVSLATTUR').AsFloat:= ItemTrans.LINJUAVSLATTUR;
            FDComItemTransInsert.ParamByName('NEW_FAKTURAAVSLATTUR').AsFloat:= ItemTrans.INVOICEDISCOUNT;
            FDComItemTransInsert.ParamByName('NEW_STARVSFOLK').AsString:= ItemTrans.STARVSFOLK;
            FDComItemTransInsert.ParamByName('NEW_DAGFEST').AsDateTime:= ItemTrans.DAGFEST;
            FDComItemTransInsert.ParamByName('NEW_SOLUPRISUR').AsFloat:= ItemTrans.SOLUPRISUR;
            FDComItemTransInsert.ParamByName('NEW_VARUNRID').AsInteger:= ItemTrans.VARUNRID;
            FDComItemTransInsert.ParamByName('NEW_TRANSSLAG').AsSmallInt:= ItemTrans.TRANSSLAG;
            FDComItemTransInsert.ParamByName('NEW_NOGD').AsFloat:= ItemTrans.NOGD;
            FDComItemTransInsert.ParamByName('NEW_DATAAREAID').AsSmallInt:= ItemTrans.DATAAREAID;
            FDComItemTransInsert.Execute(1);
        end;
    except
        on E: Exception do
        begin
            LogFile(Self,'ERROR: Item: '+IntToStr(ItemTrans.VARUNRID)+': '+E.Message, True);
        end;
    end;
end;


procedure TdmItemTransaction.UpdateItem(ItemTrans: TItemTransRecord; RefTable, RefLineID: integer);
begin
    try
        if (ItemTrans.REFLINESTATUS = 1) then
        begin
            FDComItemTransUpdate.ParamByName('NEW_REFTABEL').AsSmallInt:= ItemTrans.REFTABEL;
            FDComItemTransUpdate.ParamByName('NEW_REFLINJUID').AsInteger:= ItemTrans.REFLINJUID;
            FDComItemTransUpdate.ParamByName('NEW_NETTOPRISUR').AsFloat:= ItemTrans.NETTOPRISUR;
            FDComItemTransUpdate.ParamByName('NEW_KOSTPRISUR').AsFloat:= ItemTrans.KOSTPRISUR;
            FDComItemTransUpdate.ParamByName('NEW_FLUTNINGSKOSTNADUR').AsFloat:= ItemTrans.FLUTNINGSKOSTNADUR;
            FDComItemTransUpdate.ParamByName('NEW_MVGSATSUR').AsFloat:= ItemTrans.MVGSATSUR;
            FDComItemTransUpdate.ParamByName('NEW_GATT').AsFloat:= ItemTrans.GATT;
            FDComItemTransUpdate.ParamByName('NEW_PUNKT').AsFloat:= ItemTrans.PUNKT;
            FDComItemTransUpdate.ParamByName('NEW_VEKTGJALD').AsFloat:= ItemTrans.VEKTGJALD;
            FDComItemTransUpdate.ParamByName('NEW_LINJUAVSLATTUR').AsFloat:= ItemTrans.LINJUAVSLATTUR;
            FDComItemTransUpdate.ParamByName('NEW_FAKTURAAVSLATTUR').AsFloat:= ItemTrans.INVOICEDISCOUNT;
            FDComItemTransUpdate.ParamByName('NEW_STARVSFOLK').AsString:= ItemTrans.STARVSFOLK;
            FDComItemTransUpdate.ParamByName('NEW_DAGFEST').AsDateTime:= ItemTrans.DAGFEST;
            FDComItemTransUpdate.ParamByName('NEW_SOLUPRISUR').AsFloat:= ItemTrans.SOLUPRISUR;
            FDComItemTransUpdate.ParamByName('NEW_VARUNRID').AsInteger:= ItemTrans.VARUNRID;
            FDComItemTransUpdate.ParamByName('NEW_TRANSSLAG').AsSmallInt:= ItemTrans.TRANSSLAG;
            FDComItemTransUpdate.ParamByName('NEW_NOGD').AsFloat:= ItemTrans.NOGD;
            FDComItemTransUpdate.ParamByName('NEW_DATAAREAID').AsSmallInt:= ItemTrans.DATAAREAID;
            FDComItemTransUpdate.ParamByName('OLD_REFLINJUID').Value:=RefLineID;
            FDComItemTransUpdate.ParamByName('OLD_REFTABEL').Value:=RefTable;
            FDComItemTransUpdate.Execute(1);
        end
        else
        begin
            FDComItemTransDelete.ParamByName('OLD_REFLINJUID').Value:=RefLineID;
            FDComItemTransDelete.ParamByName('OLD_REFTABEL').Value:=RefTable;
        end;
    except
        on E: Exception do
        begin
            LogFile(Self,'ERROR: Item: '+IntToStr(ItemTrans.VARUNRID)+': '+E.Message, True);
        end;
    end;
end;

end.
