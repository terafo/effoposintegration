object dmCashStatements: TdmCashStatements
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 400
  Width = 625
  object FDQCashStatements: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select * from cashstatement'
      'where dataareaid = :DATAAREAID '
      'and CLIENTSTATEMENTID = :CLIENTSTATEMENTID '
      'and POSCLIENTID = :POSCLIENTID')
    Left = 80
    Top = 48
    ParamData = <
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
        ParamType = ptInput
        Value = 1
      end
      item
        Name = 'CLIENTSTATEMENTID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'POSCLIENTID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object FDQCashStatementsSTATEMENTID: TIntegerField
      FieldName = 'STATEMENTID'
      Origin = 'STATEMENTID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQCashStatementsDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQCashStatementsPOSCLIENTID: TIntegerField
      FieldName = 'POSCLIENTID'
      Origin = 'POSCLIENTID'
      Required = True
    end
    object FDQCashStatementsTRANSDATETIMEFROM: TSQLTimeStampField
      FieldName = 'TRANSDATETIMEFROM'
      Origin = 'TRANSDATETIMEFROM'
      Required = True
    end
    object FDQCashStatementsTRANSDATETIMETO: TSQLTimeStampField
      FieldName = 'TRANSDATETIMETO'
      Origin = 'TRANSDATETIMETO'
      Required = True
    end
    object FDQCashStatementsDATECREATED: TSQLTimeStampField
      FieldName = 'DATECREATED'
      Origin = 'DATECREATED'
      Required = True
    end
    object FDQCashStatementsSTATUS: TIntegerField
      FieldName = 'STATUS'
      Origin = 'STATUS'
      Required = True
    end
    object FDQCashStatementsEMPLID: TStringField
      FieldName = 'EMPLID'
      Origin = 'EMPLID'
      Required = True
      Size = 10
    end
    object FDQCashStatementsCLIENTSTATEMENTID: TIntegerField
      FieldName = 'CLIENTSTATEMENTID'
      Origin = 'CLIENTSTATEMENTID'
    end
    object FDQCashStatementsRECID: TIntegerField
      FieldName = 'RECID'
      Origin = 'RECID'
    end
  end
  object FDQCashStatementLines: TFDQuery
    Connection = dmDBConnection.FDConnection1
    FetchOptions.AssignedValues = [evRecordCountMode]
    FetchOptions.RecordCountMode = cmFetched
    SQL.Strings = (
      'select * from cashstatementlines'
      'where DATAAREAID = :DATAAREAID '
      'and CASHSTATEMENTID = :CASHSTATEMENTID'
      'and PAYMENTTYPE = :PAYMENTTYPE'
      '')
    Left = 88
    Top = 112
    ParamData = <
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'CASHSTATEMENTID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'PAYMENTTYPE'
        DataType = ftSmallint
        ParamType = ptInput
      end>
    object FDQCashStatementLinesLINEID: TIntegerField
      FieldName = 'LINEID'
      Origin = 'LINEID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQCashStatementLinesCASHSTATEMENTID: TIntegerField
      FieldName = 'CASHSTATEMENTID'
      Origin = 'CASHSTATEMENTID'
      Required = True
    end
    object FDQCashStatementLinesDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      Required = True
    end
    object FDQCashStatementLinesPAYMENTTYPE: TSmallintField
      FieldName = 'PAYMENTTYPE'
      Origin = 'PAYMENTTYPE'
      Required = True
    end
    object FDQCashStatementLinesCURRENCY: TStringField
      FieldName = 'CURRENCY'
      Origin = 'CURRENCY'
      FixedChar = True
      Size = 3
    end
    object FDQCashStatementLinesTRANSAMOUNT: TCurrencyField
      FieldName = 'TRANSAMOUNT'
      Origin = 'TRANSAMOUNT'
    end
    object FDQCashStatementLinesCOUNTEDAMOUNT: TCurrencyField
      FieldName = 'COUNTEDAMOUNT'
      Origin = 'COUNTEDAMOUNT'
    end
    object FDQCashStatementLinesBANKEDAMOUNT: TCurrencyField
      FieldName = 'BANKEDAMOUNT'
      Origin = 'BANKEDAMOUNT'
    end
    object FDQCashStatementLinesCHANGEMONEY: TCurrencyField
      FieldName = 'CHANGEMONEY'
      Origin = 'CHANGEMONEY'
    end
    object FDQCashStatementLinesDIFFERENCEAMOUNT: TFMTBCDField
      FieldName = 'DIFFERENCEAMOUNT'
      Origin = 'DIFFERENCEAMOUNT'
      Precision = 18
      Size = 2
    end
    object FDQCashStatementLinesBASEDATAAREAID: TSmallintField
      FieldName = 'BASEDATAAREAID'
      Origin = 'BASEDATAAREAID'
      Required = True
    end
  end
  object FDQPaymentTypeByID: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select paymenttypeid, dataareaid '
      'from paymenttype'
      
        'where paymenttypeid = :paymenttypeid and dataareaid = :dataareai' +
        'd')
    Left = 80
    Top = 168
    ParamData = <
      item
        Name = 'PAYMENTTYPEID'
        DataType = ftSmallint
        ParamType = ptInput
        Value = 1
      end
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
        ParamType = ptInput
        Value = 1
      end>
  end
  object FDMemFuelPosPaymMethods: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 80
    Top = 232
    object FDMemFuelPosPaymMethodsMOP: TSmallintField
      FieldName = 'MOP'
    end
    object FDMemFuelPosPaymMethodsMOP_ADDX: TSmallintField
      FieldName = 'MOP_ADDX'
    end
    object FDMemFuelPosPaymMethodsMOP_ADDY: TSmallintField
      FieldName = 'MOP_ADDY'
    end
    object FDMemFuelPosPaymMethodsROP: TSmallintField
      FieldName = 'ROP'
    end
    object FDMemFuelPosPaymMethodsPAYMENTMETHOD: TSmallintField
      FieldName = 'PAYMENTMETHOD'
    end
    object FDMemFuelPosPaymMethodsNODENAME: TStringField
      FieldName = 'NODENAME'
    end
  end
end
