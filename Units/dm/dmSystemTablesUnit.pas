unit dmSystemTablesUnit;

interface

uses
  System.SysUtils
  , System.Classes
  , FireDAC.Stan.Intf
  , FireDAC.Stan.Option
  , FireDAC.Stan.Param
  , FireDAC.Stan.Error
  , FireDAC.DatS
  , FireDAC.Phys.Intf
  , FireDAC.DApt.Intf
  , Data.DB
  , FireDAC.Comp.DataSet
  , FireDAC.Comp.Client
  , IniFileUnit
  , System.IOUtils
  , TeraFunkUnit
  , FireDAC.Stan.StorageXML
  , dmDBConnectionUnit
  , FireDAC.Stan.Async
  , FireDAC.DApt
  ;

type
  TdmSystemTables = class(TDataModule)
    FDMemFuelPosPaymMethods: TFDMemTable;
    FDMemFuelPosPaymMethodsMOP: TSmallintField;
    FDMemFuelPosPaymMethodsMOP_ADDX: TSmallintField;
    FDMemFuelPosPaymMethodsMOP_ADDY: TSmallintField;
    FDMemFuelPosPaymMethodsROP: TSmallintField;
    FDMemFuelPosPaymMethodsPAYMENTMETHOD: TSmallintField;
    FDStanStorageXMLLink1: TFDStanStorageXMLLink;
    FDQPaymentMethods: TFDQuery;
    FDQPaymentMethodsPAYMENTTYPEID: TSmallintField;
    FDQPaymentMethodsNAME: TStringField;
    FDMemFuelPosPaymMethodsPAYMMETHNAME: TStringField;
    FDMemFuelPosPaymMethodsNODENAME: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure FDMemFuelPosPaymMethodsBeforeRefresh(DataSet: TDataSet);
    procedure FDMemFuelPosPaymMethodsAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure SavePaymTableToFile;
    procedure OpenPaymTable;
    procedure ExportPaymentsToCsv;
    procedure ImportPaymentFromCsv;
  end;



implementation



{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TdmSystemTables.DataModuleCreate(Sender: TObject);
begin
    FDQPaymentMethods.Open;
    //OpenPaymTable;
end;

procedure TdmSystemTables.DataModuleDestroy(Sender: TObject);
begin
    SavePaymTableToFile;
end;

procedure TdmSystemTables.ExportPaymentsToCsv;
var
  Line: String;
  ExportFile: TStringList;
begin
    ExportFile:=TStringList.Create;
    try
        FDMemFuelPosPaymMethods.First;

        while not FDMemFuelPosPaymMethods.Eof do
        begin
             Line:=FDMemFuelPosPaymMethodsMOP.AsString+';';
             Line:=Line+FDMemFuelPosPaymMethodsMOP_ADDX.AsString+';';
             Line:=Line+FDMemFuelPosPaymMethodsMOP_ADDY.AsString+';';
             Line:=Line+FDMemFuelPosPaymMethodsROP.AsString+';';
             Line:=Line+FDMemFuelPosPaymMethodsPAYMENTMETHOD.AsString+';';
             Line:=Line+FDMemFuelPosPaymMethodsNODENAME.AsString+';';
             ExportFile.Add(Line);
             FDMemFuelPosPaymMethods.Next;
        end;

        ExportFile.SaveToFile('PaymentMethods.csv');
    finally
        ExportFile.Free;
    end;
end;

procedure TdmSystemTables.FDMemFuelPosPaymMethodsAfterOpen(DataSet: TDataSet);
begin
    FDMemFuelPosPaymMethods.IndexName:='PAYMINDEX';
end;

procedure TdmSystemTables.FDMemFuelPosPaymMethodsBeforeRefresh(
  DataSet: TDataSet);
begin
    FDQPaymentMethods.Close;
    FDQPaymentMethods.Open;
end;


procedure TdmSystemTables.ImportPaymentFromCsv;
var
  Line: String;
  ImportFile: TStringList;
  i: integer;
begin
    ImportFile:=TStringList.Create;
    try
        ImportFile.LoadFromFile('PaymentMethods.csv');
        FDMemFuelPosPaymMethods.EmptyDataSet;

        for i:= 0 to ImportFile.Count-1 do
        begin
             Line:= ImportFile[i];
             try
                 FDMemFuelPosPaymMethods.Insert;
                 FDMemFuelPosPaymMethodsMOP.AsInteger:=StrToInt(getStringFromString(Line,';',0,1));
                 FDMemFuelPosPaymMethodsMOP_ADDX.AsInteger:=StrToInt(getStringFromString(Line,';',1,2));
                 FDMemFuelPosPaymMethodsMOP_ADDY.AsInteger:=StrToInt(getStringFromString(Line,';',2,3));
                 FDMemFuelPosPaymMethodsROP.AsInteger:=StrToInt(getStringFromString(Line,';',3,4));
                 FDMemFuelPosPaymMethodsPAYMENTMETHOD.AsInteger:=StrToInt(getStringFromString(Line,';',4,5));
                 FDMemFuelPosPaymMethodsNODENAME.AsString:=getStringFromString(Line,';',5,6);
                 FDMemFuelPosPaymMethods.Post;
             except
                 FDMemFuelPosPaymMethods.Cancel;
             end;
        end;
    finally
        ImportFile.Free;
    end;
end;

procedure TdmSystemTables.OpenPaymTable;
begin
    //if FDMemFuelPosPaymMethods.Active then
    //    FDMemFuelPosPaymMethods.Close;

    if TFile.Exists(dmDBConnection.ProgramParams.PaymentTable) then
        FDMemFuelPosPaymMethods.LoadFromFile(dmDBConnection.ProgramParams.PaymentTable);
end;

procedure TdmSystemTables.SavePaymTableToFile;
begin
    KannaFilPath(ExtractFilePath(dmDBConnection.ProgramParams.PaymentTable));
    FDMemFuelPosPaymMethods.SaveToFile(dmDBConnection.ProgramParams.PaymentTable);
end;

end.
