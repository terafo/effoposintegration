unit dmCaxhStatementsUnit;

interface

uses
  System.SysUtils
  , System.Classes
  , FireDAC.Stan.Intf
  , FireDAC.Stan.Option
  , FireDAC.Stan.Param
  , FireDAC.Stan.Error
  , FireDAC.DatS
  , FireDAC.Phys.Intf
  , FireDAC.DApt.Intf
  , FireDAC.Stan.Async
  , FireDAC.DApt
  , Data.DB
  , FireDAC.Comp.DataSet
  , FireDAC.Comp.Client
  , dmDBConnectionUnit
  , IniFileUnit
  , InterFaceUnit, SharedUnit
  ;

type
  TdmCashStatements = class(TDataModule)
    FDQCashStatements: TFDQuery;
    FDQCashStatementsSTATEMENTID: TIntegerField;
    FDQCashStatementsDATAAREAID: TSmallintField;
    FDQCashStatementsPOSCLIENTID: TIntegerField;
    FDQCashStatementsTRANSDATETIMEFROM: TSQLTimeStampField;
    FDQCashStatementsTRANSDATETIMETO: TSQLTimeStampField;
    FDQCashStatementsDATECREATED: TSQLTimeStampField;
    FDQCashStatementsSTATUS: TIntegerField;
    FDQCashStatementsEMPLID: TStringField;
    FDQCashStatementsCLIENTSTATEMENTID: TIntegerField;
    FDQCashStatementsRECID: TIntegerField;
    FDQCashStatementLines: TFDQuery;
    FDQCashStatementLinesLINEID: TIntegerField;
    FDQCashStatementLinesCASHSTATEMENTID: TIntegerField;
    FDQCashStatementLinesDATAAREAID: TSmallintField;
    FDQCashStatementLinesPAYMENTTYPE: TSmallintField;
    FDQCashStatementLinesCURRENCY: TStringField;
    FDQCashStatementLinesTRANSAMOUNT: TCurrencyField;
    FDQCashStatementLinesCOUNTEDAMOUNT: TCurrencyField;
    FDQCashStatementLinesBANKEDAMOUNT: TCurrencyField;
    FDQCashStatementLinesCHANGEMONEY: TCurrencyField;
    FDQPaymentTypeByID: TFDQuery;
    FDQCashStatementLinesDIFFERENCEAMOUNT: TFMTBCDField;
    FDMemFuelPosPaymMethods: TFDMemTable;
    FDMemFuelPosPaymMethodsMOP: TSmallintField;
    FDMemFuelPosPaymMethodsMOP_ADDX: TSmallintField;
    FDMemFuelPosPaymMethodsMOP_ADDY: TSmallintField;
    FDMemFuelPosPaymMethodsROP: TSmallintField;
    FDMemFuelPosPaymMethodsPAYMENTMETHOD: TSmallintField;
    FDMemFuelPosPaymMethodsNODENAME: TStringField;
    FDQCashStatementLinesBASEDATAAREAID: TSmallintField;
    procedure FDQCashStatementsAfterScroll(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private

  public
    function DoPaymentTypeExists(PaymTypeID: integer): boolean;
    function SaveStatementHeader(StatementID, POSClientID: integer; DateFrom, DateTo: TDateTime; EmplID: string; ADataAreaID: SmallInt): integer;
    procedure SavePayments(PaymentTypeID: integer; TransAmount, CountedAmount: Currency);
    function GetPaymentTypeByNodeName(NodeName: string): integer;
    constructor Create(AOwner: TComponent; DataAreaID: SmallInt); reintroduce ;overload;
  end;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses TeraFunkUnit
     , System.IOUtils
     ;

{$R *.dfm}

constructor TdmCashStatements.Create(AOwner: TComponent; DataAreaID: SmallInt);
begin
    inherited Create(AOwner);
end;

procedure TdmCashStatements.DataModuleCreate(Sender: TObject);
begin
    if TFile.Exists(dmDBConnection.ProgramParams.PaymentTable) then
        FDMemFuelPosPaymMethods.LoadFromFile(dmDBConnection.ProgramParams.PaymentTable);
end;

function TdmCashStatements.DoPaymentTypeExists(PaymTypeID: integer): boolean;
begin
    result:=False;

    try
        FDQPaymentTypeByID.ParamByName('paymenttypeid').Value:=PaymTypeID;
        FDQPaymentTypeByID.ParamByName('DATAAREAID').Value:=1; //dmDBConnection.ProgramParams.DataAreaID;
        FDQPaymentTypeByID.Open;                               // Fyribils: skal setast vi� stamdata parametri

        if (FDQPaymentTypeByID.RecordCount > 0) then
           result:=True
        else
           Logfile(nil,'ERROR: CashStatements Missing PaymentType: '+IntToStr(PaymTypeID),True);
    finally
        FDQPaymentTypeByID.Close;
    end;
end;

procedure TdmCashStatements.FDQCashStatementsAfterScroll(DataSet: TDataSet);
begin
    FDQCashStatementLines.Close;
    FDQCashStatementLines.Open;
end;

function TdmCashStatements.GetPaymentTypeByNodeName(NodeName: string): integer;
begin
    if FDMemFuelPosPaymMethods.Locate('NODENAME',NodeName,[]) then
        result:=FDMemFuelPosPaymMethodsPAYMENTMETHOD.Value
    else
        result:=-1;
end;

procedure TdmCashStatements.SavePayments(PaymentTypeID: integer; TransAmount, CountedAmount: Currency);
begin
    try
        try
            FDQCashStatementLines.ParamByName('PAYMENTTYPE').Value:=PaymentTypeID;
            FDQCashStatementLines.ParamByName('DATAAREAID').Value:=FDQCashStatementsDATAAREAID.Value;
            FDQCashStatementLines.ParamByName('CASHSTATEMENTID').Value:=FDQCashStatementsSTATEMENTID.Value;
            FDQCashStatementLines.Open;

            if (FDQCashStatementLines.RecordCount > 0) then
            begin
               FDQCashStatementLines.Edit;
            end
            else
            begin
               FDQCashStatementLines.Open;
               FDQCashStatementLines.Insert;
               FDQCashStatementLinesLINEID.Value:=dmDBConnection.GetGenID('POSCASHSTATEMLINEID_GEN');
               FDQCashStatementLinesDATAAREAID.Value:=FDQCashStatementsDATAAREAID.Value;
               FDQCashStatementLinesBASEDATAAREAID.Value:=BaseDataAreaID;
            end;

            FDQCashStatementLinesCASHSTATEMENTID.Value:=FDQCashStatementsSTATEMENTID.Value;
            FDQCashStatementLinesPAYMENTTYPE.Value:=PaymentTypeID;
            FDQCashStatementLinesCURRENCY.Value:='DKK';
            FDQCashStatementLinesTRANSAMOUNT.Value:=TransAmount;
            FDQCashStatementLinesCOUNTEDAMOUNT.Value:=CountedAmount;
            FDQCashStatementLines.Post;
        except
            on E: Exception do
            begin
                LogFile(self,'Error saving Cashstatements Payments: '+E.Message, True);
            end;
        end;
    finally
        FDQCashStatementLines.Close;
    end;
end;

function TdmCashStatements.SaveStatementHeader(StatementID, POSClientID: integer; DateFrom, DateTo: TDateTime; EmplID: string; ADataAreaID: SmallInt): integer;
begin
    try
        FDQCashStatements.Close;
        FDQCashStatements.ParamByName('DATAAREAID').Value:=ADataAreaID;
        FDQCashStatements.ParamByName('CLIENTSTATEMENTID').Value:=StatementID;
        FDQCashStatements.ParamByName('POSCLIENTID').Value:=POSClientID;
        FDQCashStatements.Open;

        if (FDQCashStatements.RecordCount > 0) then
        begin
            FDQCashStatements.Edit;
        end
        else
        begin
            FDQCashStatements.Insert;
            FDQCashStatementsSTATEMENTID.Value:=dmDBConnection.GetGenID('POSCASHSTATEMENTID_GEN');
            FDQCashStatementsDATECREATED.AsDateTime:=Now;
            FDQCashStatementsCLIENTSTATEMENTID.Value:=StatementID;
        end;

        FDQCashStatementsDATAAREAID.Value:=ADataAreaID;
        FDQCashStatementsPOSCLIENTID.Value:=POSClientID;
        FDQCashStatementsTRANSDATETIMEFROM.AsDateTime:=DateFrom;
        FDQCashStatementsTRANSDATETIMETO.AsDateTime:=DateTo;
        FDQCashStatementsSTATUS.Value:=0;
        FDQCashStatementsEMPLID.AsString:=EmplID;
        FDQCashStatements.Post;

        result:=FDQCashStatementsSTATEMENTID.Value;
    except
        result:= -1;
    end;
end;

end.
