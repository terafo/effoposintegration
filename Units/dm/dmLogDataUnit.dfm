object dmBon: TdmBon
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 401
  Width = 606
  object FDQBonHeader: TFDQuery
    ActiveStoredUsage = [auDesignTime]
    Connection = dmDBConnection.FDConnection1
    UpdateObject = FDUpdBonHeader
    SQL.Strings = (
      'select * from bonjournal'
      
        'where DATAAREAID = :DATAAREAID AND POSCLIENTID = :POSCLIENTID AN' +
        'D BONID = :BONID'
      '')
    Left = 56
    Top = 40
    ParamData = <
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'POSCLIENTID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'BONID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object FDQBonHeaderBONID: TIntegerField
      FieldName = 'BONID'
      Origin = 'BONID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQBonHeaderBONDATETIME: TSQLTimeStampField
      FieldName = 'BONDATETIME'
      Origin = 'BONDATETIME'
      Required = True
    end
    object FDQBonHeaderPOSCLIENTID: TIntegerField
      FieldName = 'POSCLIENTID'
      Origin = 'POSCLIENTID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQBonHeaderBONSTATUS: TSmallintField
      FieldName = 'BONSTATUS'
      Origin = 'BONSTATUS'
      Required = True
    end
    object FDQBonHeaderEMPLID: TStringField
      FieldName = 'EMPLID'
      Origin = 'EMPLID'
      Required = True
      Size = 10
    end
    object FDQBonHeaderDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQBonHeaderBONAMOUNT: TCurrencyField
      FieldName = 'BONAMOUNT'
      Origin = 'BONAMOUNT'
    end
    object FDQBonHeaderPOSCASHSTATEMENTID: TIntegerField
      FieldName = 'POSCASHSTATEMENTID'
      Origin = 'POSCASHSTATEMENTID'
    end
    object FDQBonHeaderBOCASHSTATEMENTID: TIntegerField
      FieldName = 'BOCASHSTATEMENTID'
      Origin = 'BOCASHSTATEMENTID'
    end
    object FDQBonHeaderGENLEDGERID: TIntegerField
      FieldName = 'GENLEDGERID'
      Origin = 'GENLEDGERID'
    end
    object FDQBonHeaderSUMMARYINVOICE: TIntegerField
      FieldName = 'SUMMARYINVOICE'
      Origin = 'SUMMARYINVOICE'
    end
    object FDQBonHeaderRECID: TIntegerField
      FieldName = 'RECID'
      Origin = 'RECID'
    end
    object FDQBonHeaderREFID: TLargeintField
      FieldName = 'REFID'
      Origin = 'REFID'
    end
  end
  object FDUpdBonHeader: TFDUpdateSQL
    Connection = dmDBConnection.FDConnection1
    InsertSQL.Strings = (
      'INSERT INTO BONJOURNAL'
      '(BONID, BONDATETIME, POSCLIENTID, BONSTATUS, '
      '  EMPLID, DATAAREAID, BONAMOUNT, POSCASHSTATEMENTID, REFID)'
      
        'VALUES (:NEW_BONID, :NEW_BONDATETIME, :NEW_POSCLIENTID, :NEW_BON' +
        'STATUS, '
      
        '  :NEW_EMPLID, :NEW_DATAAREAID, :NEW_BONAMOUNT, :NEW_POSCASHSTAT' +
        'EMENTID, :NEW_REFID)')
    ModifySQL.Strings = (
      'UPDATE BONJOURNAL'
      
        'SET BONID = :NEW_BONID, BONDATETIME = :NEW_BONDATETIME, POSCLIEN' +
        'TID = :NEW_POSCLIENTID, '
      
        '  BONSTATUS = :NEW_BONSTATUS, EMPLID = :NEW_EMPLID, DATAAREAID =' +
        ' :NEW_DATAAREAID, '
      
        '  BONAMOUNT = :NEW_BONAMOUNT, POSCASHSTATEMENTID = :NEW_POSCASHS' +
        'TATEMENTID,'
      '  REFID = :NEW_REFID'
      
        'WHERE BONID = :OLD_BONID AND POSCLIENTID = :OLD_POSCLIENTID AND ' +
        'DATAAREAID = :OLD_DATAAREAID')
    DeleteSQL.Strings = (
      'DELETE FROM BONJOURNAL'
      
        'WHERE BONID = :OLD_BONID AND POSCLIENTID = :OLD_POSCLIENTID AND ' +
        'DATAAREAID = :OLD_DATAAREAID')
    FetchRowSQL.Strings = (
      
        'SELECT BONID, BONDATETIME, POSCLIENTID, BONSTATUS, EMPLID, DATAA' +
        'REAID, '
      
        '  BONAMOUNT, POSCASHSTATEMENTID, BOCASHSTATEMENTID, GENLEDGERID,' +
        ' '
      '  SUMMARYINVOICE, REFID'
      'FROM BONJOURNAL'
      
        'WHERE BONID = :BONID AND POSCLIENTID = :POSCLIENTID AND DATAAREA' +
        'ID = :DATAAREAID')
    Left = 144
    Top = 40
  end
  object FDQBonLines: TFDQuery
    AfterInsert = FDQBonLinesAfterInsert
    AfterEdit = FDQBonLinesAfterEdit
    AfterDelete = FDQBonLinesAfterDelete
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select * from bonjournallines'
      'where DATAAREAID = :DATAAREAID'
      'AND POSCLIENTID = :POSCLIENTID'
      'AND BONID = :BONID'
      'AND POSCLIENTLINEID = :POSCLIENTLINEID')
    Left = 56
    Top = 88
    ParamData = <
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'POSCLIENTID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'BONID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'POSCLIENTLINEID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object FDQBonLinesLINEID: TIntegerField
      FieldName = 'LINEID'
      Origin = 'LINEID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQBonLinesBONID: TIntegerField
      FieldName = 'BONID'
      Origin = 'BONID'
    end
    object FDQBonLinesPOSCLIENTID: TIntegerField
      FieldName = 'POSCLIENTID'
      Origin = 'POSCLIENTID'
    end
    object FDQBonLinesDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
    end
    object FDQBonLinesITEMID: TIntegerField
      FieldName = 'ITEMID'
      Origin = 'ITEMID'
    end
    object FDQBonLinesEAN: TStringField
      FieldName = 'EAN'
      Origin = 'EAN'
    end
    object FDQBonLinesITEMNAME: TStringField
      FieldName = 'ITEMNAME'
      Origin = 'ITEMNAME'
      Size = 100
    end
    object FDQBonLinesUNIT: TStringField
      FieldName = 'UNIT'
      Origin = 'UNIT'
      Size = 10
    end
    object FDQBonLinesVATRATE: TCurrencyField
      FieldName = 'VATRATE'
      Origin = 'VATRATE'
    end
    object FDQBonLinesLINESTATUS: TSmallintField
      FieldName = 'LINESTATUS'
      Origin = 'LINESTATUS'
    end
    object FDQBonLinesCOSTPRICEPRUNIT: TCurrencyField
      FieldName = 'COSTPRICEPRUNIT'
      Origin = 'COSTPRICEPRUNIT'
    end
    object FDQBonLinesLINEDATETIME: TSQLTimeStampField
      FieldName = 'LINEDATETIME'
      Origin = 'LINEDATETIME'
    end
    object FDQBonLinesLINEDISCOUNT: TFMTBCDField
      FieldName = 'LINEDISCOUNT'
      Origin = 'LINEDISCOUNT'
      Precision = 18
      Size = 5
    end
    object FDQBonLinesINVOICEDISCOUNT: TFMTBCDField
      FieldName = 'INVOICEDISCOUNT'
      Origin = 'INVOICEDISCOUNT'
      Precision = 18
      Size = 5
    end
    object FDQBonLinesGENLEDGERLINEID: TIntegerField
      FieldName = 'GENLEDGERLINEID'
      Origin = 'GENLEDGERLINEID'
    end
    object FDQBonLinesSALESPRICEINCVAT: TBCDField
      FieldName = 'SALESPRICEINCVAT'
      Origin = 'SALESPRICEINCVAT'
      Precision = 18
    end
    object FDQBonLinesSALESPRICEPRLINE: TFMTBCDField
      FieldName = 'SALESPRICEPRLINE'
      Origin = 'SALESPRICEPRLINE'
      Precision = 18
      Size = 5
    end
    object FDQBonLinesSALESPRICEPRLINEINCVAT: TFMTBCDField
      FieldName = 'SALESPRICEPRLINEINCVAT'
      Origin = 'SALESPRICEPRLINEINCVAT'
      Precision = 18
      Size = 7
    end
    object FDQBonLinesSALESPRICETOTAL: TFMTBCDField
      FieldName = 'SALESPRICETOTAL'
      Origin = 'SALESPRICETOTAL'
      Precision = 18
      Size = 5
    end
    object FDQBonLinesSALESPRICETOTALINCVAT: TFMTBCDField
      FieldName = 'SALESPRICETOTALINCVAT'
      Origin = 'SALESPRICETOTALINCVAT'
      Precision = 18
      Size = 7
    end
    object FDQBonLinesQTY: TFMTBCDField
      FieldName = 'QTY'
      Origin = 'QTY'
      Precision = 18
      Size = 3
    end
    object FDQBonLinesRECID: TIntegerField
      FieldName = 'RECID'
      Origin = 'RECID'
    end
    object FDQBonLinesPOSCLIENTLINEID: TIntegerField
      FieldName = 'POSCLIENTLINEID'
      Origin = 'POSCLIENTLINEID'
      Required = True
    end
    object FDQBonLinesSALESPRICEPRUNIT: TFMTBCDField
      FieldName = 'SALESPRICEPRUNIT'
      Origin = 'SALESPRICEPRUNIT'
      Precision = 18
      Size = 5
    end
  end
  object FDQPayments: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select * '
      'from bonpayment'
      'where DATAAREAID = :DATAAREAID'
      'AND POSCLIENTID = :POSCLIENTID'
      'AND BONID = :BONID'
      'AND POSCLIENTLINEID = :POSCLIENTLINEID')
    Left = 48
    Top = 144
    ParamData = <
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'POSCLIENTID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'BONID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'POSCLIENTLINEID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object FDQPaymentsLINEID: TIntegerField
      FieldName = 'LINEID'
      Origin = 'LINEID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQPaymentsBONID: TIntegerField
      FieldName = 'BONID'
      Origin = 'BONID'
    end
    object FDQPaymentsPAYMENTTYPE: TSmallintField
      FieldName = 'PAYMENTTYPE'
      Origin = 'PAYMENTTYPE'
    end
    object FDQPaymentsAMOUNT: TCurrencyField
      FieldName = 'AMOUNT'
      Origin = 'AMOUNT'
    end
    object FDQPaymentsPOSCLIENTID: TIntegerField
      FieldName = 'POSCLIENTID'
      Origin = 'POSCLIENTID'
    end
    object FDQPaymentsDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
    end
    object FDQPaymentsPOSCLIENTLINEID: TIntegerField
      FieldName = 'POSCLIENTLINEID'
      Origin = 'POSCLIENTLINEID'
      Required = True
    end
  end
  object FDMemFuelPosPaymMethods: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 64
    Top = 208
    object FDMemFuelPosPaymMethodsMOP: TSmallintField
      FieldName = 'MOP'
    end
    object FDMemFuelPosPaymMethodsMOP_ADDX: TSmallintField
      FieldName = 'MOP_ADDX'
    end
    object FDMemFuelPosPaymMethodsMOP_ADDY: TSmallintField
      FieldName = 'MOP_ADDY'
    end
    object FDMemFuelPosPaymMethodsROP: TSmallintField
      FieldName = 'ROP'
    end
    object FDMemFuelPosPaymMethodsPAYMENTMETHOD: TSmallintField
      FieldName = 'PAYMENTMETHOD'
    end
    object FDMemFuelPosPaymMethodsNODENAME: TStringField
      FieldName = 'NODENAME'
    end
  end
  object FDQRefLookup: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select BONID from bonjournal'
      'where REFID = :REFID'
      'ORDER BY BONID DESC'
      '')
    Left = 64
    Top = 280
    ParamData = <
      item
        Name = 'REFID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object FDQRefLookupBONID: TIntegerField
      FieldName = 'BONID'
      Origin = 'BONID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object FDQPaymentInfo: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select * from bonpaymentinfo'
      'where BONPAYMENTRECID = :BONPAYMENTRECID'
      '')
    Left = 176
    Top = 136
    ParamData = <
      item
        Name = 'BONPAYMENTRECID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object FDQPaymentInfoRECID: TIntegerField
      FieldName = 'RECID'
      Origin = 'RECID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQPaymentInfoBONPAYMENTRECID: TIntegerField
      FieldName = 'BONPAYMENTRECID'
      Origin = 'BONPAYMENTRECID'
      Required = True
    end
    object FDQPaymentInfoACCOUNTNO: TStringField
      FieldName = 'ACCOUNTNO'
      Origin = 'ACCOUNTNO'
    end
  end
  object FDQPaymentIDLookUp: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select B.PAYMENTTYPE, B.AMOUNT'
      'from bonpayment B'
      'left join paymenttype p'
      'on p.paymenttypeid = b.paymenttype'
      'where BONID = :BONID and P.paymentfunction = 3')
    Left = 168
    Top = 280
    ParamData = <
      item
        Name = 'BONID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 110152599
      end>
    object FDQPaymentIDLookUpPAYMENTTYPE: TSmallintField
      FieldName = 'PAYMENTTYPE'
      Origin = 'PAYMENTTYPE'
    end
    object FDQPaymentIDLookUpAMOUNT: TCurrencyField
      FieldName = 'AMOUNT'
      Origin = 'AMOUNT'
    end
  end
  object FDComBonStatus: TFDCommand
    Connection = dmDBConnection.FDConnection1
    CommandText.Strings = (
      'update bonjournal'
      'set bonstatus = :BONSTATUS'
      'where BONID = :BONID')
    ParamData = <
      item
        Name = 'BONSTATUS'
        ParamType = ptInput
      end
      item
        Name = 'BONID'
        ParamType = ptInput
      end>
    Left = 384
    Top = 176
  end
  object FDQItemCostPrice: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select KOSTPRISUR1 '
      'from varunr'
      'where varunrid = :ITEMID')
    Left = 368
    Top = 56
    ParamData = <
      item
        Name = 'ITEMID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object FDQItemCostPriceKOSTPRISUR1: TFMTBCDField
      FieldName = 'KOSTPRISUR1'
      Origin = 'KOSTPRISUR1'
      Precision = 18
      Size = 3
    end
  end
end
