unit dmMainUnit;

interface

uses
  System.SysUtils
  , FMX.Types
  , System.Classes
  , dmDBConnectionUnit
  , dmItemsUnit
  , SharedUnit
  , FireDAC.Stan.PAram
  , InterFaceUnit
  , FuelPOSUnit
  , TeraFunkUnit
  , FuelPOSImportUnit
  , IniFileUnit
  , FireDAC.Stan.Intf
  , FireDAC.Stan.Option
  , FireDAC.Stan.Error
  , FireDAC.DatS
  , FireDAC.Phys.Intf
  , FireDAC.DApt.Intf
  , FireDAC.Stan.Async
  , FireDAC.DApt
  , Data.DB
  , FireDAC.Comp.DataSet
  , FireDAC.Comp.Client
  ;

type
  TdmMain = class(TDataModule)
    FDQCompanies: TFDQuery;
    FDQCompaniesFYRITOKAID: TSmallintField;
    FDQCompaniesSTDPRISOLKUR: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
  private
    FTimer: TTimer;
    procedure LoadAllItemsFromDb;
    procedure LoadPriceChangeFromDb;
    procedure LoadCampaignItemsFromDb;
    function ProcessSelectedItems(var POSExport:IUPdatePOS; PriceGroup: SmallInt): integer;
    procedure LoadPromotionsFromDb(var POSExport: IUPdatePOS; StdPriceGroup: integer);
    procedure CreateTimer;
    procedure RunBatchJob(Sender: TOBject);
  public
    EnableBatchJob: Boolean;
    function ConnectToDB: boolean;
    procedure UpdateAll;
    procedure UpdatePriceChanges;
    procedure UpdateCampaigns;
    procedure DeleteAllCampaigns;
    procedure ProcessCashStatements;
    procedure ProcessBonData;
    procedure CreateVirtualStatements;
  end;

var
  dmMain: TdmMain;

implementation

uses
    System.IOUtils
    , FMX.Dialogs
    , dmVirtualPOSClientsUnit;

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

{ TdmMain }


function TdmMain.ConnectToDB: boolean;
begin
    //result:=false;
    dmDBConnection.FDConnection1.Connected:=True;
    result:=dmDBConnection.FDConnection1.Connected;
end;



procedure TdmMain.CreateTimer;
begin
    FTimer:=TTimer.Create(self);
    FTimer.Interval:=dmDBConnection.ProgramParams.BatchTimerInterval;
    FTimer.OnTimer:=RunBatchJob;
    FTimer.Enabled:=True;
end;

procedure TdmMain.CreateVirtualStatements;
var
    dmVirtStatements: TdmVirtualPOSClients;
begin
    dmVirtStatements:= TdmVirtualPOSClients.Create(nil);
    try
        dmVirtStatements.CreateShift;
    finally
        dmVirtStatements.Free;
    end;
end;

procedure TdmMain.DataModuleCreate(Sender: TObject);
begin
    EnableBatchJob:=False;
    CreateTimer;
end;

procedure TdmMain.DeleteAllCampaigns;
var
  POSExport: IUPdatePOS;
  LFilePaths: TFilePaths;
begin
    FDQCompanies.Open;
    try
        FDQCompanies.First;

        while not FDQCompanies.Eof do
        begin
            LFilePaths:=dmDBConnection.ProgramParams.GetFilePaths(FDQCompaniesFYRITOKAID.Value);
            POSExport:=TFuelPOSExport.Create(LFilePaths);
            POSExport.DeleteAllCampaigns;

            FDQCompanies.Next;
        end;
    finally
        dmDBConnection.FDConnection1.Close;
    end;
end;

procedure TdmMain.UpdateAll;
var
  POSExport: IUPdatePOS;
  LFilePaths: TFilePaths;
begin
    FDQCompanies.Open;
    try
        FDQCompanies.First;

        while not FDQCompanies.Eof do
        begin
            dmDBConnection.FDConnection1.StartTransaction;
            LFilePaths:=dmDBConnection.ProgramParams.GetFilePaths(FDQCompaniesFYRITOKAID.Value);
            LFilePaths.FPriceGroup:=FDQCompaniesSTDPRISOLKUR.Value;

            POSExport:=TFuelPOSExport.Create(LFilePaths);
            LoadAllItemsFromDb;
            ProcessSelectedItems(POSExport, FDQCompaniesSTDPRISOLKUR.Value);

            LoadPromotionsFromDb(POSExport, FDQCompaniesSTDPRISOLKUR.Value);
            POSExport.ExportAll;

            dmDBConnection.FDConnection1.Commit;
            FDQCompanies.Next;
        end;

        POSExport.RemoveUpdatedItemsFromPriceQueue;
    finally
        dmDBConnection.FDConnection1.Close;
    end;
end;

procedure TdmMain.UpdateCampaigns;
var
  POSExport: IUPdatePOS;
  LFilePaths: TFilePaths;
begin
    FDQCompanies.Open;
    try
        FDQCompanies.First;

        while not FDQCompanies.Eof do
        begin
            dmDBConnection.FDConnection1.StartTransaction;
            LFilePaths:=dmDBConnection.ProgramParams.GetFilePaths(FDQCompaniesFYRITOKAID.Value);

            POSExport:=TFuelPOSExport.Create(LFilePaths);
            LoadCampaignItemsFromDb;
            ProcessSelectedItems(POSExport, FDQCompaniesSTDPRISOLKUR.Value);

            LoadPromotionsFromDb(POSExport, FDQCompaniesSTDPRISOLKUR.Value);
            POSExport.ExportAll;

            dmDBConnection.FDConnection1.Commit;
            FDQCompanies.Next;
        end;
    finally
        dmDBConnection.FDConnection1.Close;
    end;
end;

procedure TdmMain.UpdatePriceChanges;
var
  POSExport: IUPdatePOS;
  LFilePaths: TFilePaths;
begin
    FDQCompanies.Open;
    try
        FDQCompanies.First;

        while not FDQCompanies.Eof do
        begin
            dmDBConnection.FDConnection1.StartTransaction;
            LFilePaths:=dmDBConnection.ProgramParams.GetFilePaths(FDQCompaniesFYRITOKAID.Value);

            POSExport:=TFuelPOSExport.Create(LFilePaths);
            LoadPriceChangeFromDb;

            if ProcessSelectedItems(POSExport, FDQCompaniesSTDPRISOLKUR.Value) > 0 then
                POSExport.ExportAll;

            dmDBConnection.FDConnection1.Commit;

            FDQCompanies.Next;
        end;

        POSExport.RemoveUpdatedItemsFromPriceQueue;
    finally
        dmDBConnection.FDConnection1.Close;
    end;
end;

function TdmMain.ProcessSelectedItems(var POSExport:IUPdatePOS; PriceGroup: SmallInt): integer;
var
  Item: TItem;
  ItemCount: integer;
  PriceGrpPrice: Currency;
begin
   ItemCount:=0;
   PriceGrpPrice:=0;

   with dmItems do
   begin
      while not FDQItems.Eof do
      begin
          inc(ItemCount);
          Item:=TItem.Create(FDQItemsITEMID.Value);
          item.Text:=FDQItemsVARUNAVN.AsString;
          item.ItemGrp:=FDQItemsITEMGRPID.AsString;
          item.DSKGrp:=FDQItemsUNDIRBOLKUR.AsString;
          Item.VatRate:=fdqitemsSATSUR.Value;
          Item.AllowPriceChange:= (FDQItemsSPERRAAVSLATTUR.Value = 0);
          Item.SetNegativQty(FDQItemsNEGATIVQUANTITY.AsInteger);

          if GetPriceFromPriceGroup(FDQItemsITEMID.Value,PriceGroup,PriceGrpPrice ) = true then
              Item.PriceExVat:=PriceGrpPrice
          else
              Item.PriceExVat:=FDQItemsSOLUPRISUR1.AsCurrency;

          Item.ClearBarcodes;
          FDQBarCodes.Close;
          FDQBarCodes.ParamByName('ITEMID').Value:=Item.ItemID;
          FDQBarCodes.Open;

          while not FDQBarCodes.Eof do
          begin
              Item.Barcode:=FDQBarCodesEAN.AsString;
              FDQBarCodes.Next;
          end;

          POSExport.UpdateItem(Item);
          FDQItems.Next;
      end;
   end;

   result:=ItemCount;
   LogFile(self,IntToStr(ItemCount)+' items loaded');
end;

procedure TdmMain.RunBatchJob(Sender: TOBject);
begin
    if EnableBatchJob = False then
       Exit;

    FTimer.Enabled:=False;
    try
        try
            ProcessBonData;
            ProcessCashStatements;
            CreateVirtualStatements;
        except
            LogFile(self,'Error BatchJob: GetBonData',True);
        end;
    finally
        FTimer.Enabled:=True;
    end;
end;


procedure TdmMain.ProcessBonData;
var
    ImportPOS: IImportFromPOS;
    LFilePaths: TFilePaths;
begin
    FDQCompanies.Open;
    FDQCompanies.First;

    while not FDQCompanies.Eof do
    begin
        LFilePaths:=dmDBConnection.ProgramParams.GetFilePaths(FDQCompaniesFYRITOKAID.Value);
        ImportPOS:=TFuelPOSImport.Create(LFilePaths);
        ImportPOS.ProcessBonData;

        FDQCompanies.Next;
    end;
end;

procedure TdmMain.ProcessCashStatements;
var
    ImportPOS: IImportFromPOS;
    LFilePaths: TFilePaths;
begin
    FDQCompanies.Open;
    FDQCompanies.First;

    while not FDQCompanies.Eof do
    begin
        LFilePaths:=dmDBConnection.ProgramParams.GetFilePaths(FDQCompaniesFYRITOKAID.Value);
        ImportPOS:=TFuelPOSImport.Create(LFilePaths);;
        ImportPOS.ProcessCashStatements;

        FDQCompanies.Next;
    end;
end;

procedure TdmMain.LoadAllItemsFromDb;
const
    SqlCmd =    'select r.itemid, r.itemgrpid, v.varunavn, v.mvg, v.soluprisur1, v.sperraavslattur, ' +
    'm.satsur, v.negativquantity, v.undirbolkur ' +
    'from itemgrprelation r ' +
    'left join itemgrpname n ' +
    'on r.itemgrpnameid = n.grpnameid ' +
    'left join itemgrpheader h ' +
    'on n.grpheaderid = h.headerid ' +
    'left join varunr v ' +
    'on v.varunrid = r.itemid ' +
    'left join mvgkotur m ' +
    'on m.mvgid = v.mvg ' +
    'where h.headerid = :HEADERID ' +
    'and v.DATAAREAID = :DATAAREAID ' +
    'order by varunrid ';
begin
    with dmItems do
    begin
        FDQItems.Close;
        FDQItems.SQL.Text:=SqlCmd;
        FDQItems.ParamByName('HEADERID').Value:=dmDBConnection.ProgramParams.ExtItemGroupHeaderID;
        FDQItems.ParamByName('DATAAREAID').Value:= BaseDataAreaID; // Stamdata parametur
        FDQItems.Open;
    end;
end;


procedure TdmMain.LoadCampaignItemsFromDb;
const
    SqlCmd =   'select distinct(l.varunrid) as itemid, r.itemgrpid, v.varunavn, v.mvg, v.soluprisur1, v.sperraavslattur, ' +
    'm.satsur, v.negativquantity, v.undirbolkur ' +
    'from mixrabatlinjur l ' +
    'left join mixrabathovd h ' +
    'on l.mixrabatnr = h.tilbodnr ' +
    'left join varunr v ' +
    'on v.varunrid = l.varunrid ' +
    'left join mvgkotur m ' +
    'on m.mvgid = v.mvg ' +
    'left join itemgrprelation r ' +
    'on l.varunrid = r.itemid ' +
    'left join itemgrpname n ' +
    'on r.itemgrpnameid = n.grpnameid ' +
    'left join itemgrpheader gh ' +
    'on n.grpheaderid = gh.headerid ' +
    'where h.enda >= :CURRENTDATE ' +
    'and gh.headerid = :HEADERID ' +
    'and v.DATAAREAID = :DATAAREAID ' +
    'order by varunrid ';
begin
    with dmItems do
    begin
        FDQItems.Close;
        FDQItems.SQL.Text:=SqlCmd;
        FDQItems.ParamByName('CURRENTDATE').Value:=Date;
        FDQItems.ParamByName('HEADERID').Value:=dmDBConnection.ProgramParams.ExtItemGroupHeaderID;
        FDQItems.ParamByName('DATAAREAID').Value:=BaseDataAreaID; //Skal ver�a stamdata;
        FDQItems.Open;
    end;
end;

procedure TdmMain.LoadPriceChangeFromDb;
const
    SqlCmd =   'select r.itemid, r.itemgrpid, v.varunavn, v.mvg, v.soluprisur1, v.sperraavslattur, ' +
    'v.negativquantity, m.satsur, v.undirbolkur ' +
    'from itemgrprelation r ' +
    'left join itemgrpname n ' +
    'on r.itemgrpnameid = n.grpnameid ' +
    'left join itemgrpheader h ' +
    'on n.grpheaderid = h.headerid ' +
    'left join kassarprisuppdatering u ' +
    'on u.varunrid = r.itemid ' +
    'left join varunr v ' +
    'on v.varunrid = u.varunrid ' +
    'left join mvgkotur m ' +
    'on m.mvgid = v.mvg ' +
    'where h.headerid = :HEADERID ' +
    'and v.DATAAREAID = :DATAAREAID ' +
    'order by varunrid ';
begin
    with dmItems do
    begin
        FDQItems.Close;
        FDQItems.SQL.Text:=SqlCmd;
        FDQItems.ParamByName('HEADERID').Value:=dmDBConnection.ProgramParams.ExtItemGroupHeaderID;
        FDQItems.ParamByName('DATAAREAID').Value:=BaseDataAreaID; // Stamdata parametur
        FDQItems.Open;
    end;
end;

procedure TdmMain.LoadPromotionsFromDb(var POSExport: IUPdatePOS; StdPriceGroup: integer);
var
  Promo: TPromotion;
  PromoLine: TPromotionLine;
begin
    try
        with dmItems do
        begin
            FDQPromotionHeader.ParamByName('CURRENTDATE').AsDate:=Now;
            FDQPromotionHeader.ParamByName('DATAAREAID').Value:=BaseDataAreaID;
            FDQPromotionHeader.Open;
            FDQPromotionHeader.First;

            while not FDQPromotionHeader.Eof do
            begin
                if CheckIfCompanyIsOnCampaignPriceGroup(StdPriceGroup) = false then
                begin
                    FDQPromotionHeader.Next;
                    Continue;
                end;


                Promo:=TPromotion.Create(FDQPromotionHeaderTILBODNR.Value);
                promo.Name:=FDQPromotionHeaderLINJUTEKSTUR.AsString;
                Promo.FromDate:=FDQPromotionHeaderBYRJA.AsDateTime;
                promo.ToDate:=FDQPromotionHeaderENDA.AsDateTime;
                Promo.TotalPromoPrice:=FDQPromotionHeaderTILBODSPRISUR.Value;



                // Load PromotionLines
                FDQPromoLines.Close;
                FDQPromoLines.ParamByName('PROMOTIONID').Value:=FDQPromotionHeaderTILBODNR.Value;
                FDQPromoLines.Open;
                FDQPromoLines.First;

                while not FDQPromoLines.eof do
                begin
                    PromoLine.ItemID:= FDQPromoLinesVARUNRID.Value;
                    PromoLine.PromPrice:=Promo.TotalPromoPrice;
                    PromoLine.NormalPrice:=FDQPromoLinesSalesprice.AsCurrency;
                    PromoLine.Qty:= FDQPromoLinesNOGD.Value;
                    PromoLine.Level:=FDQPromoLinesNIVEU.Value;
                    Promo.AddLine(PromoLine);

                    FDQPromoLines.Next;
                end;

                if (Promo.PromoType = ptGroup) then
                   Promo.CalcPromoLinePrice;

                POSExport.UpdatePromotion(Promo);
                FDQPromotionHeader.Next;
            end;

            FDQPromotionHeader.Close;
            FDQPromoLines.Close;
        end;
    except
        on E: Exception do
        begin
            Logfile(self,'Error tilbo�: '+dmItems.FDQPromotionHeaderTILBODNR.AsString+': '+E.Message, True);
        end;
    end;
end;

end.
