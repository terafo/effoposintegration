unit dmDBConnectionUnit;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.IB,
  FireDAC.Phys.IBDef, FireDAC.FMXUI.Wait, Data.DB, FireDAC.Comp.Client,
  IniFileUnit, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet;

type

  TdmDBConnection = class(TDataModule)
    FDConnection1: TFDConnection;
    FDTransaction1: TFDTransaction;
    FDQGetGenID: TFDQuery;
    procedure FDConnection1BeforeConnect(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FProgramParams: TProgramParams;
    function GetProgramParams: TProgramParams;
  public
    function GetGenID(GeneratorName: string):integer;
    property ProgramParams: TProgramParams read GetProgramParams;
  end;

var
  dmDBConnection: TdmDBConnection;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TdmDBConnection.DataModuleCreate(Sender: TObject);
begin
    FProgramParams:=TProgramParams.Create;
end;

procedure TdmDBConnection.DataModuleDestroy(Sender: TObject);
begin
    ProgramParams.Free;
end;

procedure TdmDBConnection.FDConnection1BeforeConnect(Sender: TObject);
begin
    FDConnection1.Params.Clear;
    FDConnection1.Params.DriverID:='IB';  //DBParams.IP
    FDConnection1.Params.UserName:='SYSDBA';
    FDConnection1.Params.Password:='masterkey';
    FDConnection1.Params.Add('Database='+ ProgramParams.DBParams.Path);
    FDConnection1.Params.Add('Server='+ProgramParams.DBParams.IP);
end;

function TdmDBConnection.GetGenID(GeneratorName: string): integer;
begin
    with FDQGetGenID do
    begin
        SQL.Clear;
        SQL.ADD('SELECT GEN_ID ('+GeneratorName+', 1) from rdb$database');
        Open;

        // Fields 0 er fyrsta felt � Queryini.
        Result := Fields [0].AsInteger;
        Close;
    end;
end;

function TdmDBConnection.GetProgramParams: TProgramParams;
begin
    result:=FProgramParams;
end;

end.
