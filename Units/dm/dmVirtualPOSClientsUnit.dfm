object dmVirtualPOSClients: TdmVirtualPOSClients
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 430
  Width = 617
  object FDQBonHeader: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      
        'select b.bonid, b.bondatetime, b.posclientid, p.paymenttype, p.a' +
        'mount'
      'from bonjournal b'
      'left join bonpayment p'
      
        'on b.dataareaid = p.dataareaid and b.posclientid = p.posclientid' +
        ' and b.bonid = p.bonid'
      'where POSCLIENTID = :POSCLIENTID'
      
        'and POSCASHSTATEMENTID = -1 and  cast(B.BONDATETIME as Date) < :' +
        'CURRENTDATE'
      'and b.dataareaid = :DATAAREAID'
      'order by BONDATETIME')
    Left = 80
    Top = 72
    ParamData = <
      item
        Name = 'POSCLIENTID'
        DataType = ftString
        ParamType = ptInput
        Value = '54'
      end
      item
        Name = 'CURRENTDATE'
        DataType = ftDate
        ParamType = ptInput
        Value = 43751d
      end
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
        ParamType = ptInput
      end>
    object FDQBonHeaderBONID: TIntegerField
      FieldName = 'BONID'
      Origin = 'BONID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQBonHeaderBONDATETIME: TSQLTimeStampField
      FieldName = 'BONDATETIME'
      Origin = 'BONDATETIME'
      Required = True
    end
    object FDQBonHeaderPOSCLIENTID: TIntegerField
      FieldName = 'POSCLIENTID'
      Origin = 'POSCLIENTID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQBonHeaderPAYMENTTYPE: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'PAYMENTTYPE'
      Origin = 'PAYMENTTYPE'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDQBonHeaderAMOUNT: TCurrencyField
      AutoGenerateValue = arDefault
      FieldName = 'AMOUNT'
      Origin = 'AMOUNT'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object FDComUpdateBon: TFDCommand
    Connection = dmDBConnection.FDConnection1
    CommandText.Strings = (
      'update bonjournal'
      'set POSCASHSTATEMENTID = :STATEMENTID'
      'where bonid = :BONID')
    ParamData = <
      item
        Name = 'STATEMENTID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'BONID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    Left = 280
    Top = 280
  end
end
