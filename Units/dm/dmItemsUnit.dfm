object dmItems: TdmItems
  OldCreateOrder = False
  Height = 319
  Width = 510
  object FDQItems: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      
        'select r.itemid, r.itemgrpid, v.varunavn, v.mvg, v.soluprisur1, ' +
        'v.sperraavslattur,'
      'm.satsur, negativquantity, v.undirbolkur'
      'from itemgrprelation r'
      'left join itemgrpname n'
      'on r.itemgrpnameid = n.grpnameid'
      'left join itemgrpheader h'
      'on n.grpheaderid = h.headerid'
      'left join varunr v'
      'on v.varunrid = r.itemid'
      'left join mvgkotur m'
      'on m.mvgid = v.mvg'
      'where h.headerid = :HEADERID'
      'and v.DATAAREAID = :DATAAREAID'
      'order by varunrid')
    Left = 48
    Top = 40
    ParamData = <
      item
        Name = 'HEADERID'
        DataType = ftWideString
        ParamType = ptInput
        Value = '10'
      end
      item
        Name = 'DATAAREAID'
        DataType = ftWideString
        ParamType = ptInput
        Value = '1'
      end>
    object FDQItemsITEMID: TIntegerField
      FieldName = 'ITEMID'
      Origin = 'ITEMID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQItemsITEMGRPID: TStringField
      FieldName = 'ITEMGRPID'
      Origin = 'ITEMGRPID'
      Required = True
      Size = 8
    end
    object FDQItemsVARUNAVN: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'VARUNAVN'
      Origin = 'VARUNAVN'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object FDQItemsMVG: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'MVG'
      Origin = 'MVG'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 3
    end
    object FDQItemsSOLUPRISUR1: TFMTBCDField
      AutoGenerateValue = arDefault
      FieldName = 'SOLUPRISUR1'
      Origin = 'SOLUPRISUR1'
      ProviderFlags = []
      ReadOnly = True
      Precision = 18
      Size = 3
    end
    object FDQItemsSPERRAAVSLATTUR: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'SPERRAAVSLATTUR'
      Origin = 'SPERRAAVSLATTUR'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDQItemsSATSUR: TCurrencyField
      AutoGenerateValue = arDefault
      FieldName = 'SATSUR'
      Origin = 'SATSUR'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDQItemsNEGATIVQUANTITY: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'NEGATIVQUANTITY'
      Origin = 'NEGATIVQUANTITY'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDQItemsUNDIRBOLKUR: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'UNDIRBOLKUR'
      Origin = 'UNDIRBOLKUR'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object FDQBarCodes: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select VARUNRID, EAN '
      'from ean'
      'where varunrid = :ITEMID')
    Left = 48
    Top = 96
    ParamData = <
      item
        Name = 'ITEMID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 10
      end>
    object FDQBarCodesVARUNRID: TIntegerField
      FieldName = 'VARUNRID'
      Origin = 'VARUNRID'
      Required = True
    end
    object FDQBarCodesEAN: TStringField
      FieldName = 'EAN'
      Origin = 'EAN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object FDQPromotionHeader: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      
        'select TILBODNR, TILBODSPRISUR, BYRJA, ENDA, LINJUTEKSTUR, DATAA' +
        'REAID '
      'from mixrabathovd'
      'where ENDA >= :CURRENTDATE and DATAAREAID = :DATAAREAID'
      'order by TILBODNR')
    Left = 48
    Top = 160
    ParamData = <
      item
        Name = 'CURRENTDATE'
        DataType = ftWideString
        ParamType = ptInput
        Value = '01-02-2019'
      end
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
        ParamType = ptInput
      end>
    object FDQPromotionHeaderTILBODNR: TIntegerField
      FieldName = 'TILBODNR'
      Origin = 'TILBODNR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQPromotionHeaderTILBODSPRISUR: TCurrencyField
      FieldName = 'TILBODSPRISUR'
      Origin = 'TILBODSPRISUR'
    end
    object FDQPromotionHeaderBYRJA: TDateField
      FieldName = 'BYRJA'
      Origin = 'BYRJA'
    end
    object FDQPromotionHeaderENDA: TDateField
      FieldName = 'ENDA'
      Origin = 'ENDA'
    end
    object FDQPromotionHeaderLINJUTEKSTUR: TStringField
      FieldName = 'LINJUTEKSTUR'
      Origin = 'LINJUTEKSTUR'
      Required = True
      Size = 50
    end
    object FDQPromotionHeaderDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object FDQPromoLines: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select l.*, (v.soluprisur1 * ((m.satsur+100)/100)) as SALESPRICE'
      'from mixrabatlinjur l'
      'left join varunr v'
      'on l.varunrid = v.varunrid'
      'left join mvgkotur m'
      'on v.mvg = m.mvgid'
      'where mixrabatnr = :PROMOTIONID')
    Left = 48
    Top = 216
    ParamData = <
      item
        Name = 'PROMOTIONID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 1
      end>
    object FDQPromoLinesLINJUID: TIntegerField
      FieldName = 'LINJUID'
      Origin = 'LINJUID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQPromoLinesMIXRABATNR: TIntegerField
      FieldName = 'MIXRABATNR'
      Origin = 'MIXRABATNR'
      Required = True
    end
    object FDQPromoLinesVARUNRID: TIntegerField
      FieldName = 'VARUNRID'
      Origin = 'VARUNRID'
      Required = True
    end
    object FDQPromoLinesKOSTPRISUR: TCurrencyField
      FieldName = 'KOSTPRISUR'
      Origin = 'KOSTPRISUR'
    end
    object FDQPromoLinesNIVEU: TSmallintField
      FieldName = 'NIVEU'
      Origin = 'NIVEU'
      Required = True
    end
    object FDQPromoLinesNOGD: TCurrencyField
      FieldName = 'NOGD'
      Origin = 'NOGD'
      Required = True
    end
    object FDQPromoLinesDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      Required = True
    end
    object FDQPromoLinesSALESPRICE: TFMTBCDField
      AutoGenerateValue = arDefault
      FieldName = 'SALESPRICE'
      Origin = 'SALESPRICE'
      ProviderFlags = []
      ReadOnly = True
      Precision = 18
      Size = 5
    end
  end
  object FDQPriceUpdate: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select distinct(varunrid) '
      'from kassarprisuppdatering')
    Left = 152
    Top = 40
  end
  object FDQDeleteItem: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'delete from kassarprisuppdatering'
      'where varunrid = :ITEMID')
    Left = 144
    Top = 104
    ParamData = <
      item
        Name = 'ITEMID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object FDQPriceGroups: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select SALESPRICE from pricegrouplines'
      'where pricegroupid = :PRICEGROUPID AND ITEMID = :ITEMID')
    Left = 248
    Top = 40
    ParamData = <
      item
        Name = 'PRICEGROUPID'
        DataType = ftSmallint
        ParamType = ptInput
        Value = 2
      end
      item
        Name = 'ITEMID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 175893
      end>
    object FDQPriceGroupsSALESPRICE: TFMTBCDField
      FieldName = 'SALESPRICE'
      Origin = 'SALESPRICE'
      Precision = 18
      Size = 3
    end
  end
  object FDQPromoPriceGroups: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select R.PRICEGROUPID'
      'from rel_mixcampaignpricegroup R'
      
        'where R.mixcampaignid = :MIXCAMPID AND R.DATAAREAID = :DATAAREAI' +
        'D')
    Left = 184
    Top = 160
    ParamData = <
      item
        Name = 'MIXCAMPID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
        ParamType = ptInput
      end>
    object FDQPromoPriceGroupsPRICEGROUPID: TIntegerField
      FieldName = 'PRICEGROUPID'
      Origin = 'PRICEGROUPID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
end
