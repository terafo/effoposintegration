unit dmLogDataUnit;

interface

uses
  System.SysUtils
  , System.Classes
  , dmDBConnectionUnit
  , FireDAC.Stan.Intf
  , FireDAC.Stan.Option
  , FireDAC.Stan.Param
  , FireDAC.Stan.Error
  , FireDAC.DatS
  , FireDAC.Phys.Intf
  , FireDAC.DApt.Intf
  , FireDAC.Stan.Async
  , FireDAC.DApt
  , Data.DB
  , FireDAC.Comp.DataSet
  , FireDAC.Comp.Client
  , IniFileUnit
  , SharedUnit
  , TeraFunkUnit
  , System.IOUtils
  , System.Variants
  , dmItemTransactionUnit
  ;

type
  TdmBon = class(TDataModule)
    FDQBonHeader: TFDQuery;
    FDQBonHeaderBONID: TIntegerField;
    FDQBonHeaderBONDATETIME: TSQLTimeStampField;
    FDQBonHeaderPOSCLIENTID: TIntegerField;
    FDQBonHeaderBONSTATUS: TSmallintField;
    FDQBonHeaderEMPLID: TStringField;
    FDQBonHeaderDATAAREAID: TSmallintField;
    FDQBonHeaderBONAMOUNT: TCurrencyField;
    FDQBonHeaderPOSCASHSTATEMENTID: TIntegerField;
    FDQBonHeaderBOCASHSTATEMENTID: TIntegerField;
    FDQBonHeaderGENLEDGERID: TIntegerField;
    FDQBonHeaderSUMMARYINVOICE: TIntegerField;
    FDUpdBonHeader: TFDUpdateSQL;
    FDQBonLines: TFDQuery;
    FDQBonLinesLINEID: TIntegerField;
    FDQBonLinesBONID: TIntegerField;
    FDQBonLinesPOSCLIENTID: TIntegerField;
    FDQBonLinesDATAAREAID: TSmallintField;
    FDQBonLinesITEMID: TIntegerField;
    FDQBonLinesEAN: TStringField;
    FDQBonLinesITEMNAME: TStringField;
    FDQBonLinesUNIT: TStringField;
    FDQBonLinesVATRATE: TCurrencyField;
    FDQBonLinesLINESTATUS: TSmallintField;
    FDQBonLinesCOSTPRICEPRUNIT: TCurrencyField;
    FDQBonLinesLINEDATETIME: TSQLTimeStampField;
    FDQBonLinesLINEDISCOUNT: TFMTBCDField;
    FDQBonLinesINVOICEDISCOUNT: TFMTBCDField;
    FDQBonLinesGENLEDGERLINEID: TIntegerField;
    FDQBonLinesSALESPRICEINCVAT: TBCDField;
    FDQBonLinesSALESPRICEPRLINE: TFMTBCDField;
    FDQBonLinesSALESPRICEPRLINEINCVAT: TFMTBCDField;
    FDQBonLinesSALESPRICETOTAL: TFMTBCDField;
    FDQBonLinesSALESPRICETOTALINCVAT: TFMTBCDField;
    FDQBonLinesQTY: TFMTBCDField;
    FDQPayments: TFDQuery;
    FDQPaymentsLINEID: TIntegerField;
    FDQPaymentsBONID: TIntegerField;
    FDQPaymentsPAYMENTTYPE: TSmallintField;
    FDQPaymentsAMOUNT: TCurrencyField;
    FDQPaymentsPOSCLIENTID: TIntegerField;
    FDQPaymentsDATAAREAID: TSmallintField;
    FDMemFuelPosPaymMethods: TFDMemTable;
    FDMemFuelPosPaymMethodsMOP: TSmallintField;
    FDMemFuelPosPaymMethodsMOP_ADDX: TSmallintField;
    FDMemFuelPosPaymMethodsMOP_ADDY: TSmallintField;
    FDMemFuelPosPaymMethodsROP: TSmallintField;
    FDMemFuelPosPaymMethodsPAYMENTMETHOD: TSmallintField;
    FDQBonLinesRECID: TIntegerField;
    FDQBonLinesPOSCLIENTLINEID: TIntegerField;
    FDQPaymentsPOSCLIENTLINEID: TIntegerField;
    FDMemFuelPosPaymMethodsNODENAME: TStringField;
    FDQRefLookup: TFDQuery;
    FDQRefLookupBONID: TIntegerField;
    FDQPaymentInfo: TFDQuery;
    FDQPaymentInfoRECID: TIntegerField;
    FDQPaymentInfoBONPAYMENTRECID: TIntegerField;
    FDQPaymentInfoACCOUNTNO: TStringField;
    FDQPaymentIDLookUp: TFDQuery;
    FDQPaymentIDLookUpPAYMENTTYPE: TSmallintField;
    FDQBonHeaderRECID: TIntegerField;
    FDQPaymentIDLookUpAMOUNT: TCurrencyField;
    FDComBonStatus: TFDCommand;
    FDQBonLinesSALESPRICEPRUNIT: TFMTBCDField;
    FDQItemCostPrice: TFDQuery;
    FDQItemCostPriceKOSTPRISUR1: TFMTBCDField;
    FDQBonHeaderREFID: TLargeintField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure FDQBonLinesAfterEdit(DataSet: TDataSet);
    procedure FDQBonLinesAfterDelete(DataSet: TDataSet);
    procedure FDQBonLinesAfterInsert(DataSet: TDataSet);
  private
    FItemTrans: IItemTrans;
    FDataAreaID: SmallInt;
    function GetItemCostPrice(ItemID: integer): Extended;
  public
    procedure SaveBonHeader(Fields: TBonHeaderFields);
    function SaveBonLines(BonLine: TBonLineFields): Boolean;
    procedure SavePayment(Payment: TBonPayment);
    procedure SaveAccountInfo(AccountNo: string; PaymentID: integer);
    function GetBonIDFromRefID(const RefID: int64): integer;
    function GetBonPaymentType(const BonID: int64): TBonPayment;
    function GetPaymentMethod(BonID, PosClientID: integer; MOP, MOP_ADDX, MOP_ADDY, ROP: SmallInt): SmallInt;
    function GetPaymMethodByNodeName(NodeName: string):SmallInt;
    procedure UpdateBonStatus(BonID: integer; Status: SmallInt);
    constructor Create(AOwner: TComponent; DataAreaID: Smallint); reintroduce; overload;
  end;



implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

const
  TABLE_BONJOURNAL = 34;
  TABLE_BONJOURNALLINES = 35;

{$R *.dfm}

{ TdmBon }


constructor TdmBon.Create(AOwner: TComponent; DataAreaID: Smallint);
begin
    inherited Create(AOwner);
    FDataAreaID:=DataAreaID;
end;

procedure TdmBon.DataModuleCreate(Sender: TObject);
begin
    if TFile.Exists(dmDBConnection.ProgramParams.PaymentTable) then
        FDMemFuelPosPaymMethods.LoadFromFile(dmDBConnection.ProgramParams.PaymentTable);

    FItemTrans:=TdmItemTransaction.Create(self);
end;

procedure TdmBon.DataModuleDestroy(Sender: TObject);
begin
    FItemTrans:=nil;
end;

procedure TdmBon.FDQBonLinesAfterDelete(DataSet: TDataSet);
begin
   //
end;

procedure TdmBon.FDQBonLinesAfterEdit(DataSet: TDataSet);
begin
    //
end;

procedure TdmBon.FDQBonLinesAfterInsert(DataSet: TDataSet);
begin
    //
end;

function TdmBon.GetBonIDFromRefID(const RefID: int64): integer;
begin
    Result:=-1;
    try
        try
            FDQRefLookup.ParamByName('REFID').AsLargeInt := RefID;
            FDQRefLookup.Open;

            if (FDQRefLookupBONID.AsInteger > 0) then
                result:=FDQRefLookupBONID.AsLargeInt;
        finally
            FDQRefLookup.Close;
        end;
    except
        on E: Exception do
        begin
            LogFile(nil,'ERROR: GetBonIDFromRefID. RefID: '+RefID.ToString+': '+E.Message , True);
        end;
    end;
end;

function TdmBon.GetBonPaymentType(const BonID: int64): TBonPayment;
begin
    result.FPaymMethod:=-1;
    try
        try
            FDQPaymentIDLookUp.ParamByName('BONID').Value:=BonID;
            FDQPaymentIDLookUp.Open;
            result.FPaymMethod:=FDQPaymentIDLookUpPAYMENTTYPE.AsInteger;
            result.FPaydAmount:=FDQPaymentIDLookUpAMOUNT.AsFloat;
        finally
            FDQPaymentIDLookUp.Close;
        end;
    except
        on E: Exception do
        begin
            LogFile(nil,'ERROR: GetBonPaymentType. BonID: '+BonID.ToString+': '+E.Message , True);
        end;
    end;
end;

function TdmBon.GetItemCostPrice(ItemID: integer): Extended;
begin
    try
      FDQItemCostPrice.ParamByName('ITEMID').AsInteger:=ItemID;
      FDQItemCostPrice.Open;

      if not FDQItemCostPriceKOSTPRISUR1.IsNull then
          result:=FDQItemCostPriceKOSTPRISUR1.AsFloat
      else
          Result:=0;
    finally
      FDQItemCostPrice.Close;
    end;
end;

function TdmBon.GetPaymentMethod(BonID, PosClientID: integer; MOP, MOP_ADDX, MOP_ADDY,
  ROP: SmallInt): SmallInt;
begin
    if FDMemFuelPosPaymMethods.Locate('MOP;MOP_ADDX;MOP_ADDY;ROP', VarArrayOf([MOP, MOP_ADDX, MOP_ADDY, ROP]),[]) then
    begin
        result:=FDMemFuelPosPaymMethodsPAYMENTMETHOD.Value
    end
    else
    begin
        result:=-1;
        LogFile(self,'ERROR: PamentMethod Missing: '
                +'BonID: '+IntToStr(BonID)
                +' PosClientID: '+IntToStr(PosClientID)+#13
                +'                     MOP: '+IntToStr(MOP)
                +' MOP_ADDX: '+IntToStr(MOP_ADDX)
                +' MOP_ADDY: '+IntToStr(MOP_ADDY)
                +' ROP: '+IntToStr(ROP)
                , True
                );
    end;
end;

function TdmBon.GetPaymMethodByNodeName(NodeName: string): SmallInt;
begin
    if FDMemFuelPosPaymMethods.Locate('NODENAME', NodeName,[]) then
    begin
        result:=FDMemFuelPosPaymMethodsPAYMENTMETHOD.Value
    end
    else
    begin
        result:=-1;
        LogFile(self,'ERROR: PamentMethod Missing: '+NodeName, True);
    end;
end;

procedure TdmBon.SaveAccountInfo(AccountNo: string; PaymentID: integer);
begin
    FDQPaymentInfo.Open;
    try
        FDQPaymentInfo.Insert;
        FDQPaymentInfoRECID.Value:=dmDBConnection.GetGenID('BONPAYMENTINFORECID_GEN');
        FDQPaymentInfoBONPAYMENTRECID.Value:=PaymentID;
        FDQPaymentInfoACCOUNTNO.AsString:=AccountNo;
        FDQPaymentInfo.Post;
    finally
        FDQPaymentInfo.Close;
    end;
end;

procedure TdmBon.SaveBonHeader(Fields: TBonHeaderFields);
begin
    FDQBonHeader.ParamByName('BONID').Value:=Fields.BonID;
    FDQBonHeader.ParamByName('POSCLIENTID').Value:=Fields.PosClientID;
    FDQBonHeader.ParamByName('DATAAREAID').Value:=FDataAreaID;
    FDQBonHeader.Open;
    try
        if (FDQBonHeader.RecordCount > 0) then
            FDQBonHeader.Edit
        else
            FDQBonHeader.Insert;

        FDQBonHeaderBONID.Value:= Fields.BonID;
        FDQBonHeaderBONDATETIME.AsDateTime:= Fields.BonDateTime;
        FDQBonHeaderPOSCLIENTID.Value:= Fields.PosClientID;
        FDQBonHeaderBONSTATUS.Value:= Fields.BonStatus;
        FDQBonHeaderEMPLID.AsString:= Fields.EmplID;
        FDQBonHeaderDATAAREAID.Value:= FDataAreaID;
        FDQBonHeaderPOSCASHSTATEMENTID.Value:=Fields.PosCashStatementID;
        FDQBonHeaderBONAMOUNT.Value:= Fields.BonAmount;
        FDQBonHeaderREFID.Value:=Fields.Ref;
        FDQBonHeader.Post;

        FDQBonHeader.Close;
    except
        FDQBonHeader.Cancel;
        FDQBonHeader.Close;
        LogFile(self,'Error BONID: '+IntToStr(Fields.BonID),True);
    end;
end;

function TdmBon.SaveBonLines(BonLine: TBonLineFields): Boolean;
type
    TUpdateState = (sInsert,sUpdate);
var
    ItemTrans: TItemTransRecord;
    UpdateState: TupdateState;
    OLD_BonLineID: integer;
begin
    result:=false;
    OLD_BonLIneID:=0;

    dmDBConnection.FDConnection1.StartTransaction;
    try
        FDQBonLines.ParamByName('DATAAREAID').Value:=FDataAreaID;
        FDQBonLines.ParamByName('POSCLIENTID').Value:=BonLine.FPOSClientID;
        FDQBonLines.ParamByName('BONID').Value:=BonLine.FBonID;
        FDQBonLines.ParamByName('POSCLIENTLINEID').Value:=Bonline.FLineNo;
        FDQBonLines.Open;

        try
            if FDQBonLines.RecordCount = 0 then
            begin
                FDQBonLines.Insert;
                UpdateState := sInsert;
                FDQBonLinesLINEID.Value:=dmDBConnection.GetGenID('BONDETAIL_GEN');
            end
            else
            begin
                OLD_BonLineID:=FDQBonLinesLINEID.Value;
                FDQBonLines.Edit;
                UpdateState := sUpdate;
            end;

            try
                FDQBonLinesCOSTPRICEPRUNIT.Value:=GetItemCostPrice(BonLine.FItemID);
                FDQBonLinesBONID.Value:=BonLine.FBonID;
                FDQBonLinesPOSCLIENTID.Value:=BonLine.FPOSClientID;
                FDQBonLinesDATAAREAID.Value:=FDataAreaID;
                FDQBonLinesITEMID.Value:=BonLine.FItemID;
                FDQBonLinesEAN.AsString:=BonLine.FEan;
                FDQBonLinesITEMNAME.AsString:=BonLine.FItemName;
                FDQBonLinesUNIT.AsString:=BonLine.FUnit;
                FDQBonLinesVATRATE.AsFloat:=BonLine.FVatRate;
                FDQBonLinesSALESPRICEPRUNIT.AsFloat:=RoundEx(BonLine.FSalesPricePrUnit, 5);
                FDQBonLinesLINEDATETIME.AsDateTime:=BonLine.FDateTime;
                FDQBonLinesQTY.AsFloat:=BonLine.FQty;
                FDQBonLinesPOSCLIENTLINEID.Value:=BonLine.FLineNo;
                FDQBonLinesLINESTATUS.Value:=BonLine.FLineStatus;
                FDQBonLinesLINEDISCOUNT.AsCurrency:=RoundEx(BonLine.FLineDiscount,5);
                FDQBonLinesINVOICEDISCOUNT.AsFloat:=0;
                FDQBonLines.Post;

                // Register ItemTransactions
                ItemTrans.REFTABEL:=TABLE_BONJOURNALLINES;
                ItemTrans.REFLINJUID:=FDQBonLinesLINEID.Value;
                ItemTrans.NETTOPRISUR:=FDQBonLinesCOSTPRICEPRUNIT.AsFloat;
                ItemTrans.KOSTPRISUR:=FDQBonLinesCOSTPRICEPRUNIT.AsFloat;
                ItemTrans.VEKTGJALD:=0;
                ItemTrans.GATT:=0;
                ItemTrans.PUNKT:=0;
                ItemTrans.MVGSATSUR:=FDQBonLinesVATRATE.AsFloat;
                ItemTrans.LINJUAVSLATTUR:=FDQBonLinesLINEDISCOUNT.AsFloat;
                ItemTrans.INVOICEDISCOUNT:=FDQBonLinesINVOICEDISCOUNT.AsFloat;
                ItemTrans.DAGFEST:=FDQBonLinesLINEDATETIME.AsDateTime;
                ItemTrans.SOLUPRISUR:=FDQBonLinesSALESPRICEPRUNIT.AsFloat;
                ItemTrans.VARUNRID:=FDQBonLinesITEMID.AsInteger;
                ItemTrans.TRANSSLAG:=0;  // TransType 0 = Sale
                ItemTrans.NOGD:=FDQBonLinesQTY.AsFloat;
                ItemTrans.DATAAREAID:=FDQBonLinesDATAAREAID.AsInteger;
                ItemTrans.FLUTNINGSKOSTNADUR:=0;
                ItemTrans.REFLINESTATUS:=FDQBonLinesLINESTATUS.Value;

                if UpdateState = sInsert then
                   FItemTrans.InsertItem(ItemTrans)
                else
                   FItemTrans.UpdateItem(ItemTrans,TABLE_BONJOURNALLINES,OLD_BonLineID);

            except
                on E: exception do
                    LogFile(self,'ERROR: BonID '+BonLine.FBonID.ToString+': '+E.Message, True);
            end;


            FDQBonLines.Close;
            result:=true;
        except
            on E: Exception do
            begin
              FDQBonLines.Cancel;
              FDQBonLines.Close;
              LogFile(self,'Error BonLine BONID/LINEID: '+IntToStr(BonLine.FBonID)+'; '+IntToStr(BonLine.FLineNo),True);
              LogFile(self,'Error BonLine ItemID/ItemText: '+IntToStr(BonLine.FitemID)+': '+BonLine.FItemName,True);
              dmDBConnection.FDConnection1.Rollback;
            end;
        end;
    finally
        if dmDBConnection.FDConnection1.InTransaction then
           dmDBConnection.FDConnection1.Commit;
    end;
end;

procedure TdmBon.SavePayment(Payment: TBonPayment);
begin
    FDQPayments.ParamByName('DATAAREAID').Value:= FDataAreaID;
    FDQPayments.ParamByName('POSCLIENTID').Value:=Payment.FPOSClientID;
    FDQPayments.ParamByName('BONID').Value:=Payment.FBonID;
    FDQPayments.ParamByName('POSCLIENTLINEID').Value:=Payment.FLineNo;
    FDQPayments.Open;

    try
        if (FDQPayments.RecordCount = 0) then
            FDQPayments.Insert
        else if (FDQPaymentsPAYMENTTYPE.Value = -1) then
            FDQPayments.Edit
        else
        begin
            FDQPayments.Close;
            Exit;
        end;

        FDQPaymentsLINEID.Value:=dmDBConnection.GetGenID('BONPAYMENTID_GEN');
        FDQPaymentsBONID.Value:=Payment.FBonID;
        FDQPaymentsPOSCLIENTID.Value:=Payment.FPosClientID;
        FDQPaymentsDATAAREAID.Value:=FDataAreaID;
        FDQPaymentsPAYMENTTYPE.Value:=Payment.FPaymMethod;
        FDQPaymentsAMOUNT.Value:=Payment.FPaydAmount;
        FDQPaymentsPOSCLIENTLINEID.Value:=Payment.FLineNo;
        FDQPayments.Post;

        FDQPayments.Close;
    except
        FDQPayments.Cancel;
        FDQPayments.Close;
        LogFile(self,'Error BonPayment BONID/LINEID: '+IntToStr(Payment.FBonID)+'; '+IntToStr(Payment.FLineNo),True);
    end;
end;

procedure TdmBon.UpdateBonStatus(BonID: integer; Status: SmallInt);
begin
    FDComBonStatus.ParamByName('BONSTATUS').Value:=Status;
    FDComBonStatus.ParamByName('BONID').Value:=BonID;
    FDComBonStatus.Execute;
end;

end.
