unit dmVirtualPOSClientsUnit;

interface

uses
  System.SysUtils
  , System.Classes
  , System.Generics.Collections
  , FireDAC.Stan.Intf
  , FireDAC.Stan.Option
  , FireDAC.Stan.Param
  , FireDAC.Stan.Error
  , FireDAC.DatS
  , FireDAC.Phys.Intf
  , FireDAC.DApt.Intf
  , FireDAC.Stan.Async
  , FireDAC.DApt
  , Data.DB
  , FireDAC.Comp.DataSet
  , FireDAC.Comp.Client, SharedUnit
  ;

type
  TdmVirtualPOSClients = class(TDataModule)
    FDQBonHeader: TFDQuery;
    FDQBonHeaderBONID: TIntegerField;
    FDQBonHeaderBONDATETIME: TSQLTimeStampField;
    FDQBonHeaderPOSCLIENTID: TIntegerField;
    FDQBonHeaderPAYMENTTYPE: TSmallintField;
    FDQBonHeaderAMOUNT: TCurrencyField;
    FDComUpdateBon: TFDCommand;
    procedure DataModuleDestroy(Sender: TObject);
  private
    FVirtualPOSClients: TList<TVirtualClient>;
    procedure GetVirtualClients;
    procedure UpdateBon(BonID, AStatementID: integer);
  public
    procedure CreateShift;
  end;



implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses
  dmDBConnectionUnit
  , IniFileUnit
  , DateUtils
  , FMX.Dialogs
  , TeraFunkUnit
  , dmCaxhStatementsUnit
  ;

 type
  TStatement = class
  strict private
    FPayments: TDictionary<SmallInt,Currency>;
    FDataAreaID: SmallInt;
    FID: integer;
    FPOSClientID: integer;
    FDateFrom: TDateTime;
    FDateTo: TDateTime;
  private
    procedure AddPayment(ID: SmallInt; Amount: Currency);
    constructor Create(ID: integer; ADate:TDateTime; APOSClientID: integer; ADataAreaID: SmallInt);
    destructor Destroy; override;
    property ID: integer read FID;
    property POSClientID: integer read FPOSClientID;
    property DateFrom: TDateTime read FDateFrom;
    property DateTo: TDateTime read FDateTo;
    property Payments: TDictionary<SmallInt,Currency> read FPayments;
    property DataAreaID: SmallInt read FDataAreaID;
  end;

  TStatementList = record
  strict private
    FStatements: TList<TStatement>;
    FStatement: TStatement;
    FCurrentID: integer;
  private
    function AddPayment(APosClientID: integer; DateTime: TDateTime;  PaymentID: SmallInt; Amount: Currency; ADataAreaID: SmallInt): integer;
    function SaveStatements: boolean;
    procedure Create;
    procedure Destroy;
  end;


{$R *.dfm}


procedure TdmVirtualPOSClients.CreateShift;
var
  Clients: TVirtualClient;
  LStatementID: integer;
  LStatementList: TStatementList;
begin
    dmDBConnection.FDConnection1.StartTransaction;
    GetVirtualClients;
    LStatementList.Create;
    try
        for Clients in FVirtualPOSClients do    // VirtualPosClients settings in inifile.
        begin
            FDQBonHeader.Close;
            FDQBonHeader.ParamByName('POSCLIENTID').Value:=Clients.ClientID;
            FDQBonHeader.ParamByName('CURRENTDATE').Value:=Date;
            FDQBonHeader.ParamByName('DATAAREAID').Value:=Clients.DataAreaID;
            FDQBonHeader.Open;

            while not FDQBonHeader.Eof do
            begin
                LStatementID:=LStatementList.AddPayment(FDQBonHeaderPOSCLIENTID.Value
                                          ,FDQBonHeaderBONDATETIME.AsDateTime
                                          ,FDQBonHeaderPAYMENTTYPE.Value
                                          ,FDQBonHeaderAMOUNT.Value
                                          , Clients.DataAreaID
                                          );

                if (LStatementID > 0) then
                   UpdateBon(FDQBonHeaderBONID.Value,LStatementID);

                FDQBonHeader.Next;
            end;
        end;

        // Save Statements
        if LStatementList.SaveStatements = true then
           dmDBConnection.FDConnection1.Commit
        else
           dmDBConnection.FDConnection1.Rollback;
    finally
        LStatementList.Destroy;
    end;
end;

procedure TdmVirtualPOSClients.DataModuleDestroy(Sender: TObject);
begin
    //if assigned(FVirtualPOSClients) then
    //  FVirtualPOSClients.Free;
end;

procedure TdmVirtualPOSClients.GetVirtualClients;
begin
   //FVirtualPOSClients:=TList<SmallInt>.Create;
   dmDBConnection.ProgramParams.GetVirtalPOSClients(FVirtualPOSClients);
end;

procedure TdmVirtualPOSClients.UpdateBon(BonID, AStatementID: integer);
begin
    FDComUpdateBon.ParamByName('BONID').Value:=BonID;
    FDComUpdateBon.ParamByName('STATEMENTID').Value:=AStatementID;
    FDComUpdateBon.Execute;
end;

{ TStatement }

procedure TStatement.AddPayment(ID: SmallInt; Amount: Currency);
var
    LAmount: Currency;
begin
    if FPayments.TryGetValue(ID,LAmount) then
    begin
        LAmount:=LAmount+Amount;
        FPayments.AddOrSetValue(ID,LAmount);
    end
    else
        FPayments.Add(ID,Amount);
end;

constructor TStatement.Create(ID: integer; ADate:TDateTime; APOSClientID: integer; ADataAreaID: SmallInt);
begin
    FID:=ID;
    FDataAreaID:=ADataAreaID;
    FPOSClientID:=APOSClientID;
    FDateFrom:=DateOf(ADate);
    FDateTo:=DateOf(ADate)+StrToTime('23'+FormatSettings.TimeSeparator+'59'+FormatSettings.TimeSeparator+'59');
    FPayments:=TDictionary<SmallInt,Currency>.Create;
end;

destructor TStatement.Destroy;
begin
  FPayments.Free;
  inherited;
end;

{ TStatementList }

function TStatementList.AddPayment(APosClientID: integer; DateTime: TDateTime;
  PaymentID: SmallInt; Amount: Currency; ADataAreaID: SmallInt): integer;
var
  tempID: integer;
begin
    try
        tempID:=StrToInt(FormatDateToStr(DateOf(DateTime),'YYYYMMDD'));

        if FCurrentID = 0 then
        begin
            FStatement:=TStatement.Create(tempID, DateTime, APosClientID, ADataAreaID);
            FCurrentID:=tempID;
        end;

        if (FStatement.ID <> tempID) or (FStatement.POSClientID <> APosClientID) then
        begin
            FStatements.Add(FStatement);
            FStatement:=TStatement.Create(tempID, DateTime, APosClientID, ADataAreaID);
        end;

        FStatement.AddPayment(PaymentID,Amount);
        result:=FStatement.ID;
    except
        result:=-1;
    end;
end;

procedure TStatementList.Create;
begin
    FCurrentID:=0;
    FStatements:=TList<TStatement>.Create;
    FStatement := nil;
end;

procedure TStatementList.Destroy;
begin
    if assigned(FStatements) then
       FreeAndNil(FStatements);
end;

function TStatementList.SaveStatements: boolean;
var
  LStatement: TStatement;
  LPaym: TPair<SmallInt,Currency>;
  dmCashStatement: TdmCashStatements;
begin
    dmCashStatement:=TdmCashStatements.Create(nil);
    try
        try
            if FStatement <> nil then
               FStatements.Add(FStatement);

            for LStatement in FStatements do
            begin
               dmCashStatement.SaveStatementHeader(LStatement.ID
                                                   , LStatement.POSClientID
                                                   , LStatement.DateFrom
                                                   , LStatement.DateTo
                                                   , '00'
                                                   , LStatement.DataAreaID
                                                    );

                for LPaym in LStatement.Payments do
                begin
                    dmCashStatement.SavePayments(LPaym.Key,LPaym.Value,LPaym.Value);
                end;

                LStatement.Free;
            end;

            result:=true;
        except
            result:=False;
        end;
    finally
        dmCashStatement.Free;
    end;
end;

end.
