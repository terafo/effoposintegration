object dmItemTransaction: TdmItemTransaction
  OldCreateOrder = False
  Height = 366
  Width = 533
  object FDQItemTransaction: TFDQuery
    Connection = dmDBConnection.FDConnection1
    UpdateObject = FDUpdateSQL1
    SQL.Strings = (
      'select * from varutrans')
    Left = 48
    Top = 32
  end
  object FDUpdateSQL1: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO VARUTRANS'
      '(TRANSID, NETTOPRISUR, KOSTPRISUR, FLUTNINGSKOSTNADUR, '
      '  MVGSATSUR, GATT, PUNKT, VEKTGJALD, LINJUAVSLATTUR, '
      '  FAKTURAAVSLATTUR, STARVSFOLK, DAGFEST, SOLUPRISUR, '
      '  VARUNRID, TRANSSLAG, NOGD, TILSAMANS, '
      '  LINJUUPPHADD, LINJUUPPHADDTOTAL, DATAAREAID, '
      '  REFLINJUID, REFTABEL)'
      
        'VALUES (:NEW_TRANSID, :NEW_NETTOPRISUR, :NEW_KOSTPRISUR, :NEW_FL' +
        'UTNINGSKOSTNADUR, '
      
        '  :NEW_MVGSATSUR, :NEW_GATT, :NEW_PUNKT, :NEW_VEKTGJALD, :NEW_LI' +
        'NJUAVSLATTUR, '
      
        '  :NEW_FAKTURAAVSLATTUR, :NEW_STARVSFOLK, :NEW_DAGFEST, :NEW_SOL' +
        'UPRISUR, '
      '  :NEW_VARUNRID, :NEW_TRANSSLAG, :NEW_NOGD, :NEW_TILSAMANS, '
      '  :NEW_LINJUUPPHADD, :NEW_LINJUUPPHADDTOTAL, :NEW_DATAAREAID, '
      '  :NEW_REFLINJUID, :NEW_REFTABEL)')
    ModifySQL.Strings = (
      'UPDATE VARUTRANS'
      
        'SET TRANSID = :NEW_TRANSID, NETTOPRISUR = :NEW_NETTOPRISUR, KOST' +
        'PRISUR = :NEW_KOSTPRISUR, '
      
        '  FLUTNINGSKOSTNADUR = :NEW_FLUTNINGSKOSTNADUR, MVGSATSUR = :NEW' +
        '_MVGSATSUR, '
      
        '  GATT = :NEW_GATT, PUNKT = :NEW_PUNKT, VEKTGJALD = :NEW_VEKTGJA' +
        'LD, '
      
        '  LINJUAVSLATTUR = :NEW_LINJUAVSLATTUR, FAKTURAAVSLATTUR = :NEW_' +
        'FAKTURAAVSLATTUR, '
      
        '  STARVSFOLK = :NEW_STARVSFOLK, DAGFEST = :NEW_DAGFEST, SOLUPRIS' +
        'UR = :NEW_SOLUPRISUR, '
      
        '  VARUNRID = :NEW_VARUNRID, TRANSSLAG = :NEW_TRANSSLAG, NOGD = :' +
        'NEW_NOGD, '
      '  TILSAMANS = :NEW_TILSAMANS, LINJUUPPHADD = :NEW_LINJUUPPHADD, '
      
        '  LINJUUPPHADDTOTAL = :NEW_LINJUUPPHADDTOTAL, DATAAREAID = :NEW_' +
        'DATAAREAID, '
      '  REFLINJUID = :NEW_REFLINJUID, REFTABEL = :NEW_REFTABEL'
      'WHERE REFLINJUID = :OLD_REFLINJUID AND REFTABEL = :OLD_REFTABEL')
    DeleteSQL.Strings = (
      'DELETE FROM VARUTRANS'
      'WHERE REFLINJUID = :OLD_REFLINJUID AND REFTABEL = :OLD_REFTABEL')
    FetchRowSQL.Strings = (
      
        'SELECT TRANSID, NETTOPRISUR, KOSTPRISUR, FLUTNINGSKOSTNADUR, MVG' +
        'SATSUR, '
      '  GATT, PUNKT, VEKTGJALD, LINJUAVSLATTUR, FAKTURAAVSLATTUR, '
      '  STARVSFOLK, DAGFEST, SOLUPRISUR, VARUNRID, TRANSSLAG, NOGD, '
      '  TILSAMANS, LINJUUPPHADD, LINJUUPPHADDTOTAL, DATAAREAID, '
      '  REFLINJUID, REFTABEL'
      'FROM VARUTRANS'
      'WHERE REFLINJUID = :OLD_REFLINJUID AND REFTABEL = :OLD_REFTABEL')
    Left = 144
    Top = 32
  end
  object FDComItemTransInsert: TFDCommand
    Connection = dmDBConnection.FDConnection1
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    CommandText.Strings = (
      'INSERT INTO VARUTRANS'
      '(NETTOPRISUR, KOSTPRISUR, FLUTNINGSKOSTNADUR, '
      '  MVGSATSUR, GATT, PUNKT, VEKTGJALD, LINJUAVSLATTUR, '
      '  FAKTURAAVSLATTUR, STARVSFOLK, DAGFEST, SOLUPRISUR, '
      '  VARUNRID, TRANSSLAG, NOGD, DATAAREAID, '
      '  REFLINJUID, REFTABEL)'
      
        'VALUES (:NEW_NETTOPRISUR, :NEW_KOSTPRISUR, :NEW_FLUTNINGSKOSTNAD' +
        'UR, '
      
        '  :NEW_MVGSATSUR, :NEW_GATT, :NEW_PUNKT, :NEW_VEKTGJALD, :NEW_LI' +
        'NJUAVSLATTUR, '
      
        '  :NEW_FAKTURAAVSLATTUR, :NEW_STARVSFOLK, :NEW_DAGFEST, :NEW_SOL' +
        'UPRISUR, '
      '  :NEW_VARUNRID, :NEW_TRANSSLAG, :NEW_NOGD, :NEW_DATAAREAID, '
      '  :NEW_REFLINJUID, :NEW_REFTABEL)')
    ParamData = <
      item
        Name = 'NEW_NETTOPRISUR'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 3
        ParamType = ptInput
      end
      item
        Name = 'NEW_KOSTPRISUR'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 3
        ParamType = ptInput
      end
      item
        Name = 'NEW_FLUTNINGSKOSTNADUR'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 2
        ParamType = ptInput
      end
      item
        Name = 'NEW_MVGSATSUR'
        DataType = ftCurrency
        Precision = 4
        NumericScale = 2
        ParamType = ptInput
      end
      item
        Name = 'NEW_GATT'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 2
        ParamType = ptInput
      end
      item
        Name = 'NEW_PUNKT'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 2
        ParamType = ptInput
      end
      item
        Name = 'NEW_VEKTGJALD'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 2
        ParamType = ptInput
      end
      item
        Name = 'NEW_LINJUAVSLATTUR'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 3
        ParamType = ptInput
      end
      item
        Name = 'NEW_FAKTURAAVSLATTUR'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 3
        ParamType = ptInput
      end
      item
        Name = 'NEW_STARVSFOLK'
        DataType = ftString
        ParamType = ptInput
        Size = 10
      end
      item
        Name = 'NEW_DAGFEST'
        DataType = ftTimeStamp
        ParamType = ptInput
      end
      item
        Name = 'NEW_SOLUPRISUR'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 3
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'NEW_VARUNRID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 175895
      end
      item
        Name = 'NEW_TRANSSLAG'
        DataType = ftSmallint
        ParamType = ptInput
      end
      item
        Name = 'NEW_NOGD'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 3
        ParamType = ptInput
      end
      item
        Name = 'NEW_DATAAREAID'
        DataType = ftSmallint
        ParamType = ptInput
        Value = 1
      end
      item
        Name = 'NEW_REFLINJUID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 3
      end
      item
        Name = 'NEW_REFTABEL'
        DataType = ftSmallint
        ParamType = ptInput
        Value = 35
      end>
    Left = 60
    Top = 96
  end
  object FDComItemTransUpdate: TFDCommand
    Connection = dmDBConnection.FDConnection1
    CommandText.Strings = (
      'UPDATE VARUTRANS'
      'SET  NETTOPRISUR = :NEW_NETTOPRISUR'
      ', KOSTPRISUR = :NEW_KOSTPRISUR'
      ', FLUTNINGSKOSTNADUR = :NEW_FLUTNINGSKOSTNADUR'
      ', MVGSATSUR = :NEW_MVGSATSUR'
      ', GATT = :NEW_GATT'
      ', PUNKT = :NEW_PUNKT'
      ', VEKTGJALD = :NEW_VEKTGJALD'
      ', LINJUAVSLATTUR = :NEW_LINJUAVSLATTUR'
      ', FAKTURAAVSLATTUR = :NEW_FAKTURAAVSLATTUR'
      ', STARVSFOLK = :NEW_STARVSFOLK'
      ', DAGFEST = :NEW_DAGFEST'
      ', SOLUPRISUR = :NEW_SOLUPRISUR'
      ', VARUNRID = :NEW_VARUNRID'
      ', TRANSSLAG = :NEW_TRANSSLAG'
      ', NOGD = :NEW_NOGD'
      ', DATAAREAID = :NEW_DATAAREAID'
      ', REFLINJUID = :NEW_REFLINJUID'
      ', REFTABEL = :NEW_REFTABEL'
      'WHERE REFLINJUID = :OLD_REFLINJUID AND REFTABEL = :OLD_REFTABEL')
    ParamData = <
      item
        Name = 'NEW_NETTOPRISUR'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 3
        ParamType = ptInput
      end
      item
        Name = 'NEW_KOSTPRISUR'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 3
        ParamType = ptInput
      end
      item
        Name = 'NEW_FLUTNINGSKOSTNADUR'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 2
        ParamType = ptInput
      end
      item
        Name = 'NEW_MVGSATSUR'
        DataType = ftCurrency
        Precision = 4
        NumericScale = 2
        ParamType = ptInput
      end
      item
        Name = 'NEW_GATT'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 2
        ParamType = ptInput
      end
      item
        Name = 'NEW_PUNKT'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 2
        ParamType = ptInput
      end
      item
        Name = 'NEW_VEKTGJALD'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 2
        ParamType = ptInput
      end
      item
        Name = 'NEW_LINJUAVSLATTUR'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 3
        ParamType = ptInput
      end
      item
        Name = 'NEW_FAKTURAAVSLATTUR'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 3
        ParamType = ptInput
      end
      item
        Name = 'NEW_STARVSFOLK'
        DataType = ftString
        ParamType = ptInput
        Size = 10
      end
      item
        Name = 'NEW_DAGFEST'
        DataType = ftTimeStamp
        ParamType = ptInput
      end
      item
        Name = 'NEW_SOLUPRISUR'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 3
        ParamType = ptInput
      end
      item
        Name = 'NEW_VARUNRID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'NEW_TRANSSLAG'
        DataType = ftSmallint
        ParamType = ptInput
      end
      item
        Name = 'NEW_NOGD'
        DataType = ftCurrency
        Precision = 9
        NumericScale = 3
        ParamType = ptInput
      end
      item
        Name = 'NEW_DATAAREAID'
        DataType = ftSmallint
        ParamType = ptInput
      end
      item
        Name = 'NEW_REFLINJUID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'NEW_REFTABEL'
        DataType = ftSmallint
        ParamType = ptInput
      end
      item
        Name = 'OLD_REFLINJUID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'OLD_REFTABEL'
        DataType = ftSmallint
        ParamType = ptInput
      end>
    Left = 57
    Top = 152
  end
  object FDComItemTransDelete: TFDCommand
    Connection = dmDBConnection.FDConnection1
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    CommandText.Strings = (
      'DELETE FROM VARUTRANS'
      'WHERE REFLINJUID = :OLD_REFLINJUID AND REFTABEL = :OLD_REFTABEL')
    ParamData = <
      item
        Name = 'OLD_REFLINJUID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'OLD_REFTABEL'
        DataType = ftSmallint
        ParamType = ptInput
      end>
    Left = 57
    Top = 208
  end
end
