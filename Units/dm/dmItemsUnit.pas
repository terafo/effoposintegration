unit dmItemsUnit;

interface

uses
  System.SysUtils
  , System.Classes
  , FireDAC.Stan.Intf
  , FireDAC.Stan.Option
  , FireDAC.Stan.Param
  , FireDAC.Stan.Error
  , FireDAC.DatS
  , FireDAC.Phys.Intf
  , FireDAC.DApt.Intf
  , FireDAC.Stan.Async
  , FireDAC.DApt
  , Data.DB
  , FireDAC.Comp.DataSet
  , FireDAC.Comp.Client
  , dmDBConnectionUnit
  ;

type
  TdmItems = class(TDataModule)
    FDQItems: TFDQuery;
    FDQBarCodes: TFDQuery;
    FDQBarCodesVARUNRID: TIntegerField;
    FDQBarCodesEAN: TStringField;
    FDQPromotionHeader: TFDQuery;
    FDQPromoLines: TFDQuery;
    FDQPromotionHeaderTILBODNR: TIntegerField;
    FDQPromotionHeaderTILBODSPRISUR: TCurrencyField;
    FDQPromotionHeaderBYRJA: TDateField;
    FDQPromotionHeaderENDA: TDateField;
    FDQPromotionHeaderLINJUTEKSTUR: TStringField;
    FDQPromoLinesLINJUID: TIntegerField;
    FDQPromoLinesMIXRABATNR: TIntegerField;
    FDQPromoLinesVARUNRID: TIntegerField;
    FDQPromoLinesKOSTPRISUR: TCurrencyField;
    FDQPromoLinesNIVEU: TSmallintField;
    FDQPromoLinesNOGD: TCurrencyField;
    FDQPromoLinesDATAAREAID: TSmallintField;
    FDQPromoLinesSALESPRICE: TFMTBCDField;
    FDQPriceUpdate: TFDQuery;
    FDQDeleteItem: TFDQuery;
    FDQItemsITEMID: TIntegerField;
    FDQItemsITEMGRPID: TStringField;
    FDQItemsVARUNAVN: TStringField;
    FDQItemsMVG: TStringField;
    FDQItemsSOLUPRISUR1: TFMTBCDField;
    FDQItemsSPERRAAVSLATTUR: TSmallintField;
    FDQItemsSATSUR: TCurrencyField;
    FDQItemsNEGATIVQUANTITY: TSmallintField;
    FDQPriceGroups: TFDQuery;
    FDQPriceGroupsSALESPRICE: TFMTBCDField;
    FDQPromoPriceGroups: TFDQuery;
    FDQPromotionHeaderDATAAREAID: TSmallintField;
    FDQPromoPriceGroupsPRICEGROUPID: TIntegerField;
    FDQItemsUNDIRBOLKUR: TIntegerField;
  private
    { Private declarations }
  public
    procedure DeleteItemFromQueue(ItemID: integer);
    function GetPriceFromPriceGroup(ItemID: integer; PriceGroup: SmallInt; out Price: Currency):boolean;
    function CheckIfCompanyIsOnCampaignPriceGroup(APriceGroup: integer):boolean;
  end;

var
  dmItems: TdmItems;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

{ TdmItems }

function TdmItems.CheckIfCompanyIsOnCampaignPriceGroup(APriceGroup: integer): boolean;
begin
    //Um ongin pr�sb�lkur er settur � tilbo�i er ta� galdanda fyri allar handlarnar
    // annars er ta� bara galdandi fyri teir handlar, sum eru � �settu pr�sb�lkum,
    try
        FDQPromoPriceGroups.Close;
        FDQPromoPriceGroups.ParamByName('MIXCAMPID').Value:=FDQPromotionHeaderTILBODNR.Value;
        FDQPromoPriceGroups.ParamByName('DATAAREAID').Value:=FDQPromotionHeaderDATAAREAID.Value;
        FDQPromoPriceGroups.Open;

        if (FDQPromoPriceGroups.RecordCount > 0) then
        begin
            result:=False;

            while not FDQPromoPriceGroups.Eof do
            begin
                if FDQPromoPriceGroupsPRICEGROUPID.Value = APriceGroup then
                   result:=true;

                FDQPromoPriceGroups.Next;
            end;
        end
        else
            result:=true;
    finally
        FDQPromoPriceGroups.Close;
    end;
end;

procedure TdmItems.DeleteItemFromQueue(ItemID: integer);
begin
    dmDBConnection.FDConnection1.StartTransaction;
    FDQDeleteItem.ParamByName('ITEMID').Value:=ItemID;
    FDQDeleteItem.Execute;
    dmDBConnection.FDConnection1.Commit;
end;

function TdmItems.GetPriceFromPriceGroup(ItemID: integer; PriceGroup: SmallInt; out Price: Currency): boolean;
begin
    result:=false;
    try
        FDQPriceGroups.ParamByName('ITEMID').Value:=ItemID;
        FDQPriceGroups.ParamByName('PRICEGROUPID').Value:=PriceGroup;
        FDQPriceGroups.Open;

        if (FDQPriceGroups.RecordCount > 0) then
        begin
            if not FDQPriceGroupsSALESPRICE.IsNull then
            begin
                Price:=FDQPriceGroupsSALESPRICE.AsCurrency;
                result:=True;
            end;
        end;
    finally
        FDQPriceGroups.Close;
    end;
end;

end.
