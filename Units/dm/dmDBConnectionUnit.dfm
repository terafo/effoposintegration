object dmDBConnection: TdmDBConnection
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 360
  Width = 481
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=C:\TestDB\Effo\Leirvik\PROJECTSTYRING.GDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Protocol=TCPIP'
      'Server=127.0.0.1'
      'DriverID=IB')
    ConnectedStoredUsage = [auDesignTime]
    LoginPrompt = False
    BeforeConnect = FDConnection1BeforeConnect
    Left = 88
    Top = 48
  end
  object FDTransaction1: TFDTransaction
    Connection = FDConnection1
    Left = 184
    Top = 48
  end
  object FDQGetGenID: TFDQuery
    Connection = FDConnection1
    Left = 88
    Top = 104
  end
end
