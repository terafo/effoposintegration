object dmSystemTables: TdmSystemTables
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 299
  Width = 411
  object FDMemFuelPosPaymMethods: TFDMemTable
    AfterOpen = FDMemFuelPosPaymMethodsAfterOpen
    BeforeRefresh = FDMemFuelPosPaymMethodsBeforeRefresh
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'PAYMINDEX'
        Fields = 'MOP;MOP_ADDX;MOP_ADDY;ROP'
      end>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 56
    Top = 32
    object FDMemFuelPosPaymMethodsMOP: TSmallintField
      FieldName = 'MOP'
    end
    object FDMemFuelPosPaymMethodsMOP_ADDX: TSmallintField
      FieldName = 'MOP_ADDX'
    end
    object FDMemFuelPosPaymMethodsMOP_ADDY: TSmallintField
      FieldName = 'MOP_ADDY'
    end
    object FDMemFuelPosPaymMethodsROP: TSmallintField
      FieldName = 'ROP'
    end
    object FDMemFuelPosPaymMethodsPAYMENTMETHOD: TSmallintField
      FieldName = 'PAYMENTMETHOD'
    end
    object FDMemFuelPosPaymMethodsPAYMMETHNAME: TStringField
      FieldKind = fkLookup
      FieldName = 'PAYMMETHNAME'
      LookupDataSet = FDQPaymentMethods
      LookupKeyFields = 'PAYMENTTYPEID'
      LookupResultField = 'NAME'
      KeyFields = 'PAYMENTMETHOD'
      Size = 50
      Lookup = True
    end
    object FDMemFuelPosPaymMethodsNODENAME: TStringField
      FieldName = 'NODENAME'
    end
  end
  object FDStanStorageXMLLink1: TFDStanStorageXMLLink
    Left = 264
    Top = 40
  end
  object FDQPaymentMethods: TFDQuery
    ActiveStoredUsage = []
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select paymenttypeID, NAME '
      'from paymenttype'
      'order by paymenttypeid')
    Left = 56
    Top = 88
    object FDQPaymentMethodsPAYMENTTYPEID: TSmallintField
      FieldName = 'PAYMENTTYPEID'
      Origin = 'PAYMENTTYPEID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQPaymentMethodsNAME: TStringField
      DisplayWidth = 30
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 100
    end
  end
end
