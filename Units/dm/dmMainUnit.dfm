object dmMain: TdmMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 293
  Width = 486
  object FDQCompanies: TFDQuery
    Connection = dmDBConnection.FDConnection1
    SQL.Strings = (
      'select FYRITOKAID, STDPRISOLKUR'
      'from fyritoka')
    Left = 272
    Top = 40
    object FDQCompaniesFYRITOKAID: TSmallintField
      FieldName = 'FYRITOKAID'
      Origin = 'FYRITOKAID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQCompaniesSTDPRISOLKUR: TIntegerField
      FieldName = 'STDPRISOLKUR'
      Origin = 'STDPRISOLKUR'
    end
  end
end
